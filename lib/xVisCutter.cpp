#include "xVisCutter.hpp"

#include "xVisImplicitFunction.hpp"

xVisCutter::xVisCutter() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkCutter>::New())
{
    this->m_object = vtkCutter::SafeDownCast(xVisAlgorithm::get());
    this->m_cutFunctionCb = [this] () {
        this->updateCutFunction();
    };
}

xVisCutter::~xVisCutter() {
    if (this->m_cutFunction) {
        this->m_cutFunction->removeCallback(std::move(this->m_cutFunctionCb));
    }
}

auto xVisCutter::updateCutFunction() -> void {
    if (this->m_cutFunction) {
        this->update();
    }
}

auto xVisCutter::setCutFunction(xVisImplicitFunction* cutFunction) -> void {
    if (this->m_cutFunction) {
        this->m_cutFunction->removeCallback(std::move(this->m_cutFunctionCb));
    }

    this->m_cutFunction = cutFunction;

    if (cutFunction) {
        this->m_object->SetCutFunction(cutFunction->get());
        cutFunction->addCallback(std::move(this->m_cutFunctionCb));
        this->updateCutFunction();
    }

    emit this->cutFunctionChanged();
}

auto xVisCutter::getCutFunction() -> xVisImplicitFunction* {
    return this->m_cutFunction;
}
