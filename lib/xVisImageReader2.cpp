#include "xVisImageReader2.hpp"

xVisImageReader2::xVisImageReader2(vtkSmartPointer<vtkImageReader2> vtkObject) : xVisImageAlgorithm(vtkObject) {
    this->m_object = vtkObject;
}

auto xVisImageReader2::isValid() -> bool {
    if (!QFileInfo::exists(this->m_fileName)) {
        return false;
    }

    return true;
}

auto xVisImageReader2::setFileName(const QString& fileName) -> void {
    this->m_fileName = fileName;

    emit this->fileNameChanged();

    if (QFileInfo::exists(fileName)) {
        this->m_object->SetFileName(fileName.toStdString().c_str());
        this->m_object->Update();
        this->update();
    }
}

auto xVisImageReader2::getFileName() -> QString {
    return this->m_fileName;
}
