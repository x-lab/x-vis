#include "xVisTriangleFilter.hpp"

xVisTriangleFilter::xVisTriangleFilter() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkTriangleFilter>::New()) {
    this->m_object = vtkTriangleFilter::SafeDownCast(xVisAlgorithm::get());
}

auto xVisTriangleFilter::setPassVerts(bool pass) -> void {
    this->m_object->SetPassVerts(pass);
    this->update();
    emit this->passVertsChanged();
}

auto xVisTriangleFilter::setPassLines(bool pass) -> void {
    this->m_object->SetPassLines(pass);
    this->update();
    emit this->passLinesChanged();
}

auto xVisTriangleFilter::getPassVerts() -> bool {
    return this->m_object->GetPassVerts();
}

auto xVisTriangleFilter::getPassLines() -> bool {
    return this->m_object->GetPassLines();
}
