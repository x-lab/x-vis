#include "xVisGenericInteractor.hpp"

#include <QtCore>

#include <vtkCommand.h>
#include <vtkTransform.h>
#include <vtkRenderWindow.h>
#include <vtkRendererCollection.h>
#include <vtkObjectFactory.h>

vtkStandardNewMacro(xVisGenericInteractor);

void xVisGenericInteractor::Initialize(void)
{
    vtkRenderWindow *ren;

    int *size;

    if (!this->RenderWindow) {
        vtkErrorMacro(<< "No renderer defined!");
        return;
    }

    if (this->Initialized)
        return;

    this->Initialized = 1;

    ren = (vtkRenderWindow *)(this->RenderWindow);
    ren->Start();

    size = ren->GetSize();

    ren->GetPosition();

    this->Enable();
    this->Size[0] = size[0];
    this->Size[1] = size[1];

    m_axes_actor->SetTotalLength(10., 10., 10.);

    this->callbackCommand->SetCallback(xVisGenericInteractor::axisActorToggle);
    this->callbackCommand->SetClientData((void*)this);

    this->AddObserver(vtkCommand::CharEvent, this->callbackCommand);
}

void xVisGenericInteractor::axisActorToggle(vtkObject* vtkNotUsed(object), unsigned long event, void* clientdata, void* calldata)
{
    xVisGenericInteractor* self = reinterpret_cast<xVisGenericInteractor*>(clientdata);

    if(self->GetKeyCode() == 'a') {
        auto renderers = self->GetRenderWindow()->GetRenderers();
        int numRens = renderers->GetNumberOfItems();

        for (int i = numRens -1; i >= 0; i--)
        {
            vtkRenderer *renderer = static_cast<vtkRenderer *>(renderers->GetItemAsObject(i));
            if(self->m_axes_displayed) {
                renderer->RemoveActor(self->m_axes_actor);
            } else {
                renderer->AddActor(self->m_axes_actor);
            }
        }
        self->m_axes_displayed = !self->m_axes_displayed;
    }
}
