#pragma once

#include "xVisObject.hpp"

#include <vtkSmartPointer.h>
#include <vtkProp.h>

class xVisViewer;

class xVisProp : public xVisObject
{
    Q_OBJECT
    Q_PROPERTY(bool visibility READ getVisibility WRITE setVisibility NOTIFY visibilityChanged);

public:
             xVisProp(void) = delete;
             xVisProp(vtkSmartPointer<vtkProp>);
    virtual ~xVisProp(void);

    void update(void);
    void setVisibility(bool);
    bool getVisibility(void);
    void linkViewer(xVisViewer*);
    void unlinkViewer(xVisViewer*);
    vtkSmartPointer<vtkProp> get(void);

signals:
    void visibilityChanged(void);

private:
    bool m_initialized;
    vtkSmartPointer<vtkProp> m_object = nullptr;
    QList<xVisViewer*> m_viewers;
};
