#include "xVisMapper2D.hpp"

xVisMapper2D::xVisMapper2D(vtkSmartPointer<vtkMapper2D> vtkObject) : xVisAbstractMapper(vtkObject) {
}

auto xVisMapper2D::get() -> vtkSmartPointer<vtkMapper2D> {
    return vtkMapper2D::SafeDownCast(xVisAlgorithm::get());
}
