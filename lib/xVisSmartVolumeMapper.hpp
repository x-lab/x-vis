#pragma once

#include "xVisVolumeMapper.hpp"

#include <vtkSmartVolumeMapper.h>

class xVisSmartVolumeMapper : public xVisVolumeMapper {
    Q_OBJECT
    Q_PROPERTY(qreal finalColorWindow READ getFinalColorWindow WRITE setFinalColorWindow NOTIFY finalColorWindowChanged);
    Q_PROPERTY(qreal finalColorLevel READ getFinalColorLevel WRITE setFinalColorLevel NOTIFY finalColorLevelChanged);
private:
    vtkSmartPointer<vtkSmartVolumeMapper> m_object = nullptr;
public:
    xVisSmartVolumeMapper();
    auto setFinalColorWindow(qreal) -> void;
    auto getFinalColorWindow() -> qreal;
    auto setFinalColorLevel(qreal) -> void;
    auto getFinalColorLevel() -> qreal;
signals:
    void finalColorWindowChanged();
    void finalColorLevelChanged();
};
