#include "xVisStripper.hpp"

xVisStripper::xVisStripper() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkStripper>::New()) {
    this->m_object = vtkStripper::SafeDownCast(xVisAlgorithm::get());
}

auto xVisStripper::setMaximumLength(int maximumLength) -> void {
    this->m_object->SetMaximumLength(maximumLength);
    this->update();
    emit this->passThroughCellIdsChanged();
}

auto xVisStripper::getMaximumLength() -> int {
    return this->m_object->GetMaximumLength();
}

auto xVisStripper::setPassCellDataAsFieldData(bool passCellDataAsFieldData) -> void {
    this->m_object->SetPassCellDataAsFieldData(passCellDataAsFieldData);
    this->update();
    emit this->passCellDataAsFieldDataChanged();
}

auto xVisStripper::getPassCellDataAsFieldData() -> bool {
    return this->m_object->GetPassCellDataAsFieldData();
}

auto xVisStripper::setPassThroughCellIds(bool passThroughCellIds) -> void {
    this->m_object->SetPassThroughCellIds(passThroughCellIds);
    this->update();
    emit this->passThroughCellIdsChanged();
}

auto xVisStripper::getPassThroughCellIds() -> bool {
    return this->m_object->GetPassThroughCellIds();
}

auto xVisStripper::setPassThroughPointIds(bool passThroughPointIds) -> void {
    this->m_object->SetPassThroughPointIds(passThroughPointIds);
    this->update();
    emit this->passThroughPointIdsChanged();
}

auto xVisStripper::getPassThroughPointIds() -> bool {
    return this->m_object->GetPassThroughPointIds();
}

auto xVisStripper::setJoinContiguousSegments(bool joinContiguousSegments) -> void {
    this->m_object->SetJoinContiguousSegments(joinContiguousSegments);
    this->update();
    emit this->joinContiguousSegmentsChanged();
}

auto xVisStripper::getJoinContiguousSegments() -> bool {
    return this->m_object->GetJoinContiguousSegments();
}
