#include "xVisRandomSequence.hpp"

xVisRandomSequence::xVisRandomSequence(vtkSmartPointer<vtkRandomSequence> vtkObject) : xVisObject(xVisObject::Type::Other), m_object(vtkObject) {
}

auto xVisRandomSequence::get() -> vtkSmartPointer<vtkRandomSequence> {
    return this->m_object;
}

auto xVisRandomSequence::setSeed(int seed) -> void {
    this->m_seed = seed;
    this->m_object->Initialize(seed);
}

auto xVisRandomSequence::getSeed() -> int {
    return this->m_seed;
}

auto xVisRandomSequence::initialize() -> void {
    if (this->m_object != nullptr) {
        this->m_object->Initialize(this->m_seed);
    }
}
