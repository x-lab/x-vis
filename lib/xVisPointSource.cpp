#include "xVisPointSource.hpp"

xVisPointSource::xVisPointSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkPointSource>::New()) {
    this->m_object = vtkPointSource::SafeDownCast(xVisAlgorithm::get());

    this->m_center = new xVisVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->resetSeed();
        this->update();
    });
}

auto xVisPointSource::setDistribution(Distribution distribution) -> void {
    this->m_object->SetDistribution(distribution);
    emit this->distributionChanged();
    this->resetSeed();
    this->update();
}

auto xVisPointSource::getDistribution() -> Distribution {
    return static_cast<Distribution>(this->m_object->GetDistribution());
}

auto xVisPointSource::setNumberOfPoints(int numberOfPoints) -> void {
    this->m_object->SetNumberOfPoints(numberOfPoints);
    emit this->numberOfPointsChanged();
    this->resetSeed();
    this->update();
}

auto xVisPointSource::getNumberOfPoints() -> int {
    return this->m_object->GetNumberOfPoints();
}

auto xVisPointSource::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    emit this->radiusChanged();
    this->resetSeed();
    this->update();
}

auto xVisPointSource::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

auto xVisPointSource::getCenter() -> xVisVector3* {
    return this->m_center;
}

auto xVisPointSource::setRandomSequence(xVisRandomSequence* randomSequence) -> void {
    this->m_randomSequence = randomSequence;

    if (randomSequence == nullptr) {
        this->m_object->SetRandomSequence(nullptr);
    } else {
        this->m_object->SetRandomSequence(randomSequence->get());
        randomSequence->initialize();
    }

    emit this->randomSequenceChanged();
    this->update();
}

auto xVisPointSource::getRandomSequence() -> xVisRandomSequence* {
    return this->m_randomSequence;
}

auto xVisPointSource::resetSeed() -> void {
    if (this->m_randomSequence != nullptr) {
        this->m_randomSequence->initialize();
    }
}
