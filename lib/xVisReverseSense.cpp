#include "xVisReverseSense.hpp"

xVisReverseSense::xVisReverseSense() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkReverseSense>::New()) {
    this->m_object = vtkReverseSense::SafeDownCast(xVisAlgorithm::get());
}

auto xVisReverseSense::setReverseCells(bool reverseCells) -> void {
    this->m_object->SetReverseCells(reverseCells);
    this->update();
    emit this->reverseCellsChanged();
}

auto xVisReverseSense::getReverseCells() -> bool {
    return this->m_object->GetReverseCells();
}

auto xVisReverseSense::setReverseNormals(bool reverseNormals) -> void {
    this->m_object->SetReverseNormals(reverseNormals);
    this->update();
    emit this->reverseNormalsChanged();
}

auto xVisReverseSense::getReverseNormals() -> bool {
    return this->m_object->GetReverseNormals();
}
