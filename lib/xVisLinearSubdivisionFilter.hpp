#pragma once

#include "xVisInterpolatingSubdivisionFilter.hpp"

#include <vtkLinearSubdivisionFilter.h>

class xVisLinearSubdivisionFilter : public xVisInterpolatingSubdivisionFilter {
    Q_OBJECT
public:
    xVisLinearSubdivisionFilter();
};
