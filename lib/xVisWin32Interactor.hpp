#pragma once

#if defined(Q_OS_WIN32)

#include <vtkWin32RenderWindowInteractor.h>

class xVisWin32Interactor : public vtkWin32RenderWindowInteractor
{
public:
    vtkTypeMacro(xVisWin32Interactor, vtkWin32RenderWindowInteractor);
    static xVisWin32Interactor *New(void);
    void Initialize(void) override;

protected:
             xVisWin32Interactor(void) = default;
    virtual ~xVisWin32Interactor(void) = default;
};

#endif
