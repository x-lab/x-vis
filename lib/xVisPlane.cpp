#include "xVisPlane.hpp"

xVisPlane::xVisPlane() : xVisImplicitFunction(vtkSmartPointer<vtkPlane>::New()) {
    this->m_object = vtkPlane::SafeDownCast(xVisImplicitFunction::get());

    this->m_origin = new xVisVector3([this](){
        this->m_object->SetOrigin(this->m_origin->getValues().data());
        this->update();
    });

    this->m_normal = new xVisVector3([this](){
        this->m_object->SetNormal(this->m_normal->getValues().data());
    });
}

auto xVisPlane::getOrigin() -> xVisVector3* {
    return this->m_origin;
}

auto xVisPlane::getNormal() -> xVisVector3* {
    return this->m_origin;
}
