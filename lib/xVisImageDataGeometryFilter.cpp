#include "xVisImageDataGeometryFilter.hpp"

xVisImageDataGeometryFilter::xVisImageDataGeometryFilter() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkImageDataGeometryFilter>::New())
{
    this->m_object = vtkImageDataGeometryFilter::SafeDownCast(xVisAlgorithm::get());
}
