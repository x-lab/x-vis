#include "xVisCone.hpp"

xVisCone::xVisCone() : xVisImplicitFunction(vtkSmartPointer<vtkCone>::New()) {
    this->m_object = vtkCone::SafeDownCast(xVisImplicitFunction::get());
}

auto xVisCone::setAngle(qreal angle) -> void {
    this->m_object->SetAngle(angle);
    emit this->angleChanged();
}

auto xVisCone::getAngle() -> qreal {
    return this->m_object->GetAngle();
}
