#pragma once

#include "xVisVector3.hpp"
#include "xVisPolyDataAlgorithm.hpp"

#include <vtkEllipseArcSource.h>

class xVisEllipseArcSource : public xVisPolyDataAlgorithm
{
    Q_OBJECT
    Q_PROPERTY(xVisVector3* center READ getCenter CONSTANT);
    Q_PROPERTY(xVisVector3* normal READ getNormal CONSTANT);
    Q_PROPERTY(xVisVector3* majorRadiusVector READ getMajorRadiusVector CONSTANT);
    Q_PROPERTY(qreal startAngle READ getStartAngle WRITE setStartAngle NOTIFY startAngleChanged);
    Q_PROPERTY(qreal segmentAngle READ getSegmentAngle WRITE setSegmentAngle NOTIFY segmentAngleChanged);
    Q_PROPERTY(int resolution READ getResolution WRITE setResolution NOTIFY resolutionChanged);
    Q_PROPERTY(qreal ratio READ getRatio WRITE setRatio NOTIFY ratioChanged);

public:
    xVisEllipseArcSource();
    auto getCenter() -> xVisVector3*;
    auto getNormal() -> xVisVector3*;
    auto getMajorRadiusVector() -> xVisVector3*;
    auto setStartAngle(qreal) -> void;
    auto getStartAngle() -> qreal;
    auto setSegmentAngle(qreal) -> void;
    auto getSegmentAngle() -> qreal;
    auto setResolution(int) -> void;
    auto getResolution() -> int;
    auto setRatio(qreal) -> void;
    auto getRatio() -> qreal;

signals:
    void startAngleChanged();
    void segmentAngleChanged();
    void resolutionChanged();
    void ratioChanged();

private:
    vtkSmartPointer<vtkEllipseArcSource> m_object = nullptr;
    xVisVector3* m_center = nullptr;
    xVisVector3* m_normal = nullptr;
    xVisVector3* m_majorRadiusVector = nullptr;
};
