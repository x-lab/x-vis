#include "xVisVectorText.hpp"

xVisVectorText::xVisVectorText() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkVectorText>::New()) {
    this->m_object = vtkVectorText::SafeDownCast(xVisAlgorithm::get());
}

auto xVisVectorText::setText(const QString& text) -> void {
    this->m_object->SetText(text.toStdString().c_str());
    this->update();
    emit this->textChanged();
}

auto xVisVectorText::getText() -> QString {
    return this->m_object->GetText();
}
