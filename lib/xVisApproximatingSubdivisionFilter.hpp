#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkApproximatingSubdivisionFilter.h>

class xVisApproximatingSubdivisionFilter : public xVisPolyDataAlgorithm
{
    Q_OBJECT
    Q_PROPERTY(int numberOfSubdivisions READ getNumberOfSubdivisions WRITE setNumberOfSubdivisions NOTIFY numberOfSubdivisionsChanged);

public:
    xVisApproximatingSubdivisionFilter(vtkSmartPointer<vtkApproximatingSubdivisionFilter>);
    auto setNumberOfSubdivisions(int) -> void;
    auto getNumberOfSubdivisions() -> int;

signals:
    void numberOfSubdivisionsChanged();

private:
    vtkSmartPointer<vtkApproximatingSubdivisionFilter> m_object = nullptr;
};
