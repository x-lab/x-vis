#pragma once

#include "xVisPolyDataAlgorithm.hpp"
#include "xVisVector3.hpp"

#include <vtkLinearExtrusionFilter.h>

class xVisLinearExtrusionFilter : public xVisPolyDataAlgorithm {
    Q_OBJECT
public:
    enum ExtrusionType
    {
        VectorExtrusion = VTK_VECTOR_EXTRUSION,
        NormalExtrusion = VTK_NORMAL_EXTRUSION,
        PointExtrusion = VTK_POINT_EXTRUSION
    };
private:
    Q_ENUM(ExtrusionType);
    Q_PROPERTY(xVisVector3* vector READ getVector CONSTANT);
    Q_PROPERTY(xVisVector3* extrusionPoint READ getExtrusionPoint CONSTANT);
    Q_PROPERTY(ExtrusionType extrusionType READ getExtrusionType WRITE setExtrusionType NOTIFY extrusionTypeChanged);
    Q_PROPERTY(qreal scaleFactor READ getScaleFactor WRITE setScaleFactor NOTIFY scaleFactorChanged);
    Q_PROPERTY(bool capping READ getCapping WRITE setCapping NOTIFY cappingChanged);
private:
    vtkSmartPointer<vtkLinearExtrusionFilter> m_object = nullptr;
    xVisVector3* m_extrusionPoint = nullptr;
    xVisVector3* m_vector = nullptr;
public:
    xVisLinearExtrusionFilter();
    auto setExtrusionType(ExtrusionType) -> void;
    auto getExtrusionType() -> ExtrusionType;
    auto setScaleFactor(qreal) -> void;
    auto getScaleFactor() -> qreal;
    auto setCapping(bool) -> void;
    auto getCapping() -> bool;
    auto getVector() -> xVisVector3*;
    auto getExtrusionPoint() -> xVisVector3*;
signals:
    void extrusionTypeChanged();
    void scaleFactorChanged();
    void cappingChanged();
};
