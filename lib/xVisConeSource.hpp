#pragma once

#include "xVisPolyDataAlgorithm.hpp"
#include "xVisVector3.hpp"

#include <vtkConeSource.h>

class xVisConeSource : public xVisPolyDataAlgorithm {
  Q_OBJECT
  Q_PROPERTY(xVisVector3 *center READ getCenter CONSTANT);
  Q_PROPERTY(xVisVector3 *direction READ getDirection CONSTANT);
  Q_PROPERTY(qreal height READ getHeight WRITE setHeight NOTIFY heightChanged);
  Q_PROPERTY(qreal radius READ getRadius WRITE setRadius NOTIFY radiusChanged);
  Q_PROPERTY(qreal angle READ getAngle WRITE setAngle NOTIFY angleChanged);
  Q_PROPERTY(int resolution READ getResolution WRITE setResolution NOTIFY
                 resolutionChanged)
  Q_PROPERTY(
      bool capping READ getCapping WRITE setCapping NOTIFY cappingChanged);

public:
  xVisConeSource();
  auto getCenter() -> xVisVector3 *;
  auto getDirection() -> xVisVector3 *;
  auto setHeight(qreal) -> void;
  auto getHeight() -> qreal;
  auto setRadius(qreal) -> void;
  auto getRadius() -> qreal;
  auto setAngle(qreal) -> void;
  auto getAngle() -> qreal;
  auto setResolution(int) -> void;
  auto getResolution() -> int;
  auto setCapping(bool) -> void;
  auto getCapping() -> bool;

signals:
  void heightChanged();
  void angleChanged();
  void radiusChanged();
  void resolutionChanged();
  void cappingChanged();

private:
  vtkSmartPointer<vtkConeSource> m_object = nullptr;
  xVisVector3 *m_center = nullptr;
  xVisVector3 *m_direction = nullptr;
};
