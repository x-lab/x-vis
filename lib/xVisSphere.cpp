#include "xVisSphere.hpp"

xVisSphere::xVisSphere() : xVisImplicitFunction(vtkSmartPointer<vtkSphere>::New()) {
    this->m_object = vtkSphere::SafeDownCast(xVisImplicitFunction::get());

    this->m_center = new xVisVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });
}

auto xVisSphere::getCenter() -> xVisVector3* {
    return this->m_center;
}

qreal xVisSphere::evaluateFunction(qreal x, qreal y, qreal z) {
    return this->m_object->EvaluateFunction(x, y, z);
}

auto xVisSphere::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    emit this->radiusChanged();
}

auto xVisSphere::getRadius() -> qreal {
    return this->m_object->GetRadius();
}
