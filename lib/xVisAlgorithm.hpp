#pragma once

#include "xVisObject.hpp"

#include <vtkSmartPointer.h>
#include <vtkAlgorithm.h>

class xVisProp;
class xVisDataObject;

class xVisAlgorithm : public xVisObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<xVisAlgorithm> input READ getInput NOTIFY inputChanged);
    Q_CLASSINFO("DefaultProperty", "input");

public:
     xVisAlgorithm(void) = delete;
     xVisAlgorithm(vtkSmartPointer<vtkAlgorithm>);
    ~xVisAlgorithm(void);

    auto setProp(xVisProp*) -> void;
    auto setVtkAlgorithm(vtkSmartPointer<vtkAlgorithm>) -> void;
    auto getInput() -> QQmlListProperty<xVisAlgorithm>;
    auto update() -> void;
    auto get() -> vtkSmartPointer<vtkAlgorithm>;
    virtual auto isValid() -> bool;
    static auto appendInput(QQmlListProperty<xVisAlgorithm>*, xVisAlgorithm*) -> void;
#if QT_VERSION >= 0x060000
    static auto inputCount(QQmlListProperty<xVisAlgorithm>*) -> qsizetype;
#else
    static auto inputCount(QQmlListProperty<xVisAlgorithm>*) -> int;
#endif
#if QT_VERSION >= 0x060000
    static auto inputAt(QQmlListProperty<xVisAlgorithm>*, qsizetype) -> xVisAlgorithm*;
#else
    static auto inputAt(QQmlListProperty<xVisAlgorithm>*, int) -> xVisAlgorithm*;
#endif
    static auto clearInputs(QQmlListProperty<xVisAlgorithm>*) -> void;

signals:
    void inputChanged(void);

private:
    QList<xVisAlgorithm*> m_input;
    vtkSmartPointer<vtkAlgorithm> m_object = nullptr;
    xVisProp* m_prop = nullptr;
};
