#include "xVisSampleFunction.hpp"

xVisSampleFunction::xVisSampleFunction(void) : xVisImageAlgorithm(vtkSmartPointer<vtkSampleFunction>::New())
{
    this->m_object = vtkSampleFunction::SafeDownCast(xVisImageAlgorithm::get());

    this->m_sampleDimension = new xVisVector3([this](){
        this->m_object->SetSampleDimensions(this->m_sampleDimension->getX(), this->m_sampleDimension->getY(), this->m_sampleDimension->getZ());
        this->update();
    });

}

xVisSampleFunction::xVisSampleFunction(vtkSmartPointer<vtkSampleFunction> object) : xVisImageAlgorithm(object)
{
    this->m_object = vtkSampleFunction::SafeDownCast(xVisImageAlgorithm::get());
}

auto xVisSampleFunction::get(void) -> vtkSmartPointer<vtkSampleFunction>
{
    return this->m_object;
}

xVisSampleFunction::~xVisSampleFunction(void)
{
    this->m_object = nullptr;
}
