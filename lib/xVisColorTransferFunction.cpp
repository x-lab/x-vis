#include "xVisColorTransferFunction.hpp"

xVisColorTransferFunction::xVisColorTransferFunction(vtkSmartPointer<vtkColorTransferFunction> vtkObject, cb_t&& callback) : m_object(vtkObject), m_callback(callback)
{

}

void xVisColorTransferFunction::clear() {
    this->m_colors.clear();
    this->m_values.clear();

    emit this->sizeChanged();

    this->m_object->RemoveAllPoints();
    this->update();
}

void xVisColorTransferFunction::add(const QColor& color, double value) {
    this->m_colors.append(color);
    this->m_values.append(value);

    emit this->sizeChanged();

    this->m_object->AddRGBPoint(value, color.redF(), color.greenF(), color.blueF());
    this->update();
}

double xVisColorTransferFunction::getValue(int i) {
    return i < this->m_values.length() && i >= 0 ? this->m_values.at(i) : 0;
}

QColor xVisColorTransferFunction::getColor(int i) {
    return i < this->m_colors.length() && i >= 0 ? QColor(this->m_colors.at(i)) : QColor("#ff00ff");
}

auto xVisColorTransferFunction::update() -> void {
    this->m_callback();
}

auto xVisColorTransferFunction::getValues() -> QList<double> {
    return this->m_values;
}

auto xVisColorTransferFunction::getColors() -> QList<QColor> {
    return this->m_colors;
}

auto xVisColorTransferFunction::setClamping(bool clamping) -> void {
    this->m_object->SetClamping(clamping);
    this->update();
}

auto xVisColorTransferFunction::getClamping() -> bool {
    return this->m_object->GetClamping();
}

auto xVisColorTransferFunction::getSize() -> int {
    return this->m_values.length();
}
