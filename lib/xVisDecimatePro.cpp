#include "xVisDecimatePro.hpp"

xVisDecimatePro::xVisDecimatePro() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkDecimatePro>::New()) {
    this->m_object = vtkDecimatePro::SafeDownCast(xVisAlgorithm::get());
}

auto xVisDecimatePro::setDegree(int val) -> void {
    this->m_object->SetDegree(val);
    emit this->degreeChanged();
    this->update();
}

auto xVisDecimatePro::getDegree() -> int {
    return this->m_object->GetDegree();
}

auto xVisDecimatePro::setSplitting(bool val) -> void {
    this->m_object->SetSplitting(val);
    emit this->splittingChanged();
    this->update();
}

auto xVisDecimatePro::getSplitting() -> bool {
    return this->m_object->GetSplitting();
}

auto xVisDecimatePro::setPreSplitMesh(bool val) -> void {
    this->m_object->SetPreSplitMesh(val);
    emit this->preSplitMeshChanged();
    this->update();
}

auto xVisDecimatePro::getPreSplitMesh() -> bool {
    return this->m_object->GetPreSplitMesh();
}

auto xVisDecimatePro::setSplitAngle(qreal val) -> void {
    this->m_object->SetSplitAngle(val);
    emit this->splittingChanged();
    this->update();
}

auto xVisDecimatePro::getSplitAngle() -> qreal {
    return this->m_object->GetSplitAngle();
}

auto xVisDecimatePro::setFeatureAngle(qreal val) -> void {
    this->m_object->SetFeatureAngle(val);
    emit this->featureAngleChanged();
    this->update();
}

auto xVisDecimatePro::getFeatureAngle() -> qreal {
    return this->m_object->GetFeatureAngle();
}

auto xVisDecimatePro::setPreserveTopology(bool val) -> void {
    this->m_object->SetPreserveTopology(val);
    emit this->preserveTopologyChanged();
    this->update();
}

auto xVisDecimatePro::getPreserveTopology() -> bool {
    return this->m_object->GetPreserveTopology();
}

auto xVisDecimatePro::setTargetReduction(qreal val) -> void {
    this->m_object->SetTargetReduction(val);
    emit this->targetReductionChanged();
    this->update();
}

auto xVisDecimatePro::getTargetReduction() -> qreal {
    return this->m_object->GetTargetReduction();
}
