#pragma once

#include "xVisAbstractPolyDataReader.hpp"

#include <vtkPLYReader.h>

class xVisPLYReader : public xVisAbstractPolyDataReader {
    Q_OBJECT
public:
    xVisPLYReader();
};
