#pragma once

#include "xVisAlgorithm.hpp"

#include <vtkPointSetAlgorithm.h>

class xVisPointSetAlgorithm : public xVisAlgorithm {
    Q_OBJECT
public:
    xVisPointSetAlgorithm(vtkSmartPointer<vtkPointSetAlgorithm>);
};
