#pragma once

#include "xVisAbstractPolyDataReader.hpp"

#include <vtkSTLReader.h>

class xVisSTLReader : public xVisAbstractPolyDataReader {
    Q_OBJECT
public:
    xVisSTLReader();
};
