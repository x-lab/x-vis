#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkAdaptiveSubdivisionFilter.h>

class xVisAdaptiveSubdivisionFilter : public xVisPolyDataAlgorithm
{
    Q_OBJECT
    Q_PROPERTY(qreal maximumEdgeLength READ getMaximumEdgeLength WRITE setMaximumEdgeLength NOTIFY maximumEdgeLengthChanged);
    Q_PROPERTY(qreal maximumTriangleArea READ getMaximumTriangleArea WRITE setMaximumTriangleArea NOTIFY maximumTriangleAreaChanged);
    Q_PROPERTY(int maximumNumberOfTriangles READ getMaximumNumberOfTriangles WRITE setMaximumNumberOfTriangles NOTIFY maximumNumberOfTrianglesChanged);
    Q_PROPERTY(int maximumNumberOfPasses READ getMaximumNumberOfPasses WRITE setMaximumNumberOfPasses NOTIFY maximumNumberOfPassesChanged);

public:
    xVisAdaptiveSubdivisionFilter(void);

    auto setMaximumEdgeLength(qreal) -> void;
    auto getMaximumEdgeLength() -> qreal;
    auto setMaximumTriangleArea(qreal) -> void;
    auto getMaximumTriangleArea() -> qreal;
    auto setMaximumNumberOfTriangles(int) -> void;
    auto getMaximumNumberOfTriangles() -> int;
    auto setMaximumNumberOfPasses(int) -> void;
    auto getMaximumNumberOfPasses() -> int;

signals:
    void maximumEdgeLengthChanged(void);
    void maximumTriangleAreaChanged(void);
    void maximumNumberOfTrianglesChanged(void);
    void maximumNumberOfPassesChanged(void);

private:
    vtkSmartPointer<vtkAdaptiveSubdivisionFilter> m_object = nullptr;
};
