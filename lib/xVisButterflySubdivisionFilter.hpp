#pragma once

#include "xVisInterpolatingSubdivisionFilter.hpp"

#include <vtkButterflySubdivisionFilter.h>

class xVisButterflySubdivisionFilter : public xVisInterpolatingSubdivisionFilter
{
    Q_OBJECT

public:
    xVisButterflySubdivisionFilter();
};
