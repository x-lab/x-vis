#include "xVisProp3D.hpp"

// Qml::Register::AbstractClass<xVisProp3D> xVisProp3D::Register(true);

xVisProp3D::xVisProp3D(vtkSmartPointer<vtkProp3D> vtkObject) : xVisProp(vtkObject)
{
    this->m_object = vtkProp3D::SafeDownCast(vtkObject);

    this->m_position = new xVisVector3([this] ()
    {
        this->m_object->SetPosition(this->m_position->getValues().data());
        this->update();
    });

    this->m_origin = new xVisVector3([this] ()
    {
        this->m_object->SetOrigin(this->m_origin->getValues().data());
        this->update();
    });

    this->m_orientation = new xVisVector3([this]()
    {
        this->m_object->SetOrientation(this->m_orientation->getValues().data());
        this->update();
    });

    this->m_scale = new xVisVector3([this] ()
    {
        this->m_object->SetScale(this->m_scale->getValues().data());
        this->update();
    });
}

xVisVector3 *xVisProp3D::getPosition(void)
{
    return this->m_position;
}

xVisVector3 *xVisProp3D::getOrientation(void)
{
    return this->m_orientation;
}

xVisVector3 *xVisProp3D::getOrigin(void)
{
    return this->m_origin;
}

xVisVector3 *xVisProp3D::getScale(void)
{
    return this->m_scale;
}
