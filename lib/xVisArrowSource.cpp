#include "xVisArrowSource.hpp"

xVisArrowSource::xVisArrowSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkArrowSource>::New()) {
    this->m_object = vtkArrowSource::SafeDownCast(xVisAlgorithm::get());
}

auto xVisArrowSource::setTipResolution(int tipResolution) -> void {
    this->m_object->SetTipResolution(tipResolution);
    emit this->tipResolutionChanged();
    this->update();
}

auto xVisArrowSource::getTipResolution() -> int {
    return this->m_object->GetTipResolution();
}

auto xVisArrowSource::setTipLength(qreal tipLength) -> void {
    this->m_object->SetTipLength(tipLength);
    emit this->tipLengthChanged();
    this->update();
}

auto xVisArrowSource::getTipLength() -> qreal {
    return this->m_object->GetTipLength();
}

auto xVisArrowSource::setTipRadius(qreal tipRadius) -> void {
    this->m_object->SetTipRadius(tipRadius);
    emit this->tipRadiusChanged();
    this->update();
}

auto xVisArrowSource::getTipRadius() -> qreal {
    return this->m_object->GetTipRadius();
}

auto xVisArrowSource::setShaftResolution(int shaftResolution) -> void {
    this->m_object->SetShaftResolution(shaftResolution);
    emit this->shaftResolutionChanged();
    this->update();
}

auto xVisArrowSource::getShaftResolution() -> int {
    return this->m_object->GetShaftResolution();
}

auto xVisArrowSource::setShaftRadius(qreal shaftRadius) -> void {
    this->m_object->SetShaftRadius(shaftRadius);
    emit this->shaftRadiusChanged();
    this->update();
}

auto xVisArrowSource::getShaftRadius() -> qreal {
    return this->m_object->GetShaftRadius();
}

auto xVisArrowSource::setInvert(bool invert) -> void {
    this->m_object->SetInvert(invert);
    emit this->invertChanged();
    this->update();
}

auto xVisArrowSource::getInvert() -> bool {
    return this->m_object->GetInvert();
}
