#include "xVisProperty.hpp"
#include "xVisActor.hpp"

xVisProperty::xVisProperty(xVisActor* actor) : m_actor(actor), m_vtkActor(actor->get()) {
}

auto xVisProperty::update() -> void {
    this->m_actor->update();
}

auto xVisProperty::setDiffuseColor(const QColor& color) -> void {
    this->m_diffuseColor = color;

    this->m_vtkActor->GetProperty()->SetDiffuseColor(color.redF(), color.greenF(), color.blueF());
    this->update();

    emit this->diffuseColorChanged();
}

auto xVisProperty::setSpecularColor(const QColor& color) -> void {
    this->m_specularColor = color;

    this->m_vtkActor->GetProperty()->SetSpecularColor(color.redF(), color.greenF(), color.blueF());
    this->update();

    emit this->specularColorChanged();
}

auto xVisProperty::setAmbientColor(const QColor& color) -> void {
    this->m_ambientColor = color;

    this->m_vtkActor->GetProperty()->SetAmbientColor(color.redF(), color.greenF(), color.blueF());
    this->update();

    emit this->ambientColorChanged();
}

auto xVisProperty::setEdgeColor(const QColor& color) -> void {
    this->m_edgeColor = color;

    this->m_vtkActor->GetProperty()->SetEdgeColor(color.redF(), color.greenF(), color.blueF());
    this->update();

    emit this->edgeColorChanged();
}

auto xVisProperty::setFrontfaceCulling(bool enabled) -> void {
    this->m_vtkActor->GetProperty()->SetFrontfaceCulling(enabled);
    emit this->frontfaceCullingChanged();
    this->update();
}

auto xVisProperty::setBackfaceCulling(bool enabled) -> void {
    this->m_vtkActor->GetProperty()->SetBackfaceCulling(enabled);
    emit this->backfaceCullingChanged();
    this->update();
}

auto xVisProperty::setEdgeVisibility(bool visible) -> void {
    this->m_vtkActor->GetProperty()->SetEdgeVisibility(visible);
    this->edgeVisibilityChanged();
    this->update();
}

auto xVisProperty::setLighting(bool lighting) -> void {
    this->m_vtkActor->GetProperty()->SetLighting(lighting);
    emit this->lightingChanged();
    this->update();
}

auto xVisProperty::setShading(bool shading) -> void {
    this->m_vtkActor->GetProperty()->SetShading(shading);
    emit shadingChanged();
    this->update();
}

void xVisProperty::setOpacity(qreal opacity) {
    this->m_vtkActor->GetProperty()->SetOpacity(opacity);
    emit this->opacityChanged();
    this->update();
}

auto xVisProperty::setLineWidth(qreal lineWidth) -> void {
    this->m_vtkActor->GetProperty()->SetLineWidth(lineWidth);
    emit this->lineWidthChanged();
    this->update();
}

auto xVisProperty::setPointSize(qreal pointSize) -> void {
    this->m_vtkActor->GetProperty()->SetPointSize(pointSize);
    emit this->pointSizeChanged();
    this->update();
}

auto xVisProperty::setInterpolation(Interpolation interpolation) -> void {
    this->m_vtkActor->GetProperty()->SetInterpolation(interpolation);
    emit this->interpolationChanged();
    this->update();
}

auto xVisProperty::getInterpolation() -> Interpolation {
    return static_cast<Interpolation>(this->m_vtkActor->GetProperty()->GetInterpolation());
}

auto xVisProperty::setRepresentation(Representation representation) -> void {
    this->m_vtkActor->GetProperty()->SetRepresentation(representation);
    emit this->representationChanged();
    this->update();
}

auto xVisProperty::getRepresentation() -> Representation {
    return static_cast<Representation>(this->m_vtkActor->GetProperty()->GetRepresentation());
}

auto xVisProperty::setAmbient(qreal ambient) -> void {
    this->m_vtkActor->GetProperty()->SetAmbient(ambient);
    emit this->ambientChanged();
    this->update();
}

auto xVisProperty::setDiffuse(qreal diffuse) -> void {
    this->m_vtkActor->GetProperty()->SetDiffuse(diffuse);
    emit this->diffuseChanged();
    this->update();
}

auto xVisProperty::setSpecular(qreal specular) -> void {
    this->m_vtkActor->GetProperty()->SetSpecular(specular);
    emit this->specularChanged();
    this->update();
}

auto xVisProperty::setSpecularPower(qreal specularPower) -> void {
    this->m_vtkActor->GetProperty()->SetSpecularPower(specularPower);
    emit this->specularPowerChanged();
    this->update();
}

auto xVisProperty::getDiffuseColor() -> QColor {
    return this->m_diffuseColor;
}

auto xVisProperty::getSpecularColor() -> QColor {
    return this->m_specularColor;
}

auto xVisProperty::getAmbientColor() -> QColor {
    return this->m_ambientColor;
}

auto xVisProperty::getEdgeColor() -> QColor {
    return this->m_edgeColor;
}

auto xVisProperty::getEdgeVisibility() -> bool {
    return this->m_vtkActor->GetProperty()->GetEdgeVisibility();
}

auto xVisProperty::getLighting() -> bool {
    return this->m_vtkActor->GetProperty()->GetLighting();
}

auto xVisProperty::getShading() -> bool {
    return this->m_vtkActor->GetProperty()->GetShading();
}

auto xVisProperty::getFrontfaceCulling() -> bool {
    return this->m_vtkActor->GetProperty()->GetFrontfaceCulling();
}

auto xVisProperty::getBackfaceCulling() -> bool {
    return this->m_vtkActor->GetProperty()->GetBackfaceCulling();
}

auto xVisProperty::getOpacity() -> qreal {
    return this->m_vtkActor->GetProperty()->GetOpacity();
}

auto xVisProperty::getLineWidth() -> qreal {
    return this->m_vtkActor->GetProperty()->GetLineWidth();
}

auto xVisProperty::getPointSize() -> qreal {
    return this->m_vtkActor->GetProperty()->GetPointSize();
}

auto xVisProperty::getAmbient() -> qreal {
    return this->m_vtkActor->GetProperty()->GetAmbient();
}

auto xVisProperty::getDiffuse() -> qreal {
    return this->m_vtkActor->GetProperty()->GetDiffuse();
}

auto xVisProperty::getSpecular() -> qreal {
    return this->m_vtkActor->GetProperty()->GetSpecular();
}

auto xVisProperty::getSpecularPower() -> qreal {
    return this->m_vtkActor->GetProperty()->GetSpecularPower();
}
