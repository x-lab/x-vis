#include "xVisImageMapper.hpp"

xVisImageMapper::xVisImageMapper() : xVisMapper2D(vtkSmartPointer<vtkImageMapper>::New()) {
    this->m_object = vtkImageMapper::SafeDownCast(xVisAlgorithm::get());
}

auto xVisImageMapper::setColorWindow(qreal colorWindow) -> void {
    this->m_object->SetColorWindow(colorWindow);
    emit this->colorWindowChanged();
    emit this->colorScaleChanged();
    emit this->colorShiftChanged();

    this->update();
}

auto xVisImageMapper::getColorWindow() -> qreal {
    return this->m_object->GetColorWindow();
}

auto xVisImageMapper::setColorLevel(qreal colorLevel) -> void {
    this->m_object->SetColorLevel(colorLevel);
    emit this->colorLevelChanged();
    emit this->colorScaleChanged();
    emit this->colorShiftChanged();

    this->update();
}

auto xVisImageMapper::getColorLevel() -> qreal {
    return this->m_object->GetColorLevel();
}

auto xVisImageMapper::getColorShift() -> qreal {
    return this->m_object->GetColorShift();
}

auto xVisImageMapper::getColorScale() -> qreal {
    return this->m_object->GetColorScale();
}
