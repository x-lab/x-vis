#pragma once

#include "xVisObject.hpp"

#include <QList>

#include <vtkImplicitFunction.h>
#include <vtkSmartPointer.h>

class xVisImplicitFunction : public xVisObject {
  Q_OBJECT
public:
  using cb_t = std::function<void(void)>;

private:
  vtkSmartPointer<vtkImplicitFunction> m_object = nullptr;
  QList<cb_t *> m_callbacks;

public:
  xVisImplicitFunction() = delete;
  xVisImplicitFunction(vtkSmartPointer<vtkImplicitFunction>);
  ~xVisImplicitFunction();
  auto update() -> void;
  auto addCallback(cb_t &&) -> void;
  auto removeCallback(cb_t &&) -> void;
  auto get() -> vtkSmartPointer<vtkImplicitFunction>;
};
