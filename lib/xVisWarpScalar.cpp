#include "xVisWarpScalar.hpp"

xVisWarpScalar::xVisWarpScalar() : xVisPointSetAlgorithm(vtkSmartPointer<vtkWarpScalar>::New()) {
    this->m_object = vtkWarpScalar::SafeDownCast(xVisAlgorithm::get());
}

auto xVisWarpScalar::setScaleFactor(qreal scaleFactor) -> void {
    this->m_object->SetScaleFactor(scaleFactor);
    emit this->scaleFactorChanged();
    this->update();
}

auto xVisWarpScalar::getScaleFactor() -> qreal {
    return this->m_object->GetScaleFactor();
}

auto xVisWarpScalar::setUseNormal(bool useNormal) -> void {
    this->m_object->SetUseNormal(useNormal);
    emit this->useNormalChanged();
    this->update();
}

auto xVisWarpScalar::getUseNormal() -> bool {
    return this->m_object->GetUseNormal();
}
