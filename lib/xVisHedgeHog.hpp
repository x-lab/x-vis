#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkHedgeHog.h>

class xVisHedgeHog : public xVisPolyDataAlgorithm
{
    Q_OBJECT

public:
    enum VectorMode {
        UseVector = VTK_USE_VECTOR,
        UseNormal = VTK_USE_NORMAL
    };
    Q_ENUM(VectorMode);
    Q_PROPERTY(qreal scaleFactor READ getScaleFactor WRITE setScaleFactor NOTIFY scaleFactorChanged);
    Q_PROPERTY(VectorMode vectorMode READ getVectorMode WRITE setVectorMode NOTIFY vectorModeChanged);

private:
    vtkSmartPointer<vtkHedgeHog> m_object = nullptr;

public:
    xVisHedgeHog();
    auto setScaleFactor(qreal) -> void;
    auto getScaleFactor() -> qreal;
    auto setVectorMode(VectorMode) -> void;
    auto getVectorMode() -> VectorMode;

signals:
    void scaleFactorChanged();
    void vectorModeChanged();
};
