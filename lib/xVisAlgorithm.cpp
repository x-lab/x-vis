#include "xVisAlgorithm.hpp"

#include "xVisProp.hpp"

xVisAlgorithm::xVisAlgorithm(vtkSmartPointer<vtkAlgorithm> vtkObject) : xVisObject(xVisObject::Type::Algorithm), m_object(vtkObject)
{

}

xVisAlgorithm::~xVisAlgorithm(void)
{
    this->m_prop = nullptr;
}

auto xVisAlgorithm::setVtkAlgorithm(vtkSmartPointer<vtkAlgorithm> vtkObject) -> void
{
    this->m_object = vtkObject;
}

auto xVisAlgorithm::get() -> vtkSmartPointer<vtkAlgorithm>
{
    return this->m_object;
}

auto xVisAlgorithm::setProp(xVisProp *prop) -> void
{
    this->m_prop = prop;

    for (auto alg : this->m_input) {
        alg->setProp(prop);
    }
}

auto xVisAlgorithm::update() -> void
{
    if (this->m_input.empty()) {
        if (this->m_prop) {
            this->m_prop->update();
        }
    } else {
        for (xVisAlgorithm* alg : this->m_input) {
            alg->update();
        }
    }
}

auto xVisAlgorithm::isValid() -> bool
{
    return true;
}

auto xVisAlgorithm::getInput() -> QQmlListProperty<xVisAlgorithm>
{
    return QQmlListProperty<xVisAlgorithm>(this, 0, &appendInput, &inputCount, &inputAt, &clearInputs);
}

auto xVisAlgorithm::appendInput(QQmlListProperty<xVisAlgorithm>* list, xVisAlgorithm* algorithm) -> void
{
    auto parent = qobject_cast<xVisAlgorithm*>(list->object);

    if(parent && algorithm) {
        parent->m_input.append(algorithm);
        int count = parent->m_input.count() - 1;

        parent->get()->SetInputConnection(count, algorithm->get()->GetOutputPort());

        emit parent->inputChanged();

        parent->setProp(algorithm->m_prop);
    }
}

#if QT_VERSION >= 0x060000
auto xVisAlgorithm::inputCount(QQmlListProperty<xVisAlgorithm> *list) -> qsizetype
#else
auto xVisAlgorithm::inputCount(QQmlListProperty<xVisAlgorithm> *list) -> int
#endif
{
    auto parent = qobject_cast<xVisAlgorithm*>(list->object);

    if (parent) {
        return parent->m_input.count();
    }

    return 0;
}

#if QT_VERSION >= 0x060000
auto xVisAlgorithm::inputAt(QQmlListProperty<xVisAlgorithm> *list, qsizetype i) -> xVisAlgorithm *
#else
auto xVisAlgorithm::inputAt(QQmlListProperty<xVisAlgorithm> *list, int i) -> xVisAlgorithm *
#endif
{
    auto parent = qobject_cast<xVisAlgorithm *>(list->object);

    if (parent)
        return parent->m_input.at(i);

    return 0;
}

auto xVisAlgorithm::clearInputs(QQmlListProperty<xVisAlgorithm> *list) -> void
{
    auto parent = qobject_cast<xVisAlgorithm*>(list->object);

    if (parent) {
        parent->m_input.clear();
        emit parent->inputChanged();
        parent->update();
    }
}
