#include "xVisGPUVolumeRayCastMapper.hpp"

xVisGPUVolumeRayCastMapper::xVisGPUVolumeRayCastMapper() : xVisVolumeMapper(vtkSmartPointer<vtkGPUVolumeRayCastMapper>::New()) {
    this->m_object = vtkGPUVolumeRayCastMapper::SafeDownCast(this->get());
}

auto xVisGPUVolumeRayCastMapper::setAutoAdjustSampleDistances(bool autoAdjustSampleDistances) -> void {
    this->m_object->SetAutoAdjustSampleDistances(autoAdjustSampleDistances);
    emit this->autoAdjustSampleDistancesChanged();
    this->update();
}

auto xVisGPUVolumeRayCastMapper::getAutoAdjustSampleDistances() -> bool {
    return this->m_object->GetAutoAdjustSampleDistances();
}

auto xVisGPUVolumeRayCastMapper::setUseJittering(bool useJittering) -> void {
    this->m_object->SetUseJittering(useJittering);
    emit this->useJitteringChanged();
    this->update();
}

auto xVisGPUVolumeRayCastMapper::getUseJittering() -> bool {
    return this->m_object->GetUseJittering();
}

auto xVisGPUVolumeRayCastMapper::setUseDepthPass(bool useDepthPass) -> void {
    this->m_object->SetUseDepthPass(useDepthPass);
    emit this->useDepthPassChanged();
    this->update();
}

auto xVisGPUVolumeRayCastMapper::getUseDepthPass() -> bool {
    return this->m_object->GetUseDepthPass();
}

auto xVisGPUVolumeRayCastMapper::setSampleDistance(qreal sampleDistance) -> void {
    this->m_object->SetSampleDistance(sampleDistance);
    emit this->sampleDistanceChanged();
    this->update();
}

auto xVisGPUVolumeRayCastMapper::getSampleDistance() -> qreal {
    return this->m_object->GetSampleDistance();
}

auto xVisGPUVolumeRayCastMapper::setImageSampleDistance(qreal imageSampleDistance) -> void {
    this->m_object->SetImageSampleDistance(imageSampleDistance);
    emit this->imageSampleDistanceChanged();
    this->update();
}

auto xVisGPUVolumeRayCastMapper::getImageSampleDistance() -> qreal {
    return this->m_object->GetImageSampleDistance();
}

auto xVisGPUVolumeRayCastMapper::setMinimumImageSampleDistance(qreal minimumImageSampleDistance) -> void {
    this->m_object->SetMinimumImageSampleDistance(minimumImageSampleDistance);
    emit this->minimumImageSampleDistanceChanged();
    this->update();
}

auto xVisGPUVolumeRayCastMapper::getMinimumImageSampleDistance() -> qreal {
    return this->m_object->GetMinimumImageSampleDistance();
}

auto xVisGPUVolumeRayCastMapper::setMaximumImageSampleDistance(qreal maximumImageSampleDistance) -> void {
    this->m_object->SetMaximumImageSampleDistance(maximumImageSampleDistance);
    emit this->maximumImageSampleDistanceChanged();
    this->update();
}

auto xVisGPUVolumeRayCastMapper::getMaximumImageSampleDistance() -> qreal {
    return this->m_object->GetMaximumImageSampleDistance();
}

auto xVisGPUVolumeRayCastMapper::setFinalColorWindow(qreal finalColorWindow) -> void {
    this->m_object->SetFinalColorWindow(finalColorWindow);
    emit this->finalColorWindowChanged();
    this->update();
}

auto xVisGPUVolumeRayCastMapper::getFinalColorWindow() -> qreal {
    return this->m_object->GetFinalColorWindow();
}

auto xVisGPUVolumeRayCastMapper::setFinalColorLevel(qreal finalColorLevel) -> void {
    this->m_object->SetFinalColorLevel(finalColorLevel);
    emit this->finalColorLevelChanged();
    this->update();
}

auto xVisGPUVolumeRayCastMapper::getFinalColorLevel() -> qreal {
    return this->m_object->GetFinalColorLevel();
}
