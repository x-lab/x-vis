#include "xVisShrinkPolyData.hpp"

xVisShrinkPolyData::xVisShrinkPolyData() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkShrinkPolyData>::New()) {
    this->m_object = vtkShrinkPolyData::SafeDownCast(xVisAlgorithm::get());
}

auto xVisShrinkPolyData::setShrinkFactor(qreal val) -> void {
    this->m_object->SetShrinkFactor(val);
    emit this->shrinkFactorChanged();
    this->update();
}

auto xVisShrinkPolyData::getShrinkFactor() -> qreal {
    return this->m_object->GetShrinkFactor();
}
