#include "xVisFboOffscreenWindow.hpp"
#include "xVisFboRenderer.hpp"

#include <vtkOpenGLState.h>

#include <QtGui>
#include <QtQuick>

#undef VTK_REPORT_OPENGL_ERRORS

xVisFboOffscreenWindow::xVisFboOffscreenWindow() : QObject(), QtParentRenderer(0) {

    vtkSmartPointer<vtkRenderWindowInteractor>::New()->SetRenderWindow(this);
}

xVisFboOffscreenWindow::~xVisFboOffscreenWindow() {

}

void xVisFboOffscreenWindow::setupGraphicsBackend()
{
  QSurfaceFormat fmt = QVTKRenderWindowAdapter::defaultFormat(false);
  fmt.setAlphaBufferSize(0);

  QSurfaceFormat::setDefaultFormat(fmt);
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
  QQuickWindow::setGraphicsApi(QSGRendererInterface::OpenGLRhi);
#endif
}

auto xVisFboOffscreenWindow::New() -> xVisFboOffscreenWindow* {

    return new xVisFboOffscreenWindow();
}

auto xVisFboOffscreenWindow::OpenGLInitState() -> void {

    int glViewport[4];
    int vtkGLViewport[4];

    this->MakeCurrent();

    initializeOpenGLFunctions();

    glGetIntegerv(GL_VIEWPORT, glViewport);

    auto state = this->GetState();
    state->Initialize(this);
    state->vtkglGetIntegerv(GL_VIEWPORT, vtkGLViewport);
    state->vtkglDepthFunc(GL_LEQUAL);

    if (vtkGLViewport[2] != glViewport[2] || vtkGLViewport[3] != glViewport[3])
    {
        if (glViewport[2] > 1 && glViewport[3] > 1)
            state->vtkglViewport(0, 0, glViewport[2], glViewport[3]);
    }

    if(!m_initialised) {
        this->GetInteractor()->Initialize();
        m_initialised = true;
    }
}

auto xVisFboOffscreenWindow::Render() -> void {

    if (this->QtParentRenderer) {
        this->QtParentRenderer->update();
    }
}

auto xVisFboOffscreenWindow::InternalRender() -> void {

    Superclass::Render();

    m_adapter->blit(static_cast<unsigned int>(m_fbo->handle()), GL_COLOR_ATTACHMENT0, QRect(0, 0, m_fbo->size().width(), m_fbo->size().height()));
}

auto xVisFboOffscreenWindow::SetFramebufferObject(QOpenGLFramebufferObject *fbo) -> void {

    auto size     = fbo->size();
    this->Size[0] = size.width();
    this->Size[1] = size.height();

    if(!this->m_adapter) {
        this->m_adapter = new QVTKRenderWindowAdapter(QOpenGLContext::currentContext(), this, QGuiApplication::topLevelWindows().first());
        // this->m_adapter->setEnableHiDPI(true);
        // this->m_adapter->setUnscaledDPI(72);
    }
    this->Modified();

    this->m_fbo = fbo;
}

auto xVisFboOffscreenWindow::Resize(int width, int height) -> void {
    this->SetSize(width, height);
    this->m_adapter->resize(width, height);
}
