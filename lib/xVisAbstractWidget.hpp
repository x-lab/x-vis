#pragma once

#include "xVisObject.hpp"

#include <vtkSmartPointer.h>
#include <vtkAbstractWidget.h>

class xVisAbstractWidget : public xVisObject
{
    Q_OBJECT
    Q_PROPERTY(bool enabled READ getEnabled WRITE setEnabled NOTIFY enabledChanged);

public:
    xVisAbstractWidget(void) = delete;
    xVisAbstractWidget(vtkSmartPointer<vtkAbstractWidget>);

    void setEnabled(bool);
    bool getEnabled(void);

    vtkSmartPointer<vtkAbstractWidget> get(void);

signals:
    void enabledChanged(void);

private:
    bool m_enabled = true;
    vtkSmartPointer<vtkAbstractWidget> m_object = nullptr;
};
