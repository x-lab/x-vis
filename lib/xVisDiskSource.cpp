#include "xVisDiskSource.hpp"

xVisDiskSource::xVisDiskSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkDiskSource>::New()) {
    this->m_object = vtkDiskSource::SafeDownCast(xVisAlgorithm::get());
}

auto xVisDiskSource::setInnerRadius(qreal radius) -> void {
    this->m_object->SetInnerRadius(radius);
    emit this->innerRadiusChanged();
    this->update();
}

auto xVisDiskSource::setOuterRadius(qreal radius) -> void {
    this->m_object->SetOuterRadius(radius);
    emit this->outerRadiusChanged();
    this->update();
}

auto xVisDiskSource::setRadialResolution(int value) -> void {
    this->m_object->SetRadialResolution(value);
    emit this->radialResolutionChanged();
    this->update();
}

auto xVisDiskSource::setCircumferentialResolution(int value) -> void {
    this->m_object->SetCircumferentialResolution(value);
    emit this->circumferentialResolutionChanged();
    this->update();
}

auto xVisDiskSource::getInnerRadius() -> qreal {
    return this->m_object->GetInnerRadius();
}

auto xVisDiskSource::getOuterRadius() -> qreal {
    return this->m_object->GetOuterRadius();
}

auto xVisDiskSource::getRadialResolution() -> int {
    return this->m_object->GetRadialResolution();
}

auto xVisDiskSource::getCircumferentialResolution() -> int {
    return this->m_object->GetCircumferentialResolution();
}
