#pragma once

#include "xVisMapper.hpp"
#include "xVisProperty.hpp"
#include "xVisProp3D.hpp"
#include "xVisTexture.hpp"

#include <vtkActor.h>

class xVisActor : public xVisProp3D
{
    Q_OBJECT
    Q_PROPERTY(xVisTexture* texture READ getTexture CONSTANT);
    Q_PROPERTY(xVisProperty* property READ getProperty CONSTANT);
    Q_PROPERTY(xVisMapper* mapper READ getMapper WRITE setMapper NOTIFY mapperChanged);
    Q_CLASSINFO("DefaultProperty", "mapper");

public:
     xVisActor(void);
     xVisActor(vtkSmartPointer<vtkActor>);
    ~xVisActor(void);

    auto setVtkActor(vtkSmartPointer<vtkActor>) -> void;
    auto setMapper(xVisMapper*) -> void;

    xVisMapper *getMapper(void);
    xVisProperty *getProperty(void);
    xVisTexture *getTexture(void);
    vtkSmartPointer<vtkActor> get(void);

signals:
    void mapperChanged(void);

private:
    xVisProperty* m_property = nullptr;
    xVisTexture* m_texture = nullptr;
    xVisMapper* m_mapper = nullptr;

private:
    vtkSmartPointer<vtkActor> m_object = nullptr;
};
