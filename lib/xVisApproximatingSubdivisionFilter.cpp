#include "xVisApproximatingSubdivisionFilter.hpp"

xVisApproximatingSubdivisionFilter::xVisApproximatingSubdivisionFilter(vtkSmartPointer<vtkApproximatingSubdivisionFilter> vtkObject) : xVisPolyDataAlgorithm(vtkObject), m_object(vtkObject)
{
}

auto xVisApproximatingSubdivisionFilter::setNumberOfSubdivisions(int subdivisions) -> void
{
    this->m_object->SetNumberOfSubdivisions(subdivisions);
    emit this->numberOfSubdivisionsChanged();
    this->update();
}

auto xVisApproximatingSubdivisionFilter::getNumberOfSubdivisions() -> int
{
    return this->m_object->GetNumberOfSubdivisions();
}
