#include "xVisMinimalStandardRandomSequence.hpp"

xVisMinimalStandardRandomSequence::xVisMinimalStandardRandomSequence() : xVisRandomSequence(vtkSmartPointer<vtkMinimalStandardRandomSequence>::New()) {
    this->m_object = vtkMinimalStandardRandomSequence::SafeDownCast(xVisRandomSequence::get());
}    
