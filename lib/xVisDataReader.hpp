#pragma once

#include "xVisAlgorithm.hpp"

#include <vtkDataReader.h>

class xVisDataReader : public xVisAlgorithm {
    Q_OBJECT
    Q_PROPERTY(QString fileName READ getFileName WRITE setFileName NOTIFY fileNameChanged);
private:
    vtkSmartPointer<vtkDataReader> m_object = nullptr;
public:
    xVisDataReader(vtkSmartPointer<vtkDataReader>);
    auto setFileName(const QString&) -> void;
    auto getFileName() -> QString;
signals:
    void fileNameChanged();
};
