#include "xVisTextureMapToCylinder.hpp"

xVisTextureMapToCylinder::xVisTextureMapToCylinder() : xVisDataSetAlgorithm(vtkSmartPointer<vtkTextureMapToCylinder>::New()) {
    this->m_object = vtkTextureMapToCylinder::SafeDownCast(this->get());
}
