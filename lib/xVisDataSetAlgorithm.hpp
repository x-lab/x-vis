#pragma once

#include "xVisAlgorithm.hpp"

#include <vtkDataSetAlgorithm.h>

class xVisDataSetAlgorithm : public xVisAlgorithm {
    Q_OBJECT
public:
    xVisDataSetAlgorithm(vtkSmartPointer<vtkDataSetAlgorithm>);
};
