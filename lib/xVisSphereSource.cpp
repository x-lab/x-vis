#include "xVisSphereSource.hpp"

xVisSphereSource::xVisSphereSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkSphereSource>::New()) {
    this->m_object = vtkSphereSource::SafeDownCast(xVisAlgorithm::get());

    this->m_center = new xVisVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });
}

auto xVisSphereSource::getCenter() -> xVisVector3* {
    return this->m_center;
}

auto xVisSphereSource::setLatLongTessellation(bool value) -> void {
    this->m_object->SetLatLongTessellation(value);
    emit this->latLongTessellationChanged();
    this->update();
}

auto xVisSphereSource::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    emit this->radiusChanged();
    this->update();
}

auto xVisSphereSource::setThetaResolution(int value) -> void {
    this->m_object->SetThetaResolution(value);
    emit this->thetaResolutionChanged();
    this->update();
}

auto xVisSphereSource::setPhiResolution(int value) -> void {
    this->m_object->SetPhiResolution(value);
    emit this->phiResolutionChanged();
    this->update();
}

auto xVisSphereSource::setStartTheta(qreal value) -> void {
    this->m_object->SetStartTheta(value);
    emit this->startThetaChanged();
    this->update();
}

auto xVisSphereSource::setStartPhi(qreal value) -> void {
    this->m_object->SetStartPhi(value);
    emit this->startPhiChanged();
    this->update();
}

auto xVisSphereSource::setEndTheta(qreal value) -> void {
    this->m_object->SetEndTheta(value);
    emit this->endThetaChanged();
    this->update();
}

auto xVisSphereSource::setEndPhi(qreal value) -> void {
    this->m_object->SetEndPhi(value);
    emit this->endPhiChanged();
    this->update();
}

auto xVisSphereSource::getLatLongTessellation() -> bool {
    return this->m_object->GetLatLongTessellation();
}

auto xVisSphereSource::getThetaResolution() -> int {
    return this->m_object->GetThetaResolution();
}

auto xVisSphereSource::getPhiResolution() -> int {
    return this->m_object->GetPhiResolution();
}

auto xVisSphereSource::getStartTheta() -> qreal {
    return this->m_object->GetStartTheta();
}

auto xVisSphereSource::getStartPhi() -> qreal {
    return this->m_object->GetStartPhi();
}

auto xVisSphereSource::getEndTheta() -> qreal {
    return this->m_object->GetEndTheta();
}

auto xVisSphereSource::getEndPhi() -> qreal {
    return this->m_object->GetEndPhi();
}

auto xVisSphereSource::getRadius() -> qreal {
    return this->m_object->GetRadius();
}
