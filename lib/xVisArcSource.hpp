#pragma once

#include "xVisPolyDataAlgorithm.hpp"
#include "xVisVector3.hpp"

#include <vtkArcSource.h>

class xVisArcSource : public xVisPolyDataAlgorithm {
  Q_OBJECT
  Q_PROPERTY(xVisVector3 *point1 READ getPoint1 CONSTANT);
  Q_PROPERTY(xVisVector3 *point2 READ getPoint2 CONSTANT);
  Q_PROPERTY(xVisVector3 *center READ getCenter CONSTANT);
  Q_PROPERTY(xVisVector3 *polarVector READ getPolarVector CONSTANT);
  Q_PROPERTY(qreal angle READ getAngle WRITE setAngle NOTIFY angleChanged);
  Q_PROPERTY(int resolution READ getResolution WRITE setResolution NOTIFY
                 resolutionChanged);
  Q_PROPERTY(
      bool negative READ getNegative WRITE setNegative NOTIFY negativeChanged);
  Q_PROPERTY(bool useNormalAndAngle READ getUseNormalAndAngle WRITE
                 setUseNormalAndAngle NOTIFY useNormalAndAngleChanged);

private:
  vtkSmartPointer<vtkArcSource> m_object = nullptr;
  xVisVector3 *m_point1 = nullptr;
  xVisVector3 *m_point2 = nullptr;
  xVisVector3 *m_center = nullptr;
  xVisVector3 *m_polarVector = nullptr;

public:
  xVisArcSource();
  auto getPoint1() -> xVisVector3 *;
  auto getPoint2() -> xVisVector3 *;
  auto getCenter() -> xVisVector3 *;
  auto getPolarVector() -> xVisVector3 *;
  auto setAngle(qreal) -> void;
  auto getAngle() -> qreal;
  auto setResolution(int) -> void;
  auto getResolution() -> int;
  auto setNegative(bool) -> void;
  auto getNegative() -> bool;
  auto setUseNormalAndAngle(bool) -> void;
  auto getUseNormalAndAngle() -> bool;

signals:
  void angleChanged();
  void resolutionChanged();
  void negativeChanged();
  void useNormalAndAngleChanged();
};
