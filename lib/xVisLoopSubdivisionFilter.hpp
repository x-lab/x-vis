#pragma once

#include "xVisApproximatingSubdivisionFilter.hpp"

#include <vtkLoopSubdivisionFilter.h>

class xVisLoopSubdivisionFilter : public xVisApproximatingSubdivisionFilter {
    Q_OBJECT
public:
    xVisLoopSubdivisionFilter();
};
