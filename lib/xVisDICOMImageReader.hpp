#pragma once

#include "xVisImageReader2.hpp"

#include <vtkDICOMImageReader.h>

class xVisDICOMImageReader : public xVisImageReader2 {
    Q_OBJECT
    Q_PROPERTY(QString directoryName READ getDirectoryName WRITE setDirectoryName NOTIFY directoryNameChanged);
private:
    QString m_directoryName;
    vtkSmartPointer<vtkDICOMImageReader> m_object = nullptr;
public:
    xVisDICOMImageReader();
    auto setDirectoryName(const QString&) -> void;
    auto getDirectoryName() -> QString;
signals:
    void directoryNameChanged();
};
