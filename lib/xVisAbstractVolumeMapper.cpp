#include "xVisAbstractVolumeMapper.hpp"

xVisAbstractVolumeMapper::xVisAbstractVolumeMapper(vtkSmartPointer<vtkAbstractVolumeMapper> vtkObject) : xVisAbstractMapper3D(vtkObject)
{

}

vtkSmartPointer<vtkAbstractVolumeMapper> xVisAbstractVolumeMapper::get(void)
{
    return vtkAbstractVolumeMapper::SafeDownCast(xVisAlgorithm::get());
}
