#include "xVisSuperquadricSource.hpp"

xVisSuperquadricSource::xVisSuperquadricSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkSuperquadricSource>::New()) {

    this->m_object = vtkSuperquadricSource::SafeDownCast(xVisAlgorithm::get());

    this->m_center = new xVisVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });

    this->m_scale = new xVisVector3([this](){
        this->m_object->SetScale(this->m_scale->getValues().data());
        this->update();
    });
}

auto xVisSuperquadricSource::setToroidal(bool toroidal) -> void {
    this->m_object->SetToroidal(toroidal);
    this->update();
    emit this->toroidalChanged();
}

auto xVisSuperquadricSource::getToroidal() -> bool {
    return this->m_object->GetToroidal();
}

auto xVisSuperquadricSource::setThickness(qreal thickness) -> void {
    this->m_object->SetThickness(thickness);
    this->update();
    emit this->thicknessChanged();
}

auto xVisSuperquadricSource::getThickness() -> qreal {
    return this->m_object->GetThickness();
}

auto xVisSuperquadricSource::setSize(qreal size) -> void {
    this->m_object->SetSize(size);
    this->update();
    emit this->sizeChanged();
}

auto xVisSuperquadricSource::getSize() -> qreal {
    return this->m_object->GetSize();
}

auto xVisSuperquadricSource::setPhiRoundness(qreal phiRoundness) -> void {
    this->m_object->SetPhiRoundness(phiRoundness);
    this->update();
    emit this->phiRoundnessChanged();
}

auto xVisSuperquadricSource::getPhiRoundness() -> qreal {
    return this->m_object->GetPhiRoundness();
}

auto xVisSuperquadricSource::setThetaRoundness(qreal thetaRoundness) -> void {
    this->m_object->SetThetaRoundness(thetaRoundness);
    this->update();
    emit this->thetaRoundnessChanged();
}

auto xVisSuperquadricSource::getThetaRoundness() -> qreal {
    return this->m_object->GetThetaRoundness();
}

auto xVisSuperquadricSource::setPhiResolution(qreal phiResolution) -> void {
    this->m_object->SetPhiResolution(phiResolution);
    this->update();
    emit this->phiResolutionChanged();
}

auto xVisSuperquadricSource::getPhiResolution() -> qreal {
    return this->m_object->GetPhiResolution();
}

auto xVisSuperquadricSource::setThetaResolution(qreal thetaResolution) -> void {
    this->m_object->SetThetaResolution(thetaResolution);
    this->update();
    emit this->thetaResolutionChanged();
}

auto xVisSuperquadricSource::getThetaResolution() -> qreal {
    return this->m_object->GetThetaResolution();
}

auto xVisSuperquadricSource::setAxisOfSymmetry(Axis axis) -> void {
    switch (axis) {
        case Axis::X: this->m_object->SetXAxisOfSymmetry(); break;
        case Axis::Y: this->m_object->SetYAxisOfSymmetry(); break;
        case Axis::Z: this->m_object->SetZAxisOfSymmetry(); break;
        default: break;
    }

    this->update();
    emit this->axisOfSymmetryChanged();
}

auto xVisSuperquadricSource::getAxisOfSymmetry() -> Axis {
    return this->m_axisOfSymmetry;
}

auto xVisSuperquadricSource::getCenter() -> xVisVector3* {
    return this->m_center;
}

auto xVisSuperquadricSource::getScale() -> xVisVector3* {
    return this->m_scale;
}
