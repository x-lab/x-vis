#pragma once

#include "xVisAbstractPolyDataReader.hpp"

#include <vtkOBJReader.h>

class xVisOBJReader : public xVisAbstractPolyDataReader {
    Q_OBJECT
public:
    xVisOBJReader();
};
