#include "xVisConeSource.hpp"

xVisConeSource::xVisConeSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkConeSource>::New())
{
    this->m_object = vtkConeSource::SafeDownCast(xVisAlgorithm::get());

    this->m_center = new xVisVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });

    this->m_direction = new xVisVector3([this](){
        this->m_object->SetDirection(this->m_direction->getValues().data());
        this->update();
    });
}

auto xVisConeSource::getCenter() -> xVisVector3* {
    return this->m_center;
}

auto xVisConeSource::getDirection() -> xVisVector3* {
    return this->m_direction;
}

auto xVisConeSource::setHeight(qreal height) -> void {
    this->m_object->SetHeight(height);
    emit this->heightChanged();
    emit this->angleChanged();
    this->update();
}

auto xVisConeSource::getHeight() -> qreal {
    return this->m_object->GetHeight();
}

auto xVisConeSource::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    emit this->radiusChanged();
    this->update();
}

auto xVisConeSource::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

auto xVisConeSource::setAngle(qreal angle) -> void {
    this->m_object->SetAngle(angle);
    emit this->angleChanged();
    emit this->heightChanged();
    this->update();
}

auto xVisConeSource::getAngle() -> qreal {
    return this->m_object->GetAngle();
}

auto xVisConeSource::setResolution(int resolution) -> void {
    this->m_object->SetResolution(resolution);
    emit this->resolutionChanged();
    this->update();
}

auto xVisConeSource::getResolution() -> int {
    return this->m_object->GetResolution();
}

auto xVisConeSource::setCapping(bool capping) -> void {
    this->m_object->SetCapping(capping);
    emit this->cappingChanged();
    this->update();
}

auto xVisConeSource::getCapping() -> bool {
    return this->m_object->GetCapping();
}
