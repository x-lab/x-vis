#pragma once

#include "xVisAlgorithm.hpp"

#include <vtkPolyDataAlgorithm.h>

class xVisPolyDataAlgorithm : public xVisAlgorithm {
    Q_OBJECT
public:
    xVisPolyDataAlgorithm(vtkSmartPointer<vtkPolyDataAlgorithm>);
};
