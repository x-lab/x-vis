#include "xVisSmartVolumeMapper.hpp"

xVisSmartVolumeMapper::xVisSmartVolumeMapper() : xVisVolumeMapper(vtkSmartPointer<vtkSmartVolumeMapper>::New()) {
    this->m_object = vtkSmartVolumeMapper::SafeDownCast(this->xVisAlgorithm::get());
}

auto xVisSmartVolumeMapper::setFinalColorWindow(qreal finalColorWindow) -> void {
    this->m_object->SetFinalColorWindow(finalColorWindow);
    emit this->finalColorWindowChanged();
    this->update();
}

auto xVisSmartVolumeMapper::getFinalColorWindow() -> qreal {
    return this->m_object->GetFinalColorWindow();
}

auto xVisSmartVolumeMapper::setFinalColorLevel(qreal finalColorLevel) -> void {
    this->m_object->SetFinalColorLevel(finalColorLevel);
    emit this->finalColorLevelChanged();
    this->update();
}

auto xVisSmartVolumeMapper::getFinalColorLevel() -> qreal {
    return this->m_object->GetFinalColorLevel();
}
