#include "xVisQuadric.hpp"

xVisQuadric::xVisQuadric(void) : xVisImplicitFunction(vtkSmartPointer<vtkQuadric>::New())
{
    this->m_object = vtkQuadric::SafeDownCast(xVisImplicitFunction::get());
}

xVisQuadric::xVisQuadric(vtkSmartPointer<vtkQuadric> object) : xVisImplicitFunction(object)
{
    this->m_object = vtkQuadric::SafeDownCast(xVisImplicitFunction::get());
}

auto xVisQuadric::get(void) -> vtkSmartPointer<vtkQuadric>
{
    return this->m_object;
}

xVisQuadric::~xVisQuadric(void)
{
    this->m_object = nullptr;
}
