#include <QtGui>
#include <QtQml>
#include <QtQuick>

#include "xVisAbstractMapper.hpp"
#include "xVisAbstractMapper3D.hpp"
#include "xVisAbstractPolyDataReader.hpp"
#include "xVisAbstractVolumeMapper.hpp"
#include "xVisAbstractWidget.hpp"
#include "xVisActor.hpp"
#include "xVisActor2D.hpp"
#include "xVisAlgorithm.hpp"
#include "xVisAdaptiveSubdivisionFilter.hpp"
#include "xVisApproximatingSubdivisionFilter.hpp"
#include "xVisArcSource.hpp"
#include "xVisArrowSource.hpp"
#include "xVisBooleanOperationPolyDataFilter.hpp"
#include "xVisBoxWidget2.hpp"
#include "xVisButterflySubdivisionFilter.hpp"
#include "xVisColorTransferFunction.hpp"
#include "xVisCone.hpp"
#include "xVisConeSource.hpp"
#include "xVisContourFilter.hpp"
#include "xVisCutter.hpp"
#include "xVisCylinderSource.hpp"
#include "xVisDataReader.hpp"
#include "xVisDataSetAlgorithm.hpp"
#include "xVisDecimatePro.hpp"
#include "xVisDiskSource.hpp"
#include "xVisDensifyPolyData.hpp"
#include "xVisDICOMImageReader.hpp"
#include "xVisEllipseArcSource.hpp"
#include "xVisExtractEdges.hpp"
#include "xVisExtractSurface.hpp"
#include "xVisFboOffscreenWindow.hpp"
#include "xVisGlyph3D.hpp"
#include "xVisGPUVolumeRayCastMapper.hpp"
#include "xVisHedgeHog.hpp"
#include "xVisImageAlgorithm.hpp"
#include "xVisImageDataGeometryFilter.hpp"
#include "xVisImageLaplacian.hpp"
#include "xVisImageMapper.hpp"
#include "xVisImageReader2.hpp"
#include "xVisImplicitFunction.hpp"
#include "xVisInterpolatingSubdivisionFilter.hpp"
#include "xVisLinearExtrusionFilter.hpp"
#include "xVisLinearSubdivisionFilter.hpp"
#include "xVisLoopSubdivisionFilter.hpp"
#include "xVisMapper.hpp"
#include "xVisMapper2D.hpp"
#include "xVisMinimalStandardRandomSequence.hpp"
#include "xVisObject.hpp"
#include "xVisOBJReader.hpp"
#include "xVisPerlinNoise.hpp"
#include "xVisPiecewiseFunction.hpp"
#include "xVisPlane.hpp"
#include "xVisPlatonicSolidSource.hpp"
#include "xVisPLYReader.hpp"
#include "xVisPNGReader.hpp"
#include "xVisPointSetAlgorithm.hpp"
#include "xVisPointSource.hpp"
#include "xVisPolyDataAlgorithm.hpp"
#include "xVisPolyDataMapper.hpp"
#include "xVisPolyDataNormals.hpp"
#include "xVisProp.hpp"
#include "xVisProp3D.hpp"
#include "xVisProperty.hpp"
#include "xVisProperty2D.hpp"
#include "xVisQuadric.hpp"
#include "xVisRandomSequence.hpp"
#include "xVisReverseSense.hpp"
#include "xVisRibbonFilter.hpp"
#include "xVisRotationalExtrusionFilter.hpp"
#include "xVisSampleFunction.hpp"
#include "xVisSectorSource.hpp"
#include "xVisShrinkPolyData.hpp"
#include "xVisSmartVolumeMapper.hpp"
#include "xVisSmoothPolyDataFilter.hpp"
#include "xVisSphere.hpp"
#include "xVisSphereSource.hpp"
#include "xVisSTLReader.hpp"
#include "xVisStripper.hpp"
#include "xVisStructuredPointsReader.hpp"
#include "xVisSuperquadricSource.hpp"
#include "xVisTextSource.hpp"
#include "xVisTexture.hpp"
#include "xVisTextureMapToCylinder.hpp"
#include "xVisThreadedImageAlgorithm.hpp"
#include "xVisTriangleFilter.hpp"
#include "xVisTubeFilter.hpp"
#include "xVisVector2.hpp"
#include "xVisVector3.hpp"
#include "xVisVectorText.hpp"
#include "xVisVertexGlyphFilter.hpp"
#include "xVisViewer.hpp"
#include "xVisVolume.hpp"
#include "xVisVolumeMapper.hpp"
#include "xVisVolumeProperty.hpp"
#include "xVisWarpLens.hpp"
#include "xVisWarpScalar.hpp"
#include "xVisWarpTo.hpp"

#include <stdlib.h>
#include <stdio.h>

void x_vis_initialise(void)
{
    qmlRegisterUncreatableType<xVisAbstractMapper>("xQuick.Vis", 1, 0, "AbstractMapper", "");
    qmlRegisterUncreatableType<xVisAbstractMapper3D>("xQuick.Vis", 1, 0, "AbstractMapper3D", "");
    qmlRegisterUncreatableType<xVisAbstractPolyDataReader>("xQuick.Vis", 1, 0, "AbstractPolyDataReader", "");
    qmlRegisterUncreatableType<xVisAbstractVolumeMapper>("xQuick.Vis", 1, 0, "AbstractVolumeMapper", "");
    qmlRegisterUncreatableType<xVisAbstractWidget>("xQuick.Vis", 1, 0, "AbstractWidget", "");
    qmlRegisterUncreatableType<xVisAlgorithm>("xQuick.Vis", 1, 0, "Algorithm", "");
    qmlRegisterUncreatableType<xVisApproximatingSubdivisionFilter>("xQuick.Vis", 1, 0, "ApproximatingSubdivisionFilter", "");
    qmlRegisterUncreatableType<xVisColorTransferFunction>("xQuick.Vis", 1, 0, "ColorTransferFunction", "");
    qmlRegisterUncreatableType<xVisDataReader>("xQuick.Vis", 1, 0, "DataReader", "");
    qmlRegisterUncreatableType<xVisDataSetAlgorithm>("xQuick.Vis", 1, 0, "DataSetAlgorithm", "");
    qmlRegisterUncreatableType<xVisImageAlgorithm>("xQuick.Vis", 1, 0, "ImageAlgorithm", "");
    qmlRegisterUncreatableType<xVisImageReader2>("xQuick.Vis", 1, 0, "ImageReader2", "");
    qmlRegisterUncreatableType<xVisImplicitFunction>("xQuick.Vis", 1, 0, "ImplicitFunction", "");
    qmlRegisterUncreatableType<xVisInterpolatingSubdivisionFilter>("xQuick.Vis", 1, 0, "InterpolatingSubdivisionFilter", "");
    qmlRegisterUncreatableType<xVisMapper>("xQuick.Vis", 1, 0, "Mapper", "");
    qmlRegisterUncreatableType<xVisMapper2D>("xQuick.Vis", 1, 0, "Mapper2D", "");
    qmlRegisterUncreatableType<xVisObject>("xQuick.Vis", 1, 0, "Object", "");
    qmlRegisterUncreatableType<xVisPiecewiseFunction>("xQuick.Vis", 1, 0, "PiecewiseFunction", "");
    qmlRegisterUncreatableType<xVisPointSetAlgorithm>("xQuick.Vis", 1, 0, "PointSetAlgorithm", "");
    qmlRegisterUncreatableType<xVisPolyDataAlgorithm>("xQuick.Vis", 1, 0, "PolyDataAlgorithm", "");
    qmlRegisterUncreatableType<xVisProp>("xQuick.Vis", 1, 0, "Prop", "");
    qmlRegisterUncreatableType<xVisProp3D>("xQuick.Vis", 1, 0, "Prop3D", "");
    qmlRegisterUncreatableType<xVisProperty>("xQuick.Vis", 1, 0, "Property", "");
    qmlRegisterUncreatableType<xVisProperty2D>("xQuick.Vis", 1, 0, "Property2D", "");
    qmlRegisterUncreatableType<xVisRandomSequence>("xQuick.Vis", 1, 0, "RandomSequence", "");
    qmlRegisterUncreatableType<xVisTexture>("xQuick.Vis", 1, 0, "Texture", "");
    qmlRegisterUncreatableType<xVisThreadedImageAlgorithm>("xQuick.Vis", 1, 0, "ThreadedImageAlgorithm", "");
    qmlRegisterUncreatableType<xVisVector2>("xQuick.Vis", 1, 0, "Vector2", "");
    qmlRegisterUncreatableType<xVisVector3>("xQuick.Vis", 1, 0, "Vector3", "");
    qmlRegisterUncreatableType<xVisVolumeMapper>("xQuick.Vis", 1, 0, "VolumeMapper", "");
    qmlRegisterUncreatableType<xVisVolumeProperty>("xQuick.Vis", 1, 0, "VolumeProperty", "");

    qmlRegisterType<xVisActor>("xQuick.Vis", 1, 0, "Actor");
    qmlRegisterType<xVisActor2D>("xQuick.Vis", 1, 0, "Actor2D");
    qmlRegisterType<xVisAdaptiveSubdivisionFilter>("xQuick.Vis", 1, 0, "AdaptiveSubdivisionFilter");
    qmlRegisterType<xVisArcSource>("xQuick.Vis", 1, 0, "ArcSource");
    qmlRegisterType<xVisArrowSource>("xQuick.Vis", 1, 0, "ArrowSource");
    qmlRegisterType<xVisBooleanOperationPolyDataFilter>("xQuick.Vis", 1, 0, "BooleanOperationPolyDataFilter");
    qmlRegisterType<xVisBoxWidget2>("xQuick.Vis", 1, 0, "BoxWidget2");
    qmlRegisterType<xVisButterflySubdivisionFilter>("xQuick.Vis", 1, 0, "ButterflySubdivisionFilter");
    qmlRegisterType<xVisCone>("xQuick.Vis", 1, 0, "Cone");
    qmlRegisterType<xVisConeSource>("xQuick.Vis", 1, 0, "ConeSource");
    qmlRegisterType<xVisContourFilter>("xQuick.Vis", 1, 0, "ContourFilter");
    qmlRegisterType<xVisCutter>("xQuick.Vis", 1, 0, "Cutter");
    qmlRegisterType<xVisCylinderSource>("xQuick.Vis", 1, 0, "CylinderSource");
    qmlRegisterType<xVisDecimatePro>("xQuick.Vis", 1, 0, "DecimatePro");
    qmlRegisterType<xVisDensifyPolyData>("xQuick.Vis", 1, 0, "DensifyPolyData");
    qmlRegisterType<xVisDiskSource>("xQuick.Vis", 1, 0, "DiskSource");
    qmlRegisterType<xVisDICOMImageReader>("xQuick.Vis", 1, 0, "DICOMImageReader");
    qmlRegisterType<xVisEllipseArcSource>("xQuick.Vis", 1, 0, "EllipseArcSource");
    qmlRegisterType<xVisExtractEdges>("xQuick.Vis", 1, 0, "ExtractEdges");
    qmlRegisterType<xVisExtractSurface>("xQuick.Vis", 1, 0, "ExtractSurface");
    qmlRegisterType<xVisGlyph3D>("xQuick.Vis", 1, 0, "Glyph3D");
    qmlRegisterType<xVisGPUVolumeRayCastMapper>("xQuick.Vis", 1, 0, "GPUVolumeRayCastMapper");
    qmlRegisterType<xVisHedgeHog>("xQuick.Vis", 1, 0, "HedgeHog");
    qmlRegisterType<xVisImageDataGeometryFilter>("xQuick.Vis", 1, 0, "ImageDataGeometryFilter");
    qmlRegisterType<xVisImageLaplacian>("xQuick.Vis", 1, 0, "ImageLaplacian");
    qmlRegisterType<xVisImageMapper>("xQuick.Vis", 1, 0, "ImageMapper");
    qmlRegisterType<xVisLinearExtrusionFilter>("xQuick.Vis", 1, 0, "LinearExtrusionFilter");
    qmlRegisterType<xVisLinearSubdivisionFilter>("xQuick.Vis", 1, 0, "LinearSubdivisionFilter");
    qmlRegisterType<xVisLoopSubdivisionFilter>("xQuick.Vis", 1, 0, "LoopSubdivisionFilter");
    qmlRegisterType<xVisMinimalStandardRandomSequence>("xQuick.Vis", 1, 0, "MinimalStandardRandomSequence");
    qmlRegisterType<xVisOBJReader>("xQuick.Vis", 1, 0, "OBJReader");
    qmlRegisterType<xVisPerlinNoise>("xQuick.Vis", 1, 0, "PerlinNoise");
    qmlRegisterType<xVisPlane>("xQuick.Vis", 1, 0, "Plane");
    qmlRegisterType<xVisPlatonicSolidSource>("xQuick.Vis", 1, 0, "PlatonicSolidSource");
    qmlRegisterType<xVisPLYReader>("xQuick.Vis", 1, 0, "PLYReader");
    qmlRegisterType<xVisPNGReader>("xQuick.Vis", 1, 0, "PNGReader");
    qmlRegisterType<xVisPointSource>("xQuick.Vis", 1, 0, "PointSource");
    qmlRegisterType<xVisPolyDataMapper>("xQuick.Vis", 1, 0, "PolyDataMapper");
    qmlRegisterType<xVisPolyDataNormals>("xQuick.Vis", 1, 0, "PolyDataNormals");
    qmlRegisterType<xVisQuadric>("xQuick.Vis", 1, 0, "Quadric");
    qmlRegisterType<xVisReverseSense>("xQuick.Vis", 1, 0, "ReverseSense");
    qmlRegisterType<xVisRibbonFilter>("xQuick.Vis", 1, 0, "RibbonFilter");
    qmlRegisterType<xVisRotationalExtrusionFilter>("xQuick.Vis", 1, 0, "RotationalExtrusionFilter");
    qmlRegisterType<xVisSampleFunction>("xQuick.Vis", 1, 0, "SampleFunction");
    qmlRegisterType<xVisSectorSource>("xQuick.Vis", 1, 0, "SectorSource");
    qmlRegisterType<xVisShrinkPolyData>("xQuick.Vis", 1, 0, "ShrinkPolyData");
    qmlRegisterType<xVisSmartVolumeMapper>("xQuick.Vis", 1, 0, "SmartVolumeMapper");
    qmlRegisterType<xVisSmoothPolyDataFilter>("xQuick.Vis", 1, 0, "SmoothPolyDataFilter");
    qmlRegisterType<xVisSphere>("xQuick.Vis", 1, 0, "Sphere");
    qmlRegisterType<xVisSphereSource>("xQuick.Vis", 1, 0, "SphereSource");
    qmlRegisterType<xVisSTLReader>("xQuick.Vis", 1, 0, "STLReader");
    qmlRegisterType<xVisStripper>("xQuick.Vis", 1, 0, "Stripper");
    qmlRegisterType<xVisStructuredPointsReader>("xQuick.Vis", 1, 0, "StructuredPointsReader");
    qmlRegisterType<xVisSuperquadricSource>("xQuick.Vis", 1, 0, "SuperquadricSource");
    qmlRegisterType<xVisTextSource>("xQuick.Vis", 1, 0, "TextSource");
    qmlRegisterType<xVisTextureMapToCylinder>("xQuick.Vis", 1, 0, "TextureMapToCylinder");
    qmlRegisterType<xVisTriangleFilter>("xQuick.Vis", 1, 0, "TriangleFilter");
    qmlRegisterType<xVisTubeFilter>("xQuick.Vis", 1, 0, "TubeFilter");
    qmlRegisterType<xVisVectorText>("xQuick.Vis", 1, 0, "VectorText");
    qmlRegisterType<xVisVertexGlyphFilter>("xQuick.Vis", 1, 0, "VertexGlyphFilter");
    qmlRegisterType<xVisViewer>("xQuick.Vis", 1, 0, "Viewer");
    qmlRegisterType<xVisVolume>("xQuick.Vis", 1, 0, "Volume");
    qmlRegisterType<xVisWarpLens>("xQuick.Vis", 1, 0, "WarpLens");
    qmlRegisterType<xVisWarpScalar>("xQuick.Vis", 1, 0, "WarpScalar");
    qmlRegisterType<xVisWarpTo>("xQuick.Vis", 1, 0, "WarpTo");

#if QT_VERSION >= 0x060000
    QQuickWindow::setGraphicsApi(QSGRendererInterface::OpenGL);
#endif

    xVisFboOffscreenWindow::setupGraphicsBackend();

    setenv("QML_BAD_GUI_RENDER_LOOP", "1", 1);
    setenv("QML_USE_GLYPHCACHE_WORKAROUND", "1", 1);
}
