#pragma once

#include "xVisThreadedImageAlgorithm.hpp"

#include <vtkImageLaplacian.h>

class xVisImageLaplacian : public xVisThreadedImageAlgorithm
{
    Q_OBJECT

public:
    xVisImageLaplacian();
};
