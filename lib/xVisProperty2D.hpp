#pragma once

#include <QColor>
#include <QObject>

#include <vtkSmartPointer.h>
#include <vtkProperty2D.h>
#include <vtkActor2D.h>

class xVisActor2D;

class xVisProperty2D : public QObject {
    Q_OBJECT
    Q_PROPERTY(QColor color READ getColor WRITE setColor NOTIFY colorChanged);
    Q_PROPERTY(qreal opacity READ getOpacity WRITE setOpacity NOTIFY opacityChanged);
private:
    QColor m_color;
    xVisActor2D* m_actor2D = nullptr;
    vtkSmartPointer<vtkActor2D> m_vtkActor2D = nullptr;
private:
    void update();
public:
    xVisProperty2D() = delete;
    xVisProperty2D(xVisActor2D*);
    auto setColor(const QColor&) -> void;
    auto getColor() -> QColor;
    auto setOpacity(qreal) -> void;
    auto getOpacity() -> qreal;
signals:
    void colorChanged();
    void opacityChanged();
};
