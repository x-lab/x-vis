#pragma once

#include "xVisDataSetAlgorithm.hpp"

#include <vtkTextureMapToCylinder.h>

class xVisTextureMapToCylinder : public xVisDataSetAlgorithm {
    Q_OBJECT
private:
    vtkSmartPointer<vtkTextureMapToCylinder> m_object = nullptr;
public:
    xVisTextureMapToCylinder();
};
