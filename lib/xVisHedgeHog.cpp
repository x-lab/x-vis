#include "xVisHedgeHog.hpp"

xVisHedgeHog::xVisHedgeHog() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkHedgeHog>::New()) {
    this->m_object = vtkHedgeHog::SafeDownCast(xVisAlgorithm::get());
}

auto xVisHedgeHog::setScaleFactor(qreal scaleFactor) -> void {
    this->m_object->SetScaleFactor(scaleFactor);
    emit this->scaleFactorChanged();
    this->update();
}

auto xVisHedgeHog::getScaleFactor() -> qreal {
    return this->m_object->GetScaleFactor();
}

auto xVisHedgeHog::setVectorMode(VectorMode vectorMode) -> void {
    this->m_object->SetVectorMode(vectorMode);
    emit this->vectorModeChanged();
    this->update();
}

auto xVisHedgeHog::getVectorMode() -> VectorMode {
    return static_cast<VectorMode>(this->m_object->GetVectorMode());
}
