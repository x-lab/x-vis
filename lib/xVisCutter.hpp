#pragma once

#include "xVisImplicitFunction.hpp"
#include "xVisPolyDataAlgorithm.hpp"

#include <vtkCutter.h>

class xVisImplicitFunction;

class xVisCutter : public xVisPolyDataAlgorithm
{
    Q_OBJECT
    Q_PROPERTY(xVisImplicitFunction* cutFunction READ getCutFunction WRITE setCutFunction NOTIFY cutFunctionChanged);

private:
    xVisImplicitFunction::cb_t m_cutFunctionCb;
    xVisImplicitFunction* m_cutFunction = nullptr;
    vtkSmartPointer<vtkCutter> m_object = nullptr;

public:
     xVisCutter();
    ~xVisCutter();

    auto updateCutFunction() -> void;
    auto setCutFunction(xVisImplicitFunction*) -> void;
    auto getCutFunction() -> xVisImplicitFunction*;

signals:
    void cutFunctionChanged();
};
