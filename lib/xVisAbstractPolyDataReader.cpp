#include "xVisAbstractPolyDataReader.hpp"

xVisAbstractPolyDataReader::xVisAbstractPolyDataReader(vtkSmartPointer<vtkAbstractPolyDataReader> vtkObject) : xVisPolyDataAlgorithm(vtkObject) {
    this->m_vtkObject = vtkObject;
}

auto xVisAbstractPolyDataReader::isValid() -> bool {
    if (!QFileInfo::exists(this->m_fileName)) {
        return false;
    }

    return true;
}

auto xVisAbstractPolyDataReader::setFileName(const QString& fileName) -> void {
    this->m_fileName = fileName;

    emit this->fileNameChanged();

    if (QFileInfo::exists(fileName)) {
        this->m_vtkObject->SetFileName(fileName.toStdString().c_str());
        this->m_vtkObject->Update();
        this->update();
    }
}

auto xVisAbstractPolyDataReader::getFileName() -> QString {
    return this->m_fileName;
}
