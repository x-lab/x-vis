#pragma once

#include "xVisAlgorithm.hpp"

#include <vtkSmartPointer.h>
#include <vtkAbstractMapper.h>

class xVisAbstractMapper : public xVisAlgorithm
{
    Q_OBJECT

public:
    xVisAbstractMapper(vtkSmartPointer<vtkAbstractMapper>);
};
