#include "xVisVolumeProperty.hpp"
#include "xVisVolume.hpp"
#include "xVisPiecewiseFunction.hpp"
#include "xVisColorTransferFunction.hpp"

xVisVolumeProperty::xVisVolumeProperty(xVisVolume* volume) : m_volume(volume), m_volume_ptr(volume->get()) {
    auto property = this->m_volume_ptr->GetProperty();

    this->m_gradientOpacityFunction = new xVisPiecewiseFunction(property->GetGradientOpacity(), [this](){ this->update(); });
    this->m_scalarOpacityFunction = new xVisPiecewiseFunction(property->GetScalarOpacity(), [this](){ this->update(); });
    this->m_transferFunction = new xVisColorTransferFunction(property->GetRGBTransferFunction(), [this](){ this->update(); });
}

auto xVisVolumeProperty::update() -> void {
    this->m_volume_ptr->Update();
}

auto xVisVolumeProperty::setShade(bool shade) -> void {
    this->m_volume_ptr->GetProperty()->SetShade(shade);
    emit this->shadeChanged();
    this->update();
}

auto xVisVolumeProperty::getShade() -> bool {
    return this->m_volume_ptr->GetProperty()->GetShade();
}

auto xVisVolumeProperty::setAmbient(qreal ambient) -> void {
    this->m_volume_ptr->GetProperty()->SetAmbient(ambient);
    emit this->ambientChanged();
    this->update();
}

auto xVisVolumeProperty::getAmbient() -> qreal {
    return this->m_volume_ptr->GetProperty()->GetAmbient();
}

auto xVisVolumeProperty::setDiffuse(qreal diffuse) -> void {
    this->m_volume_ptr->GetProperty()->SetDiffuse(diffuse);
    emit this->diffuseChanged();
    this->update();
}

auto xVisVolumeProperty::getDiffuse() -> qreal {
    return this->m_volume_ptr->GetProperty()->GetDiffuse();
}

auto xVisVolumeProperty::setSpecular(qreal specular) -> void {
    this->m_volume_ptr->GetProperty()->SetSpecular(specular);
    emit this->specularChanged();
    this->update();
}

auto xVisVolumeProperty::getSpecular() -> qreal {
    return this->m_volume_ptr->GetProperty()->GetSpecular();
}

auto xVisVolumeProperty::setSpecularPower(qreal specularPower) -> void {
    this->m_volume_ptr->GetProperty()->SetSpecularPower(specularPower);
    emit this->specularPowerChanged();
    this->update();
}

auto xVisVolumeProperty::getSpecularPower() -> qreal {
    return this->m_volume_ptr->GetProperty()->GetSpecularPower();
}

auto xVisVolumeProperty::getGradientOpacityFunction() -> xVisPiecewiseFunction* {
    return this->m_gradientOpacityFunction;
}

auto xVisVolumeProperty::getScalarOpacityFunction() -> xVisPiecewiseFunction* {
    return this->m_scalarOpacityFunction;
}

auto xVisVolumeProperty::getTransferFunction() -> xVisColorTransferFunction* {
    return this->m_transferFunction;
}
