#include "xVisAdaptiveSubdivisionFilter.hpp"

xVisAdaptiveSubdivisionFilter::xVisAdaptiveSubdivisionFilter(void) : xVisPolyDataAlgorithm(vtkSmartPointer<vtkAdaptiveSubdivisionFilter>::New())
{
    this->m_object = vtkAdaptiveSubdivisionFilter::SafeDownCast(xVisAlgorithm::get());
}

auto xVisAdaptiveSubdivisionFilter::setMaximumEdgeLength(qreal maximumEdgeLength) -> void
{
    this->m_object->SetMaximumEdgeLength(maximumEdgeLength);
    emit this->maximumEdgeLengthChanged();
    this->update();
}

auto xVisAdaptiveSubdivisionFilter::getMaximumEdgeLength(void) -> qreal
{
    return this->m_object->GetMaximumEdgeLength();
}

auto xVisAdaptiveSubdivisionFilter::setMaximumTriangleArea(qreal maximumTriangleArea) -> void
{
    this->m_object->SetMaximumTriangleArea(maximumTriangleArea);
    emit this->maximumTriangleAreaChanged();
    this->update();
}

auto xVisAdaptiveSubdivisionFilter::getMaximumTriangleArea() -> qreal
{
    return this->m_object->GetMaximumTriangleArea();
}

auto xVisAdaptiveSubdivisionFilter::setMaximumNumberOfTriangles(int maximumNumberOfTriangles) -> void
{
    this->m_object->SetMaximumNumberOfTriangles(maximumNumberOfTriangles);
    emit this->maximumNumberOfTrianglesChanged();
    this->update();
}

auto xVisAdaptiveSubdivisionFilter::getMaximumNumberOfTriangles() -> int
{
    return this->m_object->GetMaximumNumberOfTriangles();
}

auto xVisAdaptiveSubdivisionFilter::setMaximumNumberOfPasses(int maximumNumberOfPasses) -> void
{
    this->m_object->SetMaximumNumberOfPasses(maximumNumberOfPasses);
    emit this->maximumNumberOfPassesChanged();
    this->update();
}

auto xVisAdaptiveSubdivisionFilter::getMaximumNumberOfPasses() -> int
{
    return this->m_object->GetMaximumNumberOfPasses();
}
