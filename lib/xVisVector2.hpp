#pragma once

#include <array>
#include <functional>

#include <QObject>

class xVisVector2 : public QObject
{
    Q_OBJECT
    Q_PROPERTY(double x READ getX WRITE setX NOTIFY xChanged);
    Q_PROPERTY(double y READ getY WRITE setY NOTIFY yChanged);

public:
    using cb_t = std::function<void()>;
    using array_t = std::array<double, 2>;

public:
    xVisVector2(void) = delete;
    xVisVector2(cb_t&&, array_t = {{0,0}});

    void setX(double);
    void setY(double);

    double getX(void);
    double getY(void);

    array_t getValues(void);

    // static Qml::Register::UncreatableClass<Vector2> Register;

signals:
    void xChanged(void);
    void yChanged(void);

private:
    cb_t m_callback;
    array_t m_values;
    void notify(void);
};
