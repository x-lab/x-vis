#pragma once

#include "xVisVector3.hpp"
#include "xVisImplicitFunction.hpp"

#include <vtkPlane.h>

class xVisPlane : public xVisImplicitFunction {
    Q_OBJECT
    Q_PROPERTY(xVisVector3* origin READ getOrigin CONSTANT);
    Q_PROPERTY(xVisVector3* normal READ getNormal CONSTANT);
private:
    vtkSmartPointer<vtkPlane> m_object = nullptr;
    xVisVector3* m_origin = nullptr;
    xVisVector3* m_normal = nullptr;
public:
    xVisPlane();
    auto getOrigin() -> xVisVector3*;
    auto getNormal() -> xVisVector3*;
};
