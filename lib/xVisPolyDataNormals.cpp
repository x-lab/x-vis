#include "xVisPolyDataNormals.hpp"

xVisPolyDataNormals::xVisPolyDataNormals() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkPolyDataNormals>::New()) {
    this->m_object = vtkPolyDataNormals::SafeDownCast(xVisAlgorithm::get());
}

auto xVisPolyDataNormals::setFeatureAngle(qreal featureAngle) -> void {
    this->m_object->SetFeatureAngle(featureAngle);
    emit this->featureAngleChanged();
    this->update();
}

auto xVisPolyDataNormals::getFeatureAngle() -> qreal {
    return this->m_object->GetFeatureAngle();
}

auto xVisPolyDataNormals::setSplitting(bool splitting) -> void {
    this->m_object->SetSplitting(splitting);
    emit this->splittingChanged();
    this->update();
}

auto xVisPolyDataNormals::getSplitting() -> bool {
    return this->m_object->GetSplitting();
}

auto xVisPolyDataNormals::setConsistency(bool consistency) -> void {
    this->m_object->SetConsistency(consistency);
    emit this->consistencyChanged();
    this->update();
}

auto xVisPolyDataNormals::getConsistency() -> bool {
    return this->m_object->GetConsistency();
}

auto xVisPolyDataNormals::setAutoOrientNormals(bool autoOrientNormals) -> void {
    this->m_object->SetAutoOrientNormals(autoOrientNormals);
    emit this->autoOrientNormalsChanged();
    this->update();
}

auto xVisPolyDataNormals::getAutoOrientNormals() -> bool {
    return this->m_object->GetAutoOrientNormals();
}

auto xVisPolyDataNormals::setComputePointNormals(bool computePointNormals) -> void {
    this->m_object->SetComputePointNormals(computePointNormals);
    emit this->computePointNormalsChanged();
    this->update();
}

auto xVisPolyDataNormals::getComputePointNormals() -> bool {
    return this->m_object->GetComputePointNormals();
}

auto xVisPolyDataNormals::setComputeCellNormals(bool computeCellNormals) -> void {
    this->m_object->SetComputeCellNormals(computeCellNormals);
    emit this->computeCellNormalsChanged();
    this->update();
}

auto xVisPolyDataNormals::getComputeCellNormals() -> bool {
    return this->m_object->GetComputeCellNormals();
}

auto xVisPolyDataNormals::setFlipNormals(bool flipNormals) -> void {
    this->m_object->SetFlipNormals(flipNormals);
    emit this->flipNormalsChanged();
    this->update();
}

auto xVisPolyDataNormals::getFlipNormals() -> bool {
    return this->m_object->GetFlipNormals();
}

auto xVisPolyDataNormals::setNonManifoldTraversal(bool nonManifoldTraversal) -> void {
    this->m_object->SetNonManifoldTraversal(nonManifoldTraversal);
    emit this->nonManifoldTraversalChanged();
    this->update();
}

auto xVisPolyDataNormals::getNonManifoldTraversal() -> bool {
    return this->m_object->GetNonManifoldTraversal();
}
