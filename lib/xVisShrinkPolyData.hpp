#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkShrinkPolyData.h>

class xVisShrinkPolyData : public xVisPolyDataAlgorithm {
    Q_OBJECT
    Q_PROPERTY(qreal shrinkFactor READ getShrinkFactor WRITE setShrinkFactor NOTIFY shrinkFactorChanged);
private:
    vtkSmartPointer<vtkShrinkPolyData> m_object = nullptr;
public:
    xVisShrinkPolyData();
    auto setShrinkFactor(qreal) -> void;
    auto getShrinkFactor() -> qreal;
signals:
    void shrinkFactorChanged();
};
