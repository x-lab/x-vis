#include "xVisBooleanOperationPolyDataFilter.hpp"

xVisBooleanOperationPolyDataFilter::xVisBooleanOperationPolyDataFilter() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkBooleanOperationPolyDataFilter>::New()) {
    this->m_object = vtkBooleanOperationPolyDataFilter::SafeDownCast(xVisAlgorithm::get());
}

auto xVisBooleanOperationPolyDataFilter::setOperation(Operation operation) -> void {
    this->m_object->SetOperation(operation);
    emit this->operationChanged();
    this->update();
}

auto xVisBooleanOperationPolyDataFilter::getOperation() -> Operation {
    return static_cast<Operation>(this->m_object->GetOperation());
}

auto xVisBooleanOperationPolyDataFilter::setTolerance(qreal tolerance) -> void {
    this->m_object->SetTolerance(tolerance);
    emit this->toleranceChanged();
    this->update();
}

auto xVisBooleanOperationPolyDataFilter::getTolerance() -> qreal {
    return this->m_object->GetTolerance();
}

auto xVisBooleanOperationPolyDataFilter::setReorientDifferentCells(bool reorientDifferentCells) -> void {
    this->m_object->SetReorientDifferenceCells(reorientDifferentCells);
    emit this->reorientDifferentCellsChanged();
    this->update();
}

auto xVisBooleanOperationPolyDataFilter::getReorientDifferentCells() -> bool {
    return this->m_object->GetReorientDifferenceCells();
}
