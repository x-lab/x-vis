#pragma once

#include "xVisImageAlgorithm.hpp"

#include <vtkImageReader2.h>

class xVisImageReader2 : public xVisImageAlgorithm {
    Q_OBJECT
    Q_PROPERTY(QString fileName READ getFileName WRITE setFileName NOTIFY fileNameChanged);
private:
    vtkSmartPointer<vtkImageReader2> m_object = nullptr;
    QString m_fileName;
public:
    xVisImageReader2(vtkSmartPointer<vtkImageReader2>);
    auto setFileName(const QString&) -> void;
    auto getFileName() -> QString;
    virtual auto isValid() -> bool;
signals:
    void fileNameChanged();
};
