#pragma once

#include "xVisAbstractWidget.hpp"

#include <vtkBoxWidget2.h>

class xVisBoxWidget2 : public xVisAbstractWidget
{
    Q_OBJECT

public:
    xVisBoxWidget2(void);

private:
    vtkSmartPointer<vtkAbstractWidget> m_object = nullptr;
};
