#include "xVisActor.hpp"
#include "xVisMapper.hpp"
#include "xVisTexture.hpp"
#include "xVisProperty.hpp"

xVisActor::xVisActor(void) : xVisProp3D(vtkSmartPointer<vtkActor>::New())
{
    this->m_object = vtkActor::SafeDownCast(xVisProp::get());
    this->m_property = new xVisProperty(this);
    this->m_texture = new xVisTexture(this->m_object->GetTexture(), [this] (void) {
        this->update();
    });
}

xVisActor::xVisActor(vtkSmartPointer<vtkActor> vtkObject) : xVisProp3D(vtkObject)
{
    this->m_object = vtkObject;
    this->m_property = new xVisProperty(this);
}

xVisActor::~xVisActor(void)
{
    delete this->m_property;
}

vtkSmartPointer<vtkActor> xVisActor::get(void)
{
    return this->m_object;
}

xVisProperty *xVisActor::getProperty(void)
{
    return this->m_property;
}

void xVisActor::setMapper(xVisMapper *mapper)
{
    this->m_mapper = mapper;
    this->m_object->SetMapper(mapper->get());
    mapper->setProp(this);

    emit this->mapperChanged();

    this->update();
}

xVisMapper *xVisActor::getMapper(void)
{
    return this->m_mapper;
}

xVisTexture *xVisActor::getTexture(void)
{
    return this->m_texture;
}
