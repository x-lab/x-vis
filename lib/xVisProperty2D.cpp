#include "xVisProperty2D.hpp"
#include "xVisActor2D.hpp"

xVisProperty2D::xVisProperty2D(xVisActor2D* actor2D) : m_actor2D(actor2D), m_vtkActor2D(actor2D->get()) {
}

auto xVisProperty2D::update() -> void {
    this->m_actor2D->update();
}

auto xVisProperty2D::setColor(const QColor& color) -> void {
    this->m_color = color;

    this->m_vtkActor2D->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());
    this->update();

    emit this->colorChanged();
}

auto xVisProperty2D::getColor() -> QColor {
    return this->m_color;
}

auto xVisProperty2D::setOpacity(qreal opacity) -> void {
    this->m_vtkActor2D->GetProperty()->SetOpacity(opacity);
    this->update();

    emit this->opacityChanged();
}

auto xVisProperty2D::getOpacity() -> qreal {
    return this->m_vtkActor2D->GetProperty()->GetOpacity();
}
