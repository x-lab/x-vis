#pragma once

#include "xVisPolyDataAlgorithm.hpp"
#include "xVisRandomSequence.hpp"
#include "xVisVector3.hpp"

#include <vtkPointSource.h>

class xVisPointSource : public xVisPolyDataAlgorithm {
    Q_OBJECT
public:
    enum Distribution {
        UniformDistribution = VTK_POINT_UNIFORM,
        ShellDistribution = VTK_POINT_SHELL
    };
private:
    Q_ENUM(Distribution);
    Q_PROPERTY(xVisVector3* center READ getCenter CONSTANT);
    Q_PROPERTY(Distribution distribution READ getDistribution WRITE setDistribution NOTIFY distributionChanged);
    Q_PROPERTY(xVisRandomSequence* randomSequence READ getRandomSequence WRITE setRandomSequence NOTIFY randomSequenceChanged);
    Q_PROPERTY(int numberOfPoints READ getNumberOfPoints WRITE setNumberOfPoints NOTIFY numberOfPointsChanged);
    Q_PROPERTY(qreal radius READ getRadius WRITE setRadius NOTIFY radiusChanged);
    Q_CLASSINFO("DefaultProperty", "randomSequence");
private:
    vtkSmartPointer<vtkPointSource> m_object = nullptr;
    xVisVector3* m_center = nullptr;
    xVisRandomSequence* m_randomSequence = nullptr;
private:
    auto resetSeed() -> void;
public:
    xVisPointSource();
    auto setDistribution(Distribution) -> void;
    auto getDistribution() -> Distribution;
    auto setNumberOfPoints(int) -> void;
    auto getNumberOfPoints() -> int;
    auto setRadius(qreal) -> void;
    auto getRadius() -> qreal;
    auto getCenter() -> xVisVector3*;
    auto setRandomSequence(xVisRandomSequence*) -> void;
    auto getRandomSequence() -> xVisRandomSequence*;
signals:
    void randomSequenceChanged();
    void distributionChanged();
    void numberOfPointsChanged();
    void radiusChanged();
};
