#include "xVisTextSource.hpp"

xVisTextSource::xVisTextSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkTextSource>::New()) {
    this->m_object = vtkTextSource::SafeDownCast(xVisAlgorithm::get());
}

auto xVisTextSource::setText(const QString& text) -> void {
    this->m_object->SetText(text.toStdString().c_str());
    this->update();
    emit this->textChanged();
}

auto xVisTextSource::getText() -> QString {
    return this->m_object->GetText();
}

auto xVisTextSource::setBacking(bool backing) -> void {
    this->m_object->SetBacking(backing);
    this->update();
    emit this->backingChanged();
}

auto xVisTextSource::getBacking() -> bool {
    return this->m_object->GetBacking();
}

auto xVisTextSource::setForegroundColor(const QColor& foregroundColor) -> void {
    this->m_foregroundColor = foregroundColor;
    this->m_object->SetForegroundColor(foregroundColor.redF(), foregroundColor.greenF(), foregroundColor.blueF());
    this->update();
    emit this->foregroundColorChanged();
}

auto xVisTextSource::getForegroundColor() -> QColor {
    return this->m_foregroundColor;
}

auto xVisTextSource::setBackgroundColor(const QColor& backgroundColor) -> void {
    this->m_backgroundColor = backgroundColor;
    this->m_object->SetBackgroundColor(backgroundColor.redF(), backgroundColor.greenF(), backgroundColor.blueF());
    this->update();
    emit this->backgroundColorChanged();
}

auto xVisTextSource::getBackgroundColor() -> QColor {
    return this->m_backgroundColor;
}
