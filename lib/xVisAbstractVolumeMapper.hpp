#pragma once

#include "xVisAbstractMapper3D.hpp"

#include <vtkAbstractVolumeMapper.h>

class xVisAbstractVolumeMapper : public xVisAbstractMapper3D
{
    Q_OBJECT

public:
    xVisAbstractVolumeMapper(vtkSmartPointer<vtkAbstractVolumeMapper>);
    vtkSmartPointer<vtkAbstractVolumeMapper> get(void);
};
