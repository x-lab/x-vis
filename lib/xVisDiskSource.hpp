#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkDiskSource.h>

class xVisDiskSource : public xVisPolyDataAlgorithm
{
    Q_OBJECT
    Q_PROPERTY(qreal innerRadius READ getInnerRadius WRITE setInnerRadius NOTIFY innerRadiusChanged);
    Q_PROPERTY(qreal outerRadius READ getOuterRadius WRITE setOuterRadius NOTIFY outerRadiusChanged);
    Q_PROPERTY(int radialResolution READ getRadialResolution WRITE setRadialResolution NOTIFY radialResolutionChanged);
    Q_PROPERTY(int circumferentialResolution READ getCircumferentialResolution WRITE setCircumferentialResolution NOTIFY circumferentialResolutionChanged);

private:
    vtkSmartPointer<vtkDiskSource> m_object = nullptr;

public:
    xVisDiskSource();
    auto setInnerRadius(qreal) -> void;
    auto setOuterRadius(qreal) -> void;
    auto setRadialResolution(int) -> void;
    auto setCircumferentialResolution(int) -> void ;
    auto getInnerRadius() -> qreal;
    auto getOuterRadius() -> qreal;
    auto getRadialResolution() -> int;
    auto getCircumferentialResolution() -> int;

signals:
    void innerRadiusChanged();
    void outerRadiusChanged();
    void radialResolutionChanged();
    void circumferentialResolutionChanged();
};
