#include "xVisPlatonicSolidSource.hpp"

xVisPlatonicSolidSource::xVisPlatonicSolidSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkPlatonicSolidSource>::New()) {
    this->m_object = vtkPlatonicSolidSource::SafeDownCast(xVisAlgorithm::get());
}

auto xVisPlatonicSolidSource::setSolidType(SolidType solidType) -> void {
    this->m_object->SetSolidType(solidType);
    this->update();
    emit this->solidTypeChanged();
}

auto xVisPlatonicSolidSource::getSolidType() -> SolidType {
    return static_cast<SolidType>(this->m_object->GetSolidType());
}
