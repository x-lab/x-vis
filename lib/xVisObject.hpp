#pragma once

#include <QtCore>
#include <QtQml>

class xVisObject : public QObject
{
    Q_OBJECT

public:
    enum class Type {
        Data,
        Prop,
        Algorithm,
        Widget,
        Other
    };

public:
    xVisObject(Type);

    Type getType(void);

private:
    Type m_type;
};
