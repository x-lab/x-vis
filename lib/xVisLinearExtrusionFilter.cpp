#include "xVisLinearExtrusionFilter.hpp"

xVisLinearExtrusionFilter::xVisLinearExtrusionFilter() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkLinearExtrusionFilter>::New()) {
    this->m_object = vtkLinearExtrusionFilter::SafeDownCast(xVisAlgorithm::get());

    this->m_vector = new xVisVector3([this](){
        this->m_object->SetVector(this->m_vector->getValues().data());
        this->update();
    });

    this->m_extrusionPoint = new xVisVector3([this](){
        this->m_object->SetExtrusionPoint(this->m_extrusionPoint->getValues().data());
        this->update();
    });
}

auto xVisLinearExtrusionFilter::setExtrusionType(ExtrusionType extrusionType) -> void {
    this->m_object->SetExtrusionType(extrusionType);
    emit this->extrusionTypeChanged();
    this->update();
}

auto xVisLinearExtrusionFilter::getExtrusionType() -> ExtrusionType {
    return static_cast<ExtrusionType>(this->m_object->GetExtrusionType());
}

auto xVisLinearExtrusionFilter::setScaleFactor(qreal scaleFactor) -> void {
    this->m_object->SetScaleFactor(scaleFactor);
    emit this->scaleFactorChanged();
    this->update();
}

auto xVisLinearExtrusionFilter::getScaleFactor() -> qreal {
    return this->m_object->GetScaleFactor();
}

auto xVisLinearExtrusionFilter::setCapping(bool capping) -> void {
    this->m_object->SetCapping(capping);
    emit this->cappingChanged();
    this->update();
}

auto xVisLinearExtrusionFilter::getCapping() -> bool {
    return this->m_object->GetCapping();
}

auto xVisLinearExtrusionFilter::getVector() -> xVisVector3* {
    return this->m_vector;
}

auto xVisLinearExtrusionFilter::getExtrusionPoint() -> xVisVector3* {
    return this->m_extrusionPoint;
}
