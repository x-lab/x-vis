#pragma once

#include "xVisProp.hpp"
#include "xVisVector3.hpp"

#include <vtkSmartPointer.h>
#include <vtkProp3D.h>

class xVisProp3D : public xVisProp
{
    Q_OBJECT
    Q_PROPERTY(xVisVector3 *scale READ getScale CONSTANT);
    Q_PROPERTY(xVisVector3 *origin READ getOrigin CONSTANT);
    Q_PROPERTY(xVisVector3 *position READ getPosition CONSTANT);
    Q_PROPERTY(xVisVector3 *orientation READ getOrientation CONSTANT);

public:
    xVisProp3D(vtkSmartPointer<vtkProp3D>);
    xVisVector3 *getScale(void);
    xVisVector3 *getOrigin(void);
    xVisVector3 *getPosition(void);
    xVisVector3 *getOrientation(void);

private:
    xVisVector3 *m_scale = nullptr;
    xVisVector3 *m_origin = nullptr;
    xVisVector3 *m_position = nullptr;
    xVisVector3 *m_orientation = nullptr;
    vtkSmartPointer<vtkProp3D> m_object = nullptr;
};
