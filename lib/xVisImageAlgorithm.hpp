#pragma once

#include "xVisAlgorithm.hpp"

#include <vtkImageAlgorithm.h>

class xVisImageAlgorithm : public xVisAlgorithm
{
    Q_OBJECT

public:
    xVisImageAlgorithm(vtkSmartPointer<vtkImageAlgorithm>);
};
