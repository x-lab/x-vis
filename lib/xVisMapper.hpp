#pragma once

#include "xVisAbstractMapper3D.hpp"

#include <vtkMapper.h>

class xVisScalarsToColors;

class xVisMapper : public xVisAbstractMapper3D {
    Q_OBJECT
public:
    enum ScalarMode {
        ScalarModeDefault = VTK_SCALAR_MODE_DEFAULT,
        ScalarModeUsePointData = VTK_SCALAR_MODE_USE_POINT_DATA,
        ScalarModeUseCellData = VTK_SCALAR_MODE_USE_CELL_DATA,
        ScalarModeUsePointFieldData = VTK_SCALAR_MODE_USE_POINT_FIELD_DATA,
        ScalarModeUseCellFieldData = VTK_SCALAR_MODE_USE_CELL_FIELD_DATA
    };
private:
    Q_ENUM(ScalarMode);
    Q_PROPERTY(ScalarMode scalarMode READ getScalarMode WRITE setScalarMode NOTIFY scalarModeChanged);
    Q_PROPERTY(bool scalarVisibility READ getScalarVisibility WRITE setScalarVisibility NOTIFY scalarVisibilityChanged);
private:
    vtkSmartPointer<vtkMapper> m_object = nullptr;
public:
    xVisMapper(vtkSmartPointer<vtkMapper>);
    auto get() -> vtkSmartPointer<vtkMapper>;
    auto setScalarMode(ScalarMode) -> void;
    auto getScalarMode() -> ScalarMode;
    auto setScalarVisibility(bool) -> void;
    auto getScalarVisibility() -> bool;
signals:
    void scalarVisibilityChanged();
    void scalarModeChanged();
};
