#pragma once

#include "xVisAbstractMapper.hpp"

#include <vtkAbstractMapper3D.h>

class xVisAbstractMapper3D : public xVisAbstractMapper
{
    Q_OBJECT

public:
    xVisAbstractMapper3D(vtkSmartPointer<vtkAbstractMapper3D>);
};
