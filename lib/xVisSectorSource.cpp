#include "xVisSectorSource.hpp"

xVisSectorSource::xVisSectorSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkSectorSource>::New()) {
    this->m_object = vtkSectorSource::SafeDownCast(xVisAlgorithm::get());
}

auto xVisSectorSource::setInnerRadius(qreal radius) -> void {
    this->m_object->SetInnerRadius(radius);
    emit this->innerRadiusChanged();
    this->update();
}

auto xVisSectorSource::getInnerRadius() -> qreal {
    return this->m_object->GetInnerRadius();
}

auto xVisSectorSource::setOuterRadius(qreal radius) -> void {
    this->m_object->SetOuterRadius(radius);
    emit this->outerRadiusChanged();
    this->update();
}

auto xVisSectorSource::getOuterRadius() -> qreal {
    return this->m_object->GetOuterRadius();
}

auto xVisSectorSource::setRadialResolution(int value) -> void {
    this->m_object->SetRadialResolution(value);
    emit this->radialResolutionChanged();
    this->update();
}

auto xVisSectorSource::getRadialResolution() -> int {
    return this->m_object->GetRadialResolution();
}

auto xVisSectorSource::setCircumferentialResolution(int value) -> void {
    this->m_object->SetCircumferentialResolution(value);
    emit this->circumferentialResolutionChanged();
    this->update();
}

auto xVisSectorSource::getCircumferentialResolution() -> int {
    return this->m_object->GetCircumferentialResolution();
}

auto xVisSectorSource::setStartAngle(qreal startAngle) -> void {
    this->m_object->SetStartAngle(startAngle);
    this->update();
    emit this->startAngleChanged();
}

auto xVisSectorSource::getStartAngle() -> qreal {
    return this->m_object->GetStartAngle();
}

auto xVisSectorSource::setEndAngle(qreal endAngle) -> void {
    this->m_object->SetEndAngle(endAngle);
    this->update();
    emit this->endAngleChanged();
}

auto xVisSectorSource::getEndAngle() -> qreal {
    return this->m_object->GetEndAngle();
}

auto xVisSectorSource::setZCoord(qreal zCoord) -> void {
    this->m_object->SetZCoord(zCoord);
    this->update();
    emit this->zCoordChanged();
}

auto xVisSectorSource::getZCoord() -> qreal {
    return this->m_object->GetZCoord();
}
