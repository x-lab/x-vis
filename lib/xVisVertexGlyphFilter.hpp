#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkVertexGlyphFilter.h>

class xVisVertexGlyphFilter : public xVisPolyDataAlgorithm {
    Q_OBJECT
public:
    xVisVertexGlyphFilter();
};
