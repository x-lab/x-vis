#include "xVisActor2D.hpp"
#include "xVisMapper2D.hpp"
#include "xVisProperty2D.hpp"

xVisActor2D::xVisActor2D(void) : xVisProp(vtkSmartPointer<vtkActor2D>::New())
{
    this->m_object = vtkActor2D::SafeDownCast(xVisProp::get());
    this->m_property = new xVisProperty2D(this);
}

xVisActor2D::xVisActor2D(vtkSmartPointer<vtkActor2D> vtkObject) : xVisProp(vtkObject)
{
    this->m_object = vtkActor2D::SafeDownCast(vtkObject);
    this->m_property = new xVisProperty2D(this);
}

xVisActor2D::~xVisActor2D(void)
{
    delete this->m_property;
}

vtkSmartPointer<vtkActor2D> xVisActor2D::get(void)
{
    return vtkActor2D::SafeDownCast(xVisProp::get());
}

void xVisActor2D::setMapper(xVisMapper2D *mapper)
{
    this->m_mapper = mapper;
    this->m_object->SetMapper(mapper->get());
    mapper->setProp(this);

    emit this->mapperChanged();

    this->update();
}

xVisMapper2D *xVisActor2D::getMapper(void)
{
    return this->m_mapper;
}

xVisProperty2D *xVisActor2D::getProperty(void)
{
    return this->m_property;
}

void xVisActor2D::setWidth(qreal width)
{
    this->m_object->SetWidth(width);
    emit this->widthChanged();
    this->update();
}

qreal xVisActor2D::getWidth(void)
{
    return this->m_object->GetWidth();
}

void xVisActor2D::setHeight(qreal height)
{
    this->m_object->SetHeight(height);
    emit this->heightChanged();
    this->update();
}

qreal xVisActor2D::getHeight(void)
{
    return this->m_object->GetHeight();
}
