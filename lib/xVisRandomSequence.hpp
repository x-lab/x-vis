#pragma once

#include "xVisObject.hpp"

#include <vtkRandomSequence.h>
#include <vtkSmartPointer.h>

class xVisRandomSequence : public xVisObject {
    Q_OBJECT
    Q_PROPERTY(int seed READ getSeed WRITE setSeed NOTIFY seedChanged);
private:
    vtkSmartPointer<vtkRandomSequence> m_object = nullptr;
    int m_seed;
public:
    xVisRandomSequence() = delete;
    xVisRandomSequence(vtkSmartPointer<vtkRandomSequence>);
    auto get() -> vtkSmartPointer<vtkRandomSequence>;
    auto setSeed(int) -> void;
    auto getSeed() -> int;
    auto initialize() -> void;
signals:
    void seedChanged();
};
