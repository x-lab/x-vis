#include "xVisFboRenderer.hpp"
#include "xVisFboOffscreenWindow.hpp"
#include "xVisViewer.hpp"

#ifdef _MSC_VER
#include "xVisWin32Interactor.hpp"
#else
#include "xVisGenericInteractor.hpp"
#endif

#include <vtkRendererCollection.h>
#include <vtkCamera.h>
#include <vtkCommand.h>

xVisFboRenderer::xVisFboRenderer(xVisFboOffscreenWindow *offscreenWindow) : m_fboOffscreenWindow(offscreenWindow), m_fbo(0) {

    m_fboOffscreenWindow->Register(NULL);
    m_fboOffscreenWindow->QtParentRenderer = this;

#ifdef _MSC_VER
    m_interactor = vtkSmartPointer<xVisWin32Interactor>::New();
#else
    m_interactor = vtkSmartPointer<xVisGenericInteractor>::New();
#endif

    m_interactorStyle = vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();

    m_interactor->SetRenderWindow(offscreenWindow);
    m_interactor->SetInteractorStyle(m_interactorStyle);
}

auto xVisFboRenderer::synchronize(QQuickFramebufferObject* fbo) -> void {

    if (!m_fbo) {
        auto viewer = static_cast<xVisViewer*>(fbo);
        viewer->init();
    }

    if(m_fbo) {
        int *rendererSize = m_fboOffscreenWindow->GetSize();
        if (fbo->width() != rendererSize[0] || fbo->height() != rendererSize[1]) {
            m_fboOffscreenWindow->Resize(fbo->width(), fbo->height());
        }
    }
}

auto xVisFboRenderer::render() -> void {

    m_interactor->Disable();
    m_fboOffscreenWindow->PushState();
    m_fboOffscreenWindow->OpenGLInitState();
    m_fboOffscreenWindow->InternalRender();
    m_fboOffscreenWindow->PopState();
    m_interactor->Enable();
}

auto xVisFboRenderer::createFramebufferObject(const QSize& size) -> QOpenGLFramebufferObject * {

    QOpenGLFramebufferObjectFormat format;
    format.setAttachment(QOpenGLFramebufferObject::Depth);

#ifdef Q_OS_MAC
    QSize macSize = QSize(size.width() / 2, size.height() / 2);
    m_fbo = new QOpenGLFramebufferObject(macSize, format);
#else
    m_fbo = new QOpenGLFramebufferObject(size, format);
#endif

    m_fboOffscreenWindow->SetFramebufferObject(m_fbo);

    return m_fbo;
}

bool xVisFboRenderer::keyPressed(int keycode) {
    QMap<int, char> acceptedKeys = { {Qt::Key_R, 'r'},
                                     {Qt::Key_S, 's'},
                                     {Qt::Key_W, 'w'},
                                     {Qt::Key_A, 'a'},
                                     {Qt::Key_U, 'u'}};
    char key = acceptedKeys.value(keycode, ' ');
    this->m_interactor->SetKeyCode(key);
    this->m_interactor->InvokeEvent(vtkCommand::CharEvent, nullptr);
    return acceptedKeys.contains(keycode);
}

auto xVisFboRenderer::onMouseMoveEvent(QMouseEvent* event) -> void {

    this->m_interactor->SetEventInformationFlipY(event->position().x(), event->position().y(), (event->modifiers() & Qt::ControlModifier), (event->modifiers() & Qt::ShiftModifier));
    this->m_interactor->InvokeEvent(vtkCommand::MouseMoveEvent, event);
}

auto xVisFboRenderer::onMouseEvent(QMouseEvent* event) -> void {

    if(this->m_interactor == nullptr || event == nullptr)
        return;

    if(!this->m_interactor->GetEnabled())
        return;

    this->m_interactor->SetEventInformationFlipY(event->position().x(), event->position().y(), (event->modifiers() & Qt::ControlModifier), (event->modifiers() & Qt::ShiftModifier));

    auto command = vtkCommand::NoEvent;

    if (event->type() == QEvent::MouseButtonPress || event->type() == QEvent::MouseButtonDblClick) {
        switch (event->button()) {
            case Qt::LeftButton:    command = vtkCommand::LeftButtonPressEvent; break;
            case Qt::RightButton:   command = vtkCommand::RightButtonPressEvent; break;
            case Qt::MiddleButton:  command = vtkCommand::MiddleButtonPressEvent; break;
            default: break;
        }
    } else if (event->type() == QEvent::MouseButtonRelease) {
        switch (event->button()) {
            case Qt::LeftButton:    command = vtkCommand::LeftButtonReleaseEvent; break;
            case Qt::RightButton:   command = vtkCommand::RightButtonReleaseEvent; break;
            case Qt::MiddleButton:  command = vtkCommand::MiddleButtonReleaseEvent; break;
            default: break;
        }
    }

    this->m_interactor->InvokeEvent(command, event);
}

auto xVisFboRenderer::onWheelEvent(QWheelEvent* event) -> void {

    if(this->m_interactor == nullptr || event == nullptr)
        return;

    if(!this->m_interactor->GetEnabled())
        return;

    auto command = vtkCommand::NoEvent;
    //qDebug()<<Q_FUNC_INFO<<event->angleDelta();
    if (event->angleDelta().y() > 0) {
        command = vtkCommand::MouseWheelForwardEvent;
    } else {
        command = vtkCommand::MouseWheelBackwardEvent;
    }

    this->m_interactor->InvokeEvent(command, event);
}

xVisFboRenderer::~xVisFboRenderer(void) {
    m_fboOffscreenWindow->QtParentRenderer = 0;
    m_fboOffscreenWindow->Delete();
}
