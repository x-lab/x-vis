#pragma once

#include <QtCore>

#include "xVisPointSetAlgorithm.hpp"
#include "xVisVector3.hpp"

#include <vtkWarpTo.h>

class xVisWarpTo : public xVisPointSetAlgorithm {
  Q_OBJECT
  Q_PROPERTY(xVisVector3 *position READ getPosition CONSTANT);
  Q_PROPERTY(qreal scaleFactor READ getScaleFactor WRITE setScaleFactor NOTIFY
                 scaleFactorChanged);
  Q_PROPERTY(
      bool absolute READ getAbsolute WRITE setAbsolute NOTIFY absoluteChanged);

private:
  vtkSmartPointer<vtkWarpTo> m_object = nullptr;
  xVisVector3 *m_position = nullptr;

public:
  xVisWarpTo();
  auto getPosition() -> xVisVector3 *;
  auto setScaleFactor(qreal) -> void;
  auto getScaleFactor() -> qreal;
  auto setAbsolute(bool) -> void;
  auto getAbsolute() -> bool;
signals:
  void scaleFactorChanged();
  void absoluteChanged();
};
