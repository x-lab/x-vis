#include "xVisRibbonFilter.hpp"

xVisRibbonFilter::xVisRibbonFilter() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkRibbonFilter>::New()) {
    this->m_object = vtkRibbonFilter::SafeDownCast(xVisAlgorithm::get());

    this->m_defaultNormal = new xVisVector3([this](){
        this->m_object->SetDefaultNormal(this->m_defaultNormal->getValues().data());
        this->update();
    });
}

auto xVisRibbonFilter::setVaryWidth(bool varyWidth) -> void {
    this->m_object->SetVaryWidth(varyWidth);
    this->update();
    emit this->varyWidthChanged();
}

auto xVisRibbonFilter::getVaryWidth() -> bool {
    return this->m_object->GetVaryWidth();
}

auto xVisRibbonFilter::setGenerateTCoords(TCoords tCoords) -> void {
    this->m_object->SetGenerateTCoords(tCoords);
    this->update();
    emit this->generateTCoordsChanged();
}

auto xVisRibbonFilter::getGenerateTCoords() -> TCoords {
    return static_cast<TCoords>(this->m_object->GetGenerateTCoords());
}

auto xVisRibbonFilter::setWidth(qreal width) -> void {
    this->m_object->SetWidth(width);
    this->update();
    emit this->widthChanged();
}

auto xVisRibbonFilter::getWidth() -> qreal {
    return this->m_object->GetWidth();
}

auto xVisRibbonFilter::setTextureLength(qreal textureLength) -> void {
    this->m_object->SetTextureLength(textureLength);
    this->update();
    emit this->textureLengthChanged();
}

auto xVisRibbonFilter::getTextureLength() -> qreal {
    return this->m_object->GetTextureLength();
}

auto xVisRibbonFilter::setWidthFactor(qreal widthFactor) -> void {
    this->m_object->SetWidthFactor(widthFactor);
    this->update();
    emit this->widthFactorChanged();
}

auto xVisRibbonFilter::getWidthFactor() -> qreal {
    return this->m_object->GetWidthFactor();
}

auto xVisRibbonFilter::setUseDefaultNormal(bool useDefaultNormal) -> void {
    this->m_object->SetUseDefaultNormal(useDefaultNormal);
    this->update();
    emit this->useDefaultNormalChanged();
}

auto xVisRibbonFilter::getUseDefaultNormal() -> bool {
    return this->m_object->GetUseDefaultNormal();
}

auto xVisRibbonFilter::getDefaultNormal() -> xVisVector3* {
    return this->m_defaultNormal;
}
