#pragma once

#include "xVisPointSetAlgorithm.hpp"
#include "xVisVector2.hpp"

#include <vtkWarpLens.h>

class xVisWarpLens : public xVisPointSetAlgorithm {
    Q_OBJECT
    Q_PROPERTY(xVisVector2* center READ getCenter CONSTANT);
    Q_PROPERTY(xVisVector2* principalPoint READ getPrincipalPoint CONSTANT);
    Q_PROPERTY(qreal k1 READ getK1 WRITE setK1 NOTIFY k1Changed);
    Q_PROPERTY(qreal k2 READ getK2 WRITE setK2 NOTIFY k2Changed);
    Q_PROPERTY(qreal p1 READ getP1 WRITE setP1 NOTIFY p1Changed);
    Q_PROPERTY(qreal p2 READ getP2 WRITE setP2 NOTIFY p2Changed);
private:
    vtkSmartPointer<vtkWarpLens> m_object = nullptr;
    xVisVector2* m_center = nullptr;
    xVisVector2* m_principalPoint = nullptr;
public:
    xVisWarpLens();
    auto getCenter() -> xVisVector2*;
    auto getPrincipalPoint() -> xVisVector2*;
    auto setK1(qreal) -> void;
    auto getK1() -> qreal;
    auto setK2(qreal) -> void;
    auto getK2() -> qreal;
    auto setP1(qreal) -> void;
    auto getP1() -> qreal;
    auto setP2(qreal) -> void;
    auto getP2() -> qreal;
signals:
    void k1Changed();
    void k2Changed();
    void p1Changed();
    void p2Changed();
};
