#include "xVisDataReader.hpp"

xVisDataReader::xVisDataReader(vtkSmartPointer<vtkDataReader> vtkObject) : xVisAlgorithm(vtkObject) {
    this->m_object = vtkObject;
}

auto xVisDataReader::setFileName(const QString& fileName) -> void {
    this->m_object->SetFileName(fileName.toStdString().c_str());
    this->m_object->Update();
    this->update();

    emit this->fileNameChanged();
}

auto xVisDataReader::getFileName() -> QString {
    return this->m_object->GetFileName();
}
