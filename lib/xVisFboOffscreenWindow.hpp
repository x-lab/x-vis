#pragma once

#include <QOpenGLFunctions>
#include <QOpenGLFramebufferObject>

#include <vtkExternalOpenGLRenderWindow.h>

#include <QVTKRenderWindowAdapter.h>

class xVisFboRenderer;

class xVisFboOffscreenWindow : public QObject, public vtkExternalOpenGLRenderWindow, protected QOpenGLFunctions {

    Q_OBJECT

public:
    xVisFboRenderer* QtParentRenderer = nullptr;
    static auto New() -> xVisFboOffscreenWindow*;
    virtual auto OpenGLInitState() -> void override;
    auto Render() -> void override;
    auto InternalRender() -> void;
    auto SetFramebufferObject(QOpenGLFramebufferObject*) -> void;
    auto Resize(int width, int height) -> void;

public:
    static void setupGraphicsBackend(void);

protected:
     xVisFboOffscreenWindow();
    ~xVisFboOffscreenWindow();

private:
    QOpenGLFramebufferObject *m_fbo = 0;
private:
    QVTKRenderWindowAdapter *m_adapter = 0;
private:
    bool m_initialised = false;
};
