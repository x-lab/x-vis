#include "xVisGlyph3D.hpp"

xVisGlyph3D::xVisGlyph3D() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkGlyph3D>::New()) {
    this->m_object = vtkGlyph3D::SafeDownCast(xVisAlgorithm::get());

    this->m_range = new xVisVector2([this](){
        this->m_object->SetRange(this->m_range->getX(), this->m_range->getY());
        this->update();
    });
}

auto xVisGlyph3D::getRange() -> xVisVector2* {
    return this->m_range;
}

auto xVisGlyph3D::setScaleMode(ScaleMode scaleMode) -> void {
    this->m_object->SetScaleMode(scaleMode);
    emit this->scaleModeChanged();
    this->update();
}

auto xVisGlyph3D::getScaleMode() -> xVisGlyph3D::ScaleMode {
    return (xVisGlyph3D::ScaleMode) this->m_object->GetScaleMode();
}

auto xVisGlyph3D::setColorMode(ColorMode colorMode) -> void {
    this->m_object->SetColorMode(colorMode);
    emit this->colorModeChanged();
    this->update();
}

auto xVisGlyph3D::getColorMode() -> ColorMode {
    return static_cast<ColorMode>(this->m_object->GetColorMode());
}

auto xVisGlyph3D::setIndexMode(IndexMode indexMode) -> void {
    this->m_object->SetIndexMode(indexMode);
    emit this->indexModeChanged();
    this->update();
}

auto xVisGlyph3D::getIndexMode() -> IndexMode {
    return static_cast<IndexMode>(this->m_object->GetIndexMode());
}

auto xVisGlyph3D::setVectorMode(VectorMode vectorMode) -> void {
    this->m_object->SetVectorMode(vectorMode);
    emit this->vectorModeChanged();
    this->update();
}

auto xVisGlyph3D::getVectorMode() -> VectorMode {
    return static_cast<VectorMode>(this->m_object->GetVectorMode());
}

auto xVisGlyph3D::setScaleFactor(qreal scaleFactor) -> void {
    this->m_object->SetScaleFactor(scaleFactor);
    emit this->scaleFactorChanged();
    this->update();
}

auto xVisGlyph3D::getScaleFactor() -> qreal {
    return this->m_object->GetScaleFactor();
}

auto xVisGlyph3D::setOrient(bool orient) -> void {
    this->m_object->SetOrient(orient);
    emit this->orientChanged();
    this->update();
}

auto xVisGlyph3D::getOrient() -> bool {
    return this->m_object->GetOrient();
}

auto xVisGlyph3D::setClamping(bool clamping) -> void {
    this->m_object->SetClamping(clamping);
    emit this->clampingChanged();
    this->update();
}

auto xVisGlyph3D::getClamping() -> bool {
    return this->m_object->GetClamping();
}

auto xVisGlyph3D::setScaling(bool scaling) -> void {
    this->m_object->SetScaling(scaling);
    emit this->scalingChanged();
    this->update();
}

auto xVisGlyph3D::getScaling() -> bool {
    return this->m_object->GetScaling();
}

auto xVisGlyph3D::setFillCellData(bool fillCellData) -> void {
    this->m_object->SetFillCellData(fillCellData);
    emit this->fillCellDataChanged();
    this->update();
}

auto xVisGlyph3D::getFillCellData() -> bool {
    return this->m_object->GetFillCellData();
}

auto xVisGlyph3D::setGeneratePointIds(bool generatePointIds) -> void {
    this->m_object->SetGeneratePointIds(generatePointIds);
    emit this->generatePointIdsChanged();
    this->update();
}

auto xVisGlyph3D::getGeneratePointIds() -> bool {
    return this->m_object->GetGeneratePointIds();
}
