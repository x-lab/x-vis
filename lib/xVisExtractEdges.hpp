#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkExtractEdges.h>

class xVisExtractEdges : public xVisPolyDataAlgorithm
{
    Q_OBJECT

public:
    xVisExtractEdges();

private:
    vtkSmartPointer<vtkExtractEdges> m_object = nullptr;
};
