#pragma once

#include "xVisImplicitFunction.hpp"

#include <vtkCone.h>

class xVisCone : public xVisImplicitFunction
{
    Q_OBJECT
    Q_PROPERTY(qreal angle READ getAngle WRITE setAngle NOTIFY angleChanged);

public:
    xVisCone();
    auto setAngle(qreal) -> void;
    auto getAngle() -> qreal;

signals:
    void angleChanged();

private:
    vtkSmartPointer<vtkCone> m_object = nullptr;
};
