#pragma once

#include <vtkAxesActor.h>
#include <vtkCallbackCommand.h>
#include <vtkGenericRenderWindowInteractor.h>
#include <vtkNew.h>

class xVisGenericInteractor : public vtkGenericRenderWindowInteractor
{
public:
    vtkTypeMacro(xVisGenericInteractor, vtkGenericRenderWindowInteractor);
    static xVisGenericInteractor *New(void);
    void Initialize(void) override;

    vtkNew<vtkCallbackCommand> callbackCommand;
    vtkNew<vtkAxesActor> m_axes_actor;
    bool m_axes_displayed = false;

protected:
             xVisGenericInteractor(void) = default;
    virtual ~xVisGenericInteractor(void) = default;
    static void axisActorToggle(vtkObject* object, unsigned long event, void* clientdata, void* calldata);

};
