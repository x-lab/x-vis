#pragma once

#include <QtCore>

#include "xVisAbstractVolumeMapper.hpp"
#include "xVisProp3D.hpp"
#include "xVisVolumeProperty.hpp"

#include <vtkVolume.h>

class xVisVolume : public xVisProp3D {
  Q_OBJECT
  Q_PROPERTY(xVisAbstractVolumeMapper *mapper READ getMapper WRITE setMapper
                 NOTIFY mapperChanged);
  Q_PROPERTY(xVisVolumeProperty *property READ getProperty CONSTANT);
  Q_CLASSINFO("DefaultProperty", "mapper");

private:
  xVisVolumeProperty *m_property = nullptr;
  xVisAbstractVolumeMapper *m_mapper = nullptr;
  vtkSmartPointer<vtkVolume> m_volume = nullptr;

public:
  xVisVolume();
  ~xVisVolume();
  auto setMapper(xVisAbstractVolumeMapper *) -> void;
  auto getMapper() -> xVisAbstractVolumeMapper *;
  auto getProperty() -> xVisVolumeProperty *;
  auto get() -> vtkSmartPointer<vtkVolume>;
signals:
  void mapperChanged();
};
