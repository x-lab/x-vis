#pragma once

#include "xVisPointSetAlgorithm.hpp"

#include <vtkWarpScalar.h>

class xVisWarpScalar : public xVisPointSetAlgorithm {
    Q_OBJECT
    Q_PROPERTY(qreal scaleFactor READ getScaleFactor WRITE setScaleFactor NOTIFY scaleFactorChanged);
    Q_PROPERTY(bool useNormal READ getUseNormal WRITE setUseNormal NOTIFY useNormalChanged);
private:
    vtkSmartPointer<vtkWarpScalar> m_object = nullptr;
public:
    xVisWarpScalar();
    auto setScaleFactor(qreal) -> void;
    auto getScaleFactor() -> qreal;
    auto setUseNormal(bool) -> void;
    auto getUseNormal() -> bool;
signals:
    void scaleFactorChanged();
    void useNormalChanged();
};
