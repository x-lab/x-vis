#include "xVisVolume.hpp"
#include "xVisAbstractVolumeMapper.hpp"
#include "xVisVolumeProperty.hpp"

xVisVolume::xVisVolume() : xVisProp3D(vtkSmartPointer<vtkVolume>::New()) {
    this->m_volume = vtkVolume::SafeDownCast(xVisProp::get());
    this->m_property = new xVisVolumeProperty(this);
}

xVisVolume::~xVisVolume() {
    delete this->m_property;
}

auto xVisVolume::get() -> vtkSmartPointer<vtkVolume> {
    return this->m_volume;
}

auto xVisVolume::setMapper(xVisAbstractVolumeMapper* mapper) -> void {
    this->m_mapper = mapper;
    this->m_volume->SetMapper(mapper->get());
    mapper->setProp(this);

    emit this->mapperChanged();

    this->update();
}

auto xVisVolume::getMapper() -> xVisAbstractVolumeMapper* {
    return this->m_mapper;
}

auto xVisVolume::getProperty() -> xVisVolumeProperty* {
    return this->m_property;
}
