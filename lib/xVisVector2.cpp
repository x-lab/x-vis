#include "xVisVector2.hpp"

xVisVector2::xVisVector2(cb_t&& callback, array_t values) : m_callback(callback), m_values(values)
{

}

void xVisVector2::notify(void)
{
    this->m_callback.operator()();
}

double xVisVector2::getX(void)
{
    return this->m_values[0];
}

double xVisVector2::getY(void)
{
    return this->m_values[1];
}

void xVisVector2::setX(double x)
{
    this->m_values[0] = x;
    emit this->xChanged();
    this->notify();
}

void xVisVector2::setY(double y)
{
    this->m_values[1] = y;
    emit this->yChanged();
    this->notify();
}

xVisVector2::array_t xVisVector2::getValues(void)
{
    return this->m_values;
}
