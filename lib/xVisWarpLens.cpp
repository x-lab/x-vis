#include "xVisWarpLens.hpp"

xVisWarpLens::xVisWarpLens() : xVisPointSetAlgorithm(vtkSmartPointer<vtkWarpLens>::New()) {
    this->m_object = vtkWarpLens::SafeDownCast(xVisAlgorithm::get());

    this->m_center = new xVisVector2([this](){
        this->m_object->SetCenter(this->m_center->getX(), this->m_center->getY());
        this->update();
    });

    this->m_principalPoint = new xVisVector2([this](){
        this->m_object->SetPrincipalPoint(this->m_principalPoint->getX(), this->m_principalPoint->getY());
        this->update();
    });
}

auto xVisWarpLens::getCenter() -> xVisVector2* {
    return this->m_center;
}

auto xVisWarpLens::getPrincipalPoint() -> xVisVector2* {
    return this->m_principalPoint;
}

auto xVisWarpLens::setK1(qreal k1) -> void {
    this->m_object->SetK1(k1);
    emit this->k1Changed();
    this->update();
}

auto xVisWarpLens::getK1() -> qreal {
    return this->m_object->GetK1();
}

auto xVisWarpLens::setK2(qreal k2) -> void {
    this->m_object->SetK2(k2);
    emit this->k2Changed();
    this->update();
}

auto xVisWarpLens::getK2() -> qreal {
    return this->m_object->GetK2();
}

auto xVisWarpLens::setP1(qreal p1) -> void {
    this->m_object->SetP1(p1);
    emit this->p1Changed();
    this->update();
}

auto xVisWarpLens::getP1() -> qreal {
    return this->m_object->GetP1();
}

auto xVisWarpLens::setP2(qreal p2) -> void {
    this->m_object->SetP2(p2);
    emit this->p2Changed();
    this->update();
}

auto xVisWarpLens::getP2() -> qreal {
    return this->m_object->GetP2();
}
