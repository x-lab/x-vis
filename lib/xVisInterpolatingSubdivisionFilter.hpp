#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkInterpolatingSubdivisionFilter.h>

class xVisInterpolatingSubdivisionFilter : public xVisPolyDataAlgorithm {
    Q_OBJECT
    Q_PROPERTY(int numberOfSubdivisions READ getNumberOfSubdivisions WRITE setNumberOfSubdivisions NOTIFY numberOfSubdivisionsChanged);
private:
    vtkSmartPointer<vtkInterpolatingSubdivisionFilter> m_object = nullptr;
public:
    xVisInterpolatingSubdivisionFilter(vtkSmartPointer<vtkInterpolatingSubdivisionFilter>);
    auto setNumberOfSubdivisions(int) -> void;
    auto getNumberOfSubdivisions() -> int;
signals:
    void numberOfSubdivisionsChanged();
};
