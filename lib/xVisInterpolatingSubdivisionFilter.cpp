#include "xVisInterpolatingSubdivisionFilter.hpp"

xVisInterpolatingSubdivisionFilter::xVisInterpolatingSubdivisionFilter(vtkSmartPointer<vtkInterpolatingSubdivisionFilter> vtkObject) : xVisPolyDataAlgorithm(vtkObject) {
    this->m_object = vtkObject;
}

auto xVisInterpolatingSubdivisionFilter::setNumberOfSubdivisions(int value) -> void {
    this->m_object->SetNumberOfSubdivisions(value);
    emit this->numberOfSubdivisionsChanged();
    this->update();
}

auto xVisInterpolatingSubdivisionFilter::getNumberOfSubdivisions() -> int {
    return this->m_object->GetNumberOfSubdivisions();
}
