#pragma once

#include "xVisMapper2D.hpp"
#include "xVisProp.hpp"
#include "xVisProperty2D.hpp"

#include <vtkSmartPointer.h>
#include <vtkActor2D.h>

class xVisActor2D : public xVisProp
{
    Q_OBJECT
    Q_PROPERTY(xVisProperty2D* property READ getProperty CONSTANT);
    Q_PROPERTY(xVisMapper2D* mapper READ getMapper WRITE setMapper NOTIFY mapperChanged);
    Q_PROPERTY(qreal width READ getWidth WRITE setWidth NOTIFY widthChanged);
    Q_PROPERTY(qreal height READ getHeight WRITE setHeight NOTIFY heightChanged);
    Q_CLASSINFO("DefaultProperty", "mapper");

public:
             xVisActor2D(void);
             xVisActor2D(vtkSmartPointer<vtkActor2D>);
    virtual ~xVisActor2D(void);

    vtkSmartPointer<vtkActor2D> get(void);
    xVisProperty2D *getProperty(void);
    void setMapper(xVisMapper2D *);
    xVisMapper2D *getMapper(void);
    void setWidth(qreal);
    qreal getWidth(void);
    void setHeight(qreal);
    qreal getHeight(void);

signals:
    void widthChanged(void);
    void heightChanged(void);
    void mapperChanged(void);

private:
    vtkSmartPointer<vtkActor2D> m_object = nullptr;

private:
    xVisMapper2D* m_mapper = nullptr;
    xVisProperty2D* m_property = nullptr;
};
