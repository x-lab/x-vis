#include "xVisCylinderSource.hpp"

xVisCylinderSource::xVisCylinderSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkCylinderSource>::New())
{
    this->m_object = vtkCylinderSource::SafeDownCast(xVisAlgorithm::get());

    this->m_center = new xVisVector3([this](){
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });
}

auto xVisCylinderSource::getCenter() -> xVisVector3* {
    return this->m_center;
}

auto xVisCylinderSource::setHeight(qreal height) -> void {
    this->m_object->SetHeight(height);
    emit this->heightChanged();
    this->update();
}

auto xVisCylinderSource::getHeight() -> qreal {
    return this->m_object->GetHeight();
}

auto xVisCylinderSource::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    emit this->radiusChanged();
    this->update();
}

auto xVisCylinderSource::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

auto xVisCylinderSource::setResolution(int resolution) -> void {
    this->m_object->SetResolution(resolution);
    emit this->resolutionChanged();
    this->update();
}

auto xVisCylinderSource::getResolution() -> int {
    return this->m_object->GetResolution();
}

auto xVisCylinderSource::setCapping(bool capping) -> void {
    this->m_object->SetCapping(capping);
    emit this->cappingChanged();
    this->update();
}

auto xVisCylinderSource::getCapping() -> bool {
    return this->m_object->GetCapping();
}
