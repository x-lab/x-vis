#include "xVisAbstractWidget.hpp"

// Qml::Register::AbstractClass<xVisAbstractWidget> xVisAbstractWidget::Register(true);

xVisAbstractWidget::xVisAbstractWidget(vtkSmartPointer<vtkAbstractWidget> vtkObject) : xVisObject(xVisObject::Type::Widget), m_object(vtkObject)
{

}

vtkSmartPointer<vtkAbstractWidget> xVisAbstractWidget::get(void)
{
    return this->m_object;
}

void xVisAbstractWidget::setEnabled(bool enabled)
{
    this->m_object->SetEnabled(enabled);
    emit this->enabledChanged();
}

bool xVisAbstractWidget::getEnabled(void)
{
    return this->m_object->GetEnabled();
}
