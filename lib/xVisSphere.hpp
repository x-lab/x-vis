#pragma once

#include "xVisVector3.hpp"
#include "xVisImplicitFunction.hpp"

#include <vtkSphere.h>

class xVisSphere : public xVisImplicitFunction {
    Q_OBJECT
    Q_PROPERTY(qreal radius READ getRadius WRITE setRadius NOTIFY radiusChanged);
    Q_PROPERTY(xVisVector3* center READ getCenter CONSTANT);
private:
    vtkSmartPointer<vtkSphere> m_object = nullptr;
    xVisVector3* m_center = nullptr;
public:
    xVisSphere();
    auto setRadius(qreal) -> void;
    auto getRadius() -> qreal;
    auto setCenter(xVisVector3*) -> void;
    auto getCenter() -> xVisVector3*;
public slots:
    qreal evaluateFunction(qreal, qreal, qreal);
signals:
    void radiusChanged();
};
