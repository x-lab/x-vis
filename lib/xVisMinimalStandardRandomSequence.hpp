#pragma once

#include "xVisRandomSequence.hpp"

#include <vtkMinimalStandardRandomSequence.h>

class xVisMinimalStandardRandomSequence : public xVisRandomSequence {
    Q_OBJECT
private:
    vtkSmartPointer<vtkMinimalStandardRandomSequence> m_object = nullptr;
public:
    xVisMinimalStandardRandomSequence();
};
