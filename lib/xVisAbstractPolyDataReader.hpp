#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkAbstractPolyDataReader.h>

class xVisAbstractPolyDataReader : public xVisPolyDataAlgorithm {
    Q_OBJECT
    Q_PROPERTY(QString fileName READ getFileName WRITE setFileName NOTIFY fileNameChanged);
private:
    vtkSmartPointer<vtkAbstractPolyDataReader> m_vtkObject = nullptr;
    QString m_fileName;
public:
    xVisAbstractPolyDataReader(vtkSmartPointer<vtkAbstractPolyDataReader>);
    auto isValid() -> bool override;
    auto setFileName(const QString&) -> void;
    auto getFileName() -> QString;
signals:
    void fileNameChanged();
};
