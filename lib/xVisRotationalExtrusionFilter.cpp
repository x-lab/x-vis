#include "xVisRotationalExtrusionFilter.hpp"

xVisRotationalExtrusionFilter::xVisRotationalExtrusionFilter() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkRotationalExtrusionFilter>::New()) {
    this->m_object = vtkRotationalExtrusionFilter::SafeDownCast(xVisAlgorithm::get());
}

auto xVisRotationalExtrusionFilter::setResolution(int resolution) -> void {
    this->m_object->SetResolution(resolution);
    emit this->resolutionChanged();
    this->update();
}

auto xVisRotationalExtrusionFilter::getResolution() -> int {
    return this->m_object->GetResolution();
}

auto xVisRotationalExtrusionFilter::setCapping(bool capping) -> void {
    this->m_object->SetCapping(capping);
    emit this->cappingChanged();
    this->update();
}

auto xVisRotationalExtrusionFilter::getCapping() -> bool {
    return this->m_object->GetCapping();
}

auto xVisRotationalExtrusionFilter::setAngle(qreal angle) -> void {
    this->m_object->SetAngle(angle);
    emit this->angleChanged();
    this->update();
}

auto xVisRotationalExtrusionFilter::getAngle() -> qreal {
    return this->m_object->GetAngle();
}

auto xVisRotationalExtrusionFilter::setTranslation(qreal translation) -> void {
    this->m_object->SetTranslation(translation);
    emit this->translationChanged();
    this->update();
}

auto xVisRotationalExtrusionFilter::getTranslation() -> qreal {
    return this->m_object->GetTranslation();
}

auto xVisRotationalExtrusionFilter::setDeltaRadius(qreal deltaRadius) -> void {
    this->m_object->SetDeltaRadius(deltaRadius);
    emit this->deltaRadiusChanged();
    this->update();
}

auto xVisRotationalExtrusionFilter::getDeltaRadius() -> qreal {
    return this->m_object->GetDeltaRadius();
}
