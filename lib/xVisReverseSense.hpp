#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkReverseSense.h>

class xVisReverseSense : public xVisPolyDataAlgorithm {
    Q_OBJECT
    Q_PROPERTY(bool reverseCells READ getReverseCells WRITE setReverseCells NOTIFY reverseCellsChanged);
    Q_PROPERTY(bool reverseNormals READ getReverseNormals WRITE setReverseNormals NOTIFY reverseNormalsChanged);
private:
    vtkSmartPointer<vtkReverseSense> m_object = nullptr;
public:
    xVisReverseSense();
    auto setReverseCells(bool) -> void;
    auto getReverseCells() -> bool;
    auto setReverseNormals(bool) -> void;
    auto getReverseNormals() -> bool;
signals:
    void reverseCellsChanged();
    void reverseNormalsChanged();
};
