#include "xVisTexture.hpp"

xVisTexture::xVisTexture(vtkSmartPointer<vtkTexture> vtkObject, cb_t&& callback) : xVisImageAlgorithm(vtkObject), m_object(vtkObject), m_callback(callback) {
}

auto xVisTexture::setQuality(Quality quality) -> void {
    this->m_object->SetQuality(quality);
    emit this->qualityChanged();
    this->update();
}

auto xVisTexture::getQuality() -> Quality {
    return static_cast<Quality>(this->m_object->GetQuality());
}

auto xVisTexture::setBlendingMode(BlendingMode blendingMode) -> void {
    this->m_object->SetBlendingMode(blendingMode);
    emit this->blendingModeChanged();
    this->update();
}

auto xVisTexture::getBlendingMode() -> BlendingMode {
    return static_cast<BlendingMode>(this->m_object->GetBlendingMode());
}

auto xVisTexture::setRepeat(bool repeat) -> void {
    this->m_object->SetRepeat(repeat);
    emit this->repeatChanged();
    this->update();
}

auto xVisTexture::getRepeat() -> bool {
    return this->m_object->GetRepeat();
}

auto xVisTexture::setEdgeClamp(bool edgeClamp) -> void {
    this->m_object->SetEdgeClamp(edgeClamp);
    emit this->edgeClampChanged();
    this->update();
}

auto xVisTexture::getEdgeClamp() -> bool {
    return this->m_object->GetEdgeClamp();
}

auto xVisTexture::setInterpolate(bool interpolate) -> void {
    this->m_object->SetInterpolate(interpolate);
    emit this->interpolateChanged();
    this->update();
}

auto xVisTexture::getInterpolate() -> bool {
    return this->m_object->GetInterpolate();
}
