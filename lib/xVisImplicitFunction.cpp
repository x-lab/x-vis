#include "xVisImplicitFunction.hpp"

xVisImplicitFunction::xVisImplicitFunction(vtkSmartPointer<vtkImplicitFunction> vtkObject) : xVisObject(xVisObject::Type::Other), m_object(vtkObject) {

}

xVisImplicitFunction::~xVisImplicitFunction() {
}

auto xVisImplicitFunction::update() -> void {
    for (auto callback : this->m_callbacks) {
        callback->operator()();
    }
}

auto xVisImplicitFunction::addCallback(cb_t&& callback) -> void {
    if (this->m_callbacks.contains(&callback)) {
        return;
    }

    this->m_callbacks.append(&callback);
}

auto xVisImplicitFunction::removeCallback(cb_t&& callback) -> void {
    this->m_callbacks.removeOne(&callback);
}

auto xVisImplicitFunction::get() -> vtkSmartPointer<vtkImplicitFunction> {
    return this->m_object;
}
