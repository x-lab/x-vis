#include "xVisDensifyPolyData.hpp"

xVisDensifyPolyData::xVisDensifyPolyData() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkDensifyPolyData>::New()) {
    this->m_object = vtkDensifyPolyData::SafeDownCast(xVisAlgorithm::get());
}

auto xVisDensifyPolyData::setNumberOfSubdivisions(int numberOfSubdivisions) -> void {
    this->m_object->SetNumberOfSubdivisions(numberOfSubdivisions);
    this->update();
    emit this->numberOfSubdivisionsChanged();
}

auto xVisDensifyPolyData::getNumberOfSubdivisions() -> int {
    return this->m_object->GetNumberOfSubdivisions();
}
