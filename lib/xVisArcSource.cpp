#include "xVisArcSource.hpp"

xVisArcSource::xVisArcSource(void) : xVisPolyDataAlgorithm(vtkSmartPointer<vtkArcSource>::New())
{
    this->m_object = vtkArcSource::SafeDownCast(xVisAlgorithm::get());

    this->m_center = new xVisVector3([this]() {
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });

    this->m_point1 = new xVisVector3([this]() {
        this->m_object->SetPoint1(this->m_point1->getValues().data());
        this->update();
    });

    this->m_point2 = new xVisVector3([this]() {
        this->m_object->SetPoint2(this->m_point2->getValues().data());
        this->update();
    });

    this->m_center = new xVisVector3([this]() {
        this->m_object->SetCenter(this->m_center->getValues().data());
    });

    this->m_polarVector = new xVisVector3([this]() {
        this->m_object->SetPolarVector(this->m_polarVector->getValues().data());
    });
}

auto xVisArcSource::getPoint1() -> xVisVector3* {
    return this->m_point1;
}

auto xVisArcSource::getPoint2() -> xVisVector3* {
    return this->m_point2;
}

auto xVisArcSource::getCenter() -> xVisVector3* {
    return this->m_center;
}

auto xVisArcSource::getPolarVector() -> xVisVector3* {
    return this->m_polarVector;
}

auto xVisArcSource::setAngle(qreal angle) -> void {
    this->m_object->SetAngle(angle);
    emit this->angleChanged();
    this->update();
}

auto xVisArcSource::getAngle() -> qreal {
    return this->m_object->GetAngle();
}

auto xVisArcSource::setResolution(int resolution) -> void {
    this->m_object->SetResolution(resolution);
    emit this->resolutionChanged();
    this->update();
}

auto xVisArcSource::getResolution() -> int {
    return this->m_object->GetResolution();
}

auto xVisArcSource::setNegative(bool negative) -> void {
    this->m_object->SetNegative(negative);
    emit this->negativeChanged();
    this->update();
}

auto xVisArcSource::getNegative() -> bool {
    return this->m_object->GetNegative();
}

auto xVisArcSource::setUseNormalAndAngle(bool useNormalAndAngle) -> void {
    this->m_object->SetUseNormalAndAngle(useNormalAndAngle);
    emit this->useNormalAndAngleChanged();
    this->update();
}

auto xVisArcSource::getUseNormalAndAngle() -> bool {
    return this->m_object->GetUseNormalAndAngle();
}
