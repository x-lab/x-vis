#include "xVisEllipseArcSource.hpp"

xVisEllipseArcSource::xVisEllipseArcSource() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkEllipseArcSource>::New())
{
    this->m_object = vtkEllipseArcSource::SafeDownCast(xVisAlgorithm::get());

    this->m_center = new xVisVector3([this]() {
        this->m_object->SetCenter(this->m_center->getValues().data());
        this->update();
    });

    this->m_normal = new xVisVector3([this]() {
        this->m_object->SetNormal(this->m_normal->getValues().data());
        this->update();
    });

    this->m_majorRadiusVector = new xVisVector3([this]() {
        this->m_object->SetMajorRadiusVector(this->m_majorRadiusVector->getValues().data());
        this->update();
    });
}

auto xVisEllipseArcSource::getCenter() -> xVisVector3* {
    return this->m_center;
}

auto xVisEllipseArcSource::getNormal() -> xVisVector3* {
    return this->m_normal;
}

auto xVisEllipseArcSource::getMajorRadiusVector() -> xVisVector3* {
    return this->m_majorRadiusVector;
}

auto xVisEllipseArcSource::setStartAngle(qreal startAngle) -> void {
    this->m_object->SetStartAngle(startAngle);
    emit this->startAngleChanged();
    this->update();
}

auto xVisEllipseArcSource::getStartAngle() -> qreal {
    return this->m_object->GetStartAngle();
}

auto xVisEllipseArcSource::setSegmentAngle(qreal segmentAngle) -> void {
    this->m_object->SetSegmentAngle(segmentAngle);
    emit this->segmentAngleChanged();
    this->update();
}

auto xVisEllipseArcSource::getSegmentAngle() -> qreal {
    return this->m_object->GetSegmentAngle();
}

auto xVisEllipseArcSource::setResolution(int resolution) -> void {
    this->m_object->SetResolution(resolution);
    emit this->resolutionChanged();
    this->update();
}

auto xVisEllipseArcSource::getResolution() -> int {
    return this->m_object->GetResolution();
}

auto xVisEllipseArcSource::setRatio(qreal ratio) -> void {
    this->m_object->SetRatio(ratio);
    emit this->ratioChanged();
    this->update();
}

auto xVisEllipseArcSource::getRatio() -> qreal {
    return this->m_object->GetRatio();
}
