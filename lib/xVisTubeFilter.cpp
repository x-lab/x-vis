#include "xVisTubeFilter.hpp"

xVisTubeFilter::xVisTubeFilter() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkTubeFilter>::New()) {
    this->m_object = vtkTubeFilter::SafeDownCast(xVisAlgorithm::get());

    this->m_defaultNormal = new xVisVector3([this](){
        this->m_object->SetDefaultNormal(this->m_defaultNormal->getValues().data());
        this->update();
    });
}

auto xVisTubeFilter::setVaryRadius(VaryRadius varyRadius) -> void {
    this->m_object->SetVaryRadius(varyRadius);
    this->update();
    emit this->varyRadiusChanged();
}

auto xVisTubeFilter::getVaryRadius() -> VaryRadius {
    return static_cast<VaryRadius>(this->m_object->GetVaryRadius());
}

auto xVisTubeFilter::setGenerateTCoords(TCoords tCoords) -> void {
    this->m_object->SetGenerateTCoords(tCoords);
    this->update();
    emit this->generateTCoordsChanged();
}

auto xVisTubeFilter::getGenerateTCoords() -> TCoords {
    return static_cast<TCoords>(this->m_object->GetGenerateTCoords());
}

auto xVisTubeFilter::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    this->update();
    emit this->radiusChanged();
}

auto xVisTubeFilter::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

auto xVisTubeFilter::setTextureLength(qreal textureLength) -> void {
    this->m_object->SetTextureLength(textureLength);
    this->update();
    emit this->textureLengthChanged();
}

auto xVisTubeFilter::getTextureLength() -> qreal {
    return this->m_object->GetTextureLength();
}

auto xVisTubeFilter::setNumberOfSides(int numberOfSides) -> void {
    this->m_object->SetNumberOfSides(numberOfSides);
    this->update();
    emit this->numberOfSidesChanged();
}

auto xVisTubeFilter::getNumberOfSides() -> int {
    return this->m_object->GetNumberOfSides();
}

auto xVisTubeFilter::setRadiusFactor(qreal radiusFactor) -> void {
    this->m_object->SetRadiusFactor(radiusFactor);
    this->update();
    emit this->radiusFactorChanged();
}

auto xVisTubeFilter::getRadiusFactor() -> qreal {
    return this->m_object->GetRadiusFactor();
}

auto xVisTubeFilter::setUseDefaultNormal(bool useDefaultNormal) -> void {
    this->m_object->SetUseDefaultNormal(useDefaultNormal);
    this->update();
    emit this->useDefaultNormalChanged();
}

auto xVisTubeFilter::getUseDefaultNormal() -> bool {
    return this->m_object->GetUseDefaultNormal();
}

auto xVisTubeFilter::setSidesShareVertices(bool sidesShareVertices) -> void {
    this->m_object->SetSidesShareVertices(sidesShareVertices);
    this->update();
    emit this->sidesShareVerticesChanged();
}

auto xVisTubeFilter::getSidesShareVertices() -> bool {
    return this->m_object->GetSidesShareVertices();
}

auto xVisTubeFilter::setCapping(bool capping) -> void {
    this->m_object->SetCapping(capping);
    this->update();
    emit this->cappingChanged();
}

auto xVisTubeFilter::getCapping() -> bool {
    return this->m_object->GetCapping();
}

auto xVisTubeFilter::setOnRatio(int onRatio) -> void {
    this->m_object->SetOnRatio(onRatio);
    this->update();
    emit this->onRatioChanged();
}

auto xVisTubeFilter::getOnRatio() -> int {
    return this->m_object->GetOnRatio();
}

auto xVisTubeFilter::setOffset(int offset) -> void {
    this->m_object->SetOffset(offset);
    this->update();
    emit this->offsetChanged();
}

auto xVisTubeFilter::getOffset() -> int {
    return this->m_object->GetOffset();
}

auto xVisTubeFilter::getDefaultNormal() -> xVisVector3* {
    return this->m_defaultNormal;
}
