#include "xVisDICOMImageReader.hpp"

xVisDICOMImageReader::xVisDICOMImageReader() : xVisImageReader2(vtkSmartPointer<vtkDICOMImageReader>::New()) {
    this->m_object = vtkDICOMImageReader::SafeDownCast(xVisAlgorithm::get());
}

auto xVisDICOMImageReader::setDirectoryName(const QString& directoryName) -> void {
    this->m_directoryName = directoryName;
    emit this->directoryNameChanged();
    this->m_object->SetDirectoryName(directoryName.toStdString().c_str());
    this->m_object->Update();
    this->update();
}

auto xVisDICOMImageReader::getDirectoryName() -> QString {
    return this->m_directoryName;
}
