#pragma once

#include "xVisAbstractMapper.hpp"

#include <vtkMapper2D.h>

class xVisMapper2D : public xVisAbstractMapper {
    Q_OBJECT
public:
    xVisMapper2D(vtkSmartPointer<vtkMapper2D>);
    auto get() -> vtkSmartPointer<vtkMapper2D>;
};
