#include "xVisExtractEdges.hpp"

xVisExtractEdges::xVisExtractEdges() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkExtractEdges>::New())
{
    this->m_object = vtkExtractEdges::SafeDownCast(xVisAlgorithm::get());
}
