#include "xVisPerlinNoise.hpp"

xVisPerlinNoise::xVisPerlinNoise() : xVisImplicitFunction(vtkSmartPointer<vtkPerlinNoise>::New()) {
    this->m_object = vtkPerlinNoise::SafeDownCast(xVisImplicitFunction::get());

    this->m_frequency = new xVisVector3([this](){
        this->m_object->SetFrequency(this->m_frequency->getValues().data());
        this->update();
    });

    this->m_phase = new xVisVector3([this](){
        this->m_object->SetPhase(this->m_phase->getValues().data());
        this->update();
    });
}

auto xVisPerlinNoise::setAmplitude(qreal amplitude) -> void {
    this->m_object->SetAmplitude(amplitude);
    this->update();
}

auto xVisPerlinNoise::getAmplitude() -> qreal {
    return this->m_object->GetAmplitude();
}

auto xVisPerlinNoise::getFrequency() -> xVisVector3* {
    return this->m_frequency;
}

auto xVisPerlinNoise::getPhase() -> xVisVector3* {
    return this->m_phase;
}
