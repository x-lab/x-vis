#include "xVisVolumeMapper.hpp"

xVisVolumeMapper::xVisVolumeMapper(vtkSmartPointer<vtkVolumeMapper> vtkObject) : xVisAbstractVolumeMapper(vtkObject) {
    this->m_object = vtkVolumeMapper::SafeDownCast(vtkObject);
}

auto xVisVolumeMapper::setBlendMode(BlendMode blendMode) -> void {
    this->m_object->SetBlendMode(blendMode);
    emit this->blendModeChanged();
    this->update();
}

auto xVisVolumeMapper::getBlendMode() -> BlendMode {
    return static_cast<BlendMode>(this->m_object->GetBlendMode());
}

auto xVisVolumeMapper::setCropping(bool cropping) -> void {
    this->m_object->SetCropping(cropping);
    emit this->croppingChanged();
    this->update();
}

auto xVisVolumeMapper::getCropping() -> bool {
    return this->m_object->GetCropping();
}
