#pragma once

#include <QImage>

#include <QQuickFramebufferObject>

#include <vtkSmartPointer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkImageData.h>
#include <vtkSmartPointer.h>
#include <vtkWindowToImageFilter.h>

#include <QMouseEvent>

class xVisFboOffscreenWindow;

class xVisFboRenderer : public QQuickFramebufferObject::Renderer {
    friend class xVisFboOffscreenWindow;
private:
    xVisFboOffscreenWindow* m_fboOffscreenWindow = nullptr;
    QOpenGLFramebufferObject* m_fbo = nullptr;
public:
    vtkSmartPointer<vtkRenderWindowInteractor> m_interactor = nullptr;
    vtkSmartPointer<vtkInteractorStyleTrackballCamera> m_interactorStyle;
public:
    xVisFboRenderer(xVisFboOffscreenWindow*);
    auto synchronize(QQuickFramebufferObject*) -> void override;
    auto render() -> void override;
    bool keyPressed(int keycode);
    auto onMouseMoveEvent(QMouseEvent*) -> void;
    auto onMouseEvent(QMouseEvent*) -> void;
    auto onWheelEvent(QWheelEvent*) -> void;
    auto createFramebufferObject(const QSize&) -> QOpenGLFramebufferObject* override;
    ~xVisFboRenderer();
};
