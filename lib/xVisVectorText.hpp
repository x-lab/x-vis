#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkVectorText.h>

class xVisVectorText : public xVisPolyDataAlgorithm {
    Q_OBJECT
    Q_PROPERTY(QString text READ getText WRITE setText NOTIFY textChanged);
private:
    vtkSmartPointer<vtkVectorText> m_object = nullptr;
public:
    xVisVectorText();
    auto setText(const QString&) -> void;
    auto getText() -> QString;
signals:
    void textChanged();
};
