#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkImageDataGeometryFilter.h>

class xVisImageDataGeometryFilter : public xVisPolyDataAlgorithm
{
    Q_OBJECT

private:
    vtkSmartPointer<vtkImageDataGeometryFilter> m_object = nullptr;

public:
    xVisImageDataGeometryFilter();
};
