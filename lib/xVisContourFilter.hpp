#pragma once

#include <QObject>

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkSmartPointer.h>
#include <vtkContourFilter.h>

class xVisContourFilter : public xVisPolyDataAlgorithm
{
    Q_OBJECT

public:
             xVisContourFilter(void);
             xVisContourFilter(vtkSmartPointer<vtkContourFilter>);
    virtual ~xVisContourFilter(void);

public:
    auto get(void) -> vtkSmartPointer<vtkContourFilter>;

public slots:
    void generateValues(qreal v1, qreal v2, qreal v3)
    {
        this->m_object->GenerateValues(v1, v2, v3);
        this->update();
    }

private:
    vtkSmartPointer<vtkContourFilter> m_object = nullptr;
};
