#pragma once

#include <QQmlListProperty>

#include "xVisMapper.hpp"
#include "xVisAlgorithm.hpp"

#include <vtkPolyDataMapper.h>

class xVisPolyDataAlgorithm;

class xVisPolyDataMapper : public xVisMapper {
    Q_OBJECT
public:
    xVisPolyDataMapper();
};
