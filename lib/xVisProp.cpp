#include "xVisProp.hpp"
#include "xVisViewer.hpp"

// Qml::Register::AbstractClass<xVisProp> xVisProp::Register(true);

xVisProp::xVisProp(vtkSmartPointer<vtkProp> vtkObject) : xVisObject(xVisObject::Type::Prop), m_object(vtkObject)
{

}

xVisProp::~xVisProp(void)
{
    this->m_initialized = false;
    this->m_object = nullptr;
}

vtkSmartPointer<vtkProp> xVisProp::get(void)
{
    return this->m_object;
}

void xVisProp::linkViewer(xVisViewer* viewer)
{
    this->m_viewers.append(viewer);
    this->m_initialized = true;

    this->update();
}

void xVisProp::unlinkViewer(xVisViewer* viewer)
{
    this->m_viewers.removeOne(viewer);
}

void xVisProp::update(void)
{
    if (!this->m_initialized)
        return;

    for (auto v : this->m_viewers)
        v->update();
}

void xVisProp::setVisibility(bool visible)
{
    this->m_object->SetVisibility(visible);
    emit this->visibilityChanged();
    update();
}

bool xVisProp::getVisibility(void)
{
    return this->m_object->GetVisibility();
}
