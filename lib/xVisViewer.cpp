#include "xVisViewer.hpp"

#include "xVisProp.hpp"
#include "xVisFboRenderer.hpp"
#include "xVisAbstractWidget.hpp"
#include "xVisFboOffscreenWindow.hpp"

#include <QOpenGLFunctions>
#include <QQuickFramebufferObject>
#include <QOpenGLFramebufferObject>

#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkRendererCollection.h>
#include <vtkObjectFactory.h>
#include <vtkSmartPointer.h>
#include <vtkRenderer.h>

xVisViewer::xVisViewer() {
    this->m_renderer = 0;
    m_win = xVisFboOffscreenWindow::New();
    this->setMirrorVertically(true);
}

auto xVisViewer::init() -> void {

    if(this->m_initialized)
        return;

    auto rw = this->GetRenderWindow();
    this->m_renderer = vtkSmartPointer<vtkRenderer>::New();

    if (!this->m_externalEnabled)
        rw->AddRenderer(m_renderer);

    this->m_initialized = true;

    for (auto object : this->m_input) {
        if (object->getType() == xVisObject::Type::Prop) {
            auto prop = reinterpret_cast<xVisProp*>(object);
            prop->linkViewer(this);
            this->m_renderer->AddActor(prop->get());
        }
        else if (object->getType() == xVisObject::Type::Widget) {
            auto widget = reinterpret_cast<xVisAbstractWidget*>(object);
            auto vtkWidget = widget->get();
            vtkWidget->CreateDefaultRepresentation();
            vtkWidget->SetInteractor(this->GetRenderWindow()->GetInteractor());
            vtkWidget->SetManagesCursor(true);
            vtkWidget->SetPickingManaged(true);
            vtkWidget->On();
        }
    }

    this->update();
}

auto xVisViewer::update() -> void {

    if (!this->m_initialized) {
        return;
    }

    if (this->m_fboRenderer) {
        QQuickFramebufferObject::update();
    }
}

auto xVisViewer::removeData(xVisObject* object) -> void {
}

auto xVisViewer::setHoverEnabled(bool hoverEnabled) -> void {
    this->m_hoverEnabled = hoverEnabled;

    setAcceptHoverEvents(hoverEnabled);

    emit this->hoverEnabledChanged();
}

auto xVisViewer::setMouseEnabled(bool mouseEnabled) -> void {
    this->m_mouseEnabled = mouseEnabled;

    if (mouseEnabled) {
        setAcceptedMouseButtons(Qt::AllButtons);
    }
    else {
        setAcceptedMouseButtons(Qt::NoButton);
    }

    emit this->mouseEnabledChanged();
}

auto xVisViewer::setExternalEnabled(bool enabled) -> void {
    this->m_externalEnabled = enabled;
}

bool xVisViewer::keyPressed(int keycode)
{
    if(this->m_fboRenderer->keyPressed(keycode)) {
        this->forceActiveFocus();
        this->update();
        return true;
    } else {
        return false;
    }
}

auto xVisViewer::mousePressEvent(QMouseEvent *event) -> void {

    this->m_fboRenderer->onMouseEvent(event);

    this->forceActiveFocus();
    this->update();
}

auto xVisViewer::mouseReleaseEvent(QMouseEvent *event) -> void {

    this->m_fboRenderer->onMouseEvent(event);

    this->update();
}

auto xVisViewer::mouseMoveEvent(QMouseEvent *event) -> void {

    this->m_fboRenderer->onMouseMoveEvent(event);

    this->update();
}

auto xVisViewer::hoverMoveEvent(QHoverEvent *event) -> void {

}

auto xVisViewer::wheelEvent(QWheelEvent *event) -> void {

    this->m_fboRenderer->onWheelEvent(event);

    this->update();
}

auto xVisViewer::createRenderer() const -> QQuickFramebufferObject::Renderer* {

    this->m_fboRenderer = new xVisFboRenderer(static_cast<xVisFboOffscreenWindow*>(m_win));

    const_cast<xVisViewer *>(this)->init();
    const_cast<xVisViewer *>(this)->update();

    return this->m_fboRenderer;
}

auto xVisViewer::GetRenderWindow() const -> vtkGenericOpenGLRenderWindow* {
    return m_win;
}
auto xVisViewer::GetRenderer() const -> vtkRenderer* {
    return m_renderer;
}

auto xVisViewer::getInput() -> QQmlListProperty<xVisObject> {
    return QQmlListProperty<xVisObject>(this, 0, &appendInput, &inputCount, &inputAt, &clearInputs);
}

auto xVisViewer::appendInput(QQmlListProperty<xVisObject>* list, xVisObject* object) -> void {

    auto viewer = qobject_cast<xVisViewer*>(list->object);

    if (object && viewer) {
        viewer->m_input.append(object);
    }

    if(viewer && viewer->m_renderer && object) {

        if (object->getType() == xVisObject::Type::Prop) {
            auto prop = reinterpret_cast<xVisProp*>(object);
            viewer->m_renderer->AddActor(prop->get());
        }
        else if (object->getType() == xVisObject::Type::Widget) {
            auto widget = reinterpret_cast<xVisAbstractWidget*>(object);
            auto vtkWidget = widget->get();
            vtkWidget->SetInteractor(viewer->GetRenderWindow()->GetInteractor());
            vtkWidget->CreateDefaultRepresentation();
            vtkWidget->On();
        }

        emit viewer->inputChanged();
        viewer->update();
    }
}

#if QT_VERSION >= 0x060000
auto xVisViewer::inputCount(QQmlListProperty<xVisObject>* list) -> qsizetype {
#else
auto xVisViewer::inputCount(QQmlListProperty<xVisObject>* list) -> int {
#endif
    auto viewer = qobject_cast<xVisViewer*>(list->object);

    if (viewer) {
        return viewer->m_input.count();
    }

    return 0;
}

#if QT_VERSION >= 0x060000
auto xVisViewer::inputAt(QQmlListProperty<xVisObject>* list, qsizetype i) -> xVisObject* {
#else
auto xVisViewer::inputAt(QQmlListProperty<xVisObject>* list, int i) -> xVisObject* {
#endif
    auto viewer = qobject_cast<xVisViewer*>(list->object);

    if (viewer) {
        return viewer->m_input.at(i);
    }

    return 0;
}

auto xVisViewer::clearInputs(QQmlListProperty<xVisObject>*list) -> void {
    auto viewer = qobject_cast<xVisViewer*>(list->object);

    if (viewer) {
        for (auto object : viewer->m_input) {
            if (object->getType() == xVisObject::Type::Prop) {
                auto prop = reinterpret_cast<xVisProp*>(object);
                viewer->m_renderer->RemoveActor(prop->get());
            }
        }

        viewer->m_input.clear();
        emit viewer->inputChanged();
        viewer->update();
    }
}

auto xVisViewer::getHoverEnabled() -> bool {
    return this->m_hoverEnabled;
}

auto xVisViewer::getMouseEnabled() -> bool {
    return this->m_mouseEnabled;
}

xVisViewer::~xVisViewer() {
    m_win->Delete();
}
