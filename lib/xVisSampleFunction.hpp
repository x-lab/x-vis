#pragma once

#include <QObject>

#include "xVisImageAlgorithm.hpp"
#include "xVisImplicitFunction.hpp"
#include "xVisVector3.hpp"

#include <vtkSmartPointer.h>
#include <vtkSampleFunction.h>

class xVisSampleFunction : public xVisImageAlgorithm
{
    Q_OBJECT
    Q_PROPERTY(xVisVector3* sampleDimension READ getSampleDimension CONSTANT);
    Q_PROPERTY(xVisImplicitFunction *implicitFunction READ getImplicitFunction WRITE setImplicitFunction)

public:
             xVisSampleFunction(void);
             xVisSampleFunction(vtkSmartPointer<vtkSampleFunction>);
    virtual ~xVisSampleFunction(void);

public:
    auto get(void) -> vtkSmartPointer<vtkSampleFunction>;

public:
    auto getSampleDimension()
    {
        return m_sampleDimension;
    }

    auto getImplicitFunction(void)
    {
        return m_implicitFunction;
    }

public:
    void setImplicitFunction(xVisImplicitFunction *function)
    {
        this->m_object->SetImplicitFunction(function->get());
        this->update();
    }

private:
    vtkSmartPointer<vtkSampleFunction> m_object = nullptr;

    xVisImplicitFunction *m_implicitFunction = nullptr;

private:
    xVisVector3* m_sampleDimension = nullptr;

};
