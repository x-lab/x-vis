#pragma once

#include <QImage>

#include <QQuickFramebufferObject>

#include <vtkSmartPointer.h>
#include <vtkActor.h>
#include <vtkProp.h>
#include <vtkImageData.h>

class vtkGenericOpenGLRenderWindow;

class xVisObject;
class xVisFboRenderer;
class xVisFboOffscreenWindow;

class xVisViewer : public QQuickFramebufferObject {
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<xVisObject> input READ getInput NOTIFY inputChanged);
    Q_PROPERTY(bool hoverEnabled READ getHoverEnabled WRITE setHoverEnabled NOTIFY hoverEnabledChanged);
    Q_PROPERTY(bool mouseEnabled READ getMouseEnabled WRITE setMouseEnabled NOTIFY mouseEnabledChanged);
    Q_CLASSINFO("DefaultProperty", "input");

signals:
    void captured(void);
    void captureRetrieved(void);

public:
    Q_INVOKABLE bool keyPressed(int keycode);
private:
    friend class xVisFboRenderer;
    QList<xVisObject*> m_input;
    bool m_initialized = false;
    bool m_hoverEnabled = false;
    bool m_mouseEnabled = false;
    bool m_externalEnabled = false;
    vtkSmartPointer<vtkRenderer> m_renderer;
public:
     xVisViewer();
    ~xVisViewer();
    Renderer *createRenderer() const override;
    vtkGenericOpenGLRenderWindow* GetRenderWindow() const;
    vtkRenderer* GetRenderer() const;
public:
    virtual auto init() -> void;
    auto update() -> void;
    auto removeData(xVisObject*) -> void;
    auto setHoverEnabled(bool) -> void;
    auto setMouseEnabled(bool) -> void;
    auto setExternalEnabled(bool) -> void;
    auto getInput() -> QQmlListProperty<xVisObject>;
    auto getHoverEnabled() -> bool;
    auto getMouseEnabled() -> bool;

    static auto appendInput(QQmlListProperty<xVisObject>*, xVisObject*) -> void;
#if QT_VERSION >= 0x060000
    static auto inputCount(QQmlListProperty<xVisObject>*) -> qsizetype;
#else
    static auto inputCount(QQmlListProperty<xVisObject>*) -> int;
#endif
#if QT_VERSION >= 0x060000
    static auto inputAt(QQmlListProperty<xVisObject>*, qsizetype) -> xVisObject*;
#else
    static auto inputAt(QQmlListProperty<xVisObject>*, int) -> xVisObject*;
#endif
    static auto clearInputs(QQmlListProperty<xVisObject>*) -> void;
protected:
    mutable xVisFboRenderer* m_fboRenderer = nullptr;
    xVisFboOffscreenWindow *m_win;
    auto mousePressEvent(QMouseEvent*) -> void override;
    auto mouseReleaseEvent(QMouseEvent*) -> void override;
    auto mouseMoveEvent(QMouseEvent*) -> void override;
    auto hoverMoveEvent(QHoverEvent*) -> void override;
    auto wheelEvent(QWheelEvent*) -> void override;
signals:
    void inputChanged();
    void hoverEnabledChanged();
    void mouseEnabledChanged();
};
