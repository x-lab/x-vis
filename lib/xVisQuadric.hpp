#pragma once

#include <QObject>

#include "xVisImplicitFunction.hpp"

#include <vtkSmartPointer.h>
#include <vtkQuadric.h>

class xVisQuadric : public xVisImplicitFunction
{
    Q_OBJECT

public:
             xVisQuadric(void);
             xVisQuadric(vtkSmartPointer<vtkQuadric>);
    virtual ~xVisQuadric(void);

public:
    auto get(void) -> vtkSmartPointer<vtkQuadric>;

public slots:
    void setCoefficients(qreal a, qreal b, qreal c, qreal d, qreal e, qreal f, qreal g, qreal h, qreal i, qreal j)
    {
        this->m_object->SetCoefficients(a, b, c, d, e, f, g, h, i, j);
        this->update();
    }

private:
    vtkSmartPointer<vtkQuadric> m_object = nullptr;
};
