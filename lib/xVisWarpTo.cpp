#include "xVisWarpTo.hpp"

xVisWarpTo::xVisWarpTo() : xVisPointSetAlgorithm(vtkSmartPointer<vtkWarpTo>::New()) {
    this->m_object = vtkWarpTo::SafeDownCast(xVisAlgorithm::get());
    this->m_position = new xVisVector3([this](){
        this->m_object->SetPosition(this->m_position->getValues().data());
        this->update();
    });
}

auto xVisWarpTo::getPosition() -> xVisVector3* {
    return this->m_position;
}

auto xVisWarpTo::setScaleFactor(qreal scaleFactor) -> void {
    this->m_object->SetScaleFactor(scaleFactor);
    emit this->scaleFactorChanged();
    this->update();
}

auto xVisWarpTo::getScaleFactor() -> qreal {
    return this->m_object->GetScaleFactor();
}

auto xVisWarpTo::setAbsolute(bool absolute) -> void {
    this->m_object->SetAbsolute(absolute);
    emit this->absoluteChanged();
    this->update();
}

auto xVisWarpTo::getAbsolute() -> bool {
    return this->m_object->GetAbsolute();
}
