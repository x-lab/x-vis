#pragma once

#include "xVisVector3.hpp"
#include "xVisImplicitFunction.hpp"

#include <vtkPerlinNoise.h>

class xVisPerlinNoise : public xVisImplicitFunction {
    Q_OBJECT
    Q_PROPERTY(qreal amplitude READ getAmplitude WRITE setAmplitude NOTIFY amplitudeChanged);
    Q_PROPERTY(xVisVector3* frequency READ getFrequency CONSTANT);
    Q_PROPERTY(xVisVector3* phase READ getPhase CONSTANT);
private:
    vtkSmartPointer<vtkPerlinNoise> m_object = nullptr;
    xVisVector3* m_frequency = nullptr;
    xVisVector3* m_phase = nullptr;
public:
    xVisPerlinNoise();
    auto setAmplitude(qreal) -> void;
    auto getAmplitude() -> qreal;
    auto getFrequency() -> xVisVector3*;
    auto getPhase() -> xVisVector3*;
signals:
    void amplitudeChanged();
};
