#pragma once

#include <QColor>
#include <QObject>

#include <vtkSmartPointer.h>
#include <vtkVolumeProperty.h>
#include <vtkVolume.h>

class xVisVolume;
class xVisPiecewiseFunction;
class xVisColorTransferFunction;

class xVisVolumeProperty : public QObject {
    Q_OBJECT
    Q_PROPERTY(bool shade READ getShade WRITE setShade NOTIFY shadeChanged);
    Q_PROPERTY(xVisPiecewiseFunction* scalarOpacityFunction READ getScalarOpacityFunction CONSTANT);
    Q_PROPERTY(xVisPiecewiseFunction* gradientOpacityFunction READ getGradientOpacityFunction CONSTANT);
    Q_PROPERTY(xVisColorTransferFunction* transferFunction READ getTransferFunction CONSTANT);
    Q_PROPERTY(qreal ambient READ getAmbient WRITE setAmbient NOTIFY ambientChanged);
    Q_PROPERTY(qreal diffuse READ getDiffuse WRITE setDiffuse NOTIFY diffuseChanged);
    Q_PROPERTY(qreal specular READ getSpecular WRITE setSpecular NOTIFY specularChanged);
    Q_PROPERTY(qreal specularPower READ getSpecularPower WRITE setSpecularPower NOTIFY specularPowerChanged);
private:
    xVisVolume* m_volume = nullptr;
    xVisPiecewiseFunction* m_gradientOpacityFunction = nullptr;
    xVisPiecewiseFunction* m_scalarOpacityFunction = nullptr;
    xVisColorTransferFunction* m_transferFunction = nullptr;
    vtkSmartPointer<vtkVolume> m_volume_ptr = nullptr;
private:
    void update();
public:
    xVisVolumeProperty() = delete;
    xVisVolumeProperty(xVisVolume*);
    auto setShade(bool) -> void;
    auto getShade() -> bool;
    auto setAmbient(qreal) -> void;
    auto getAmbient() -> qreal;
    auto setDiffuse(qreal) -> void;
    auto getDiffuse() -> qreal;
    auto setSpecular(qreal) -> void;
    auto getSpecular() -> qreal;
    auto setSpecularPower(qreal) -> void;
    auto getSpecularPower() -> qreal;
    auto getGradientOpacityFunction() -> xVisPiecewiseFunction*;
    auto getScalarOpacityFunction() -> xVisPiecewiseFunction*;
    auto getTransferFunction() -> xVisColorTransferFunction*;
signals:
    void shadeChanged();
    void ambientChanged();
    void diffuseChanged();
    void specularChanged();
    void specularPowerChanged();
};
