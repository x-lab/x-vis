#include "xVisMapper.hpp"

xVisMapper::xVisMapper(vtkSmartPointer<vtkMapper> vtkObject) : xVisAbstractMapper3D(vtkObject) {
    this->m_object = vtkObject;
}

auto xVisMapper::get() -> vtkSmartPointer<vtkMapper> {
    return vtkMapper::SafeDownCast(xVisAlgorithm::get());
}

auto xVisMapper::setScalarMode(ScalarMode scalarMode) -> void {
    this->m_object->SetScalarMode(scalarMode);
    emit this->scalarModeChanged();
    this->update();
}

auto xVisMapper::getScalarMode() -> ScalarMode {
    return static_cast<ScalarMode>(this->m_object->GetScalarMode());
}

auto xVisMapper::setScalarVisibility(bool val) -> void {
    this->m_object->SetScalarVisibility(val);
    emit this->scalarVisibilityChanged();
    this->update();
}

auto xVisMapper::getScalarVisibility() -> bool {
    return this->m_object->GetScalarVisibility();
}
