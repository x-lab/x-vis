#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkPlatonicSolidSource.h>

class xVisPlatonicSolidSource : public xVisPolyDataAlgorithm {
    Q_OBJECT
public:
    enum SolidType {
        SolidTetrahedron = VTK_SOLID_TETRAHEDRON,
        SolidCube = VTK_SOLID_CUBE,
        SolidOctahedron = VTK_SOLID_OCTAHEDRON,
        SolidIcosahedron = VTK_SOLID_ICOSAHEDRON,
        SolidDodecahedron = VTK_SOLID_DODECAHEDRON
    };
private:
    Q_ENUM(SolidType);
    Q_PROPERTY(SolidType solidType READ getSolidType WRITE setSolidType NOTIFY solidTypeChanged);
private:
    vtkSmartPointer<vtkPlatonicSolidSource> m_object = nullptr;
public:
    xVisPlatonicSolidSource();
    auto setSolidType(SolidType) -> void;
    auto getSolidType() -> SolidType;
signals:
    void solidTypeChanged();
};
