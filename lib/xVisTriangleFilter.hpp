#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkTriangleFilter.h>

class xVisTriangleFilter : public xVisPolyDataAlgorithm {
    Q_OBJECT
    Q_PROPERTY(bool passVerts READ getPassVerts WRITE setPassVerts NOTIFY passVertsChanged);
    Q_PROPERTY(bool passLines READ getPassLines WRITE setPassLines NOTIFY passLinesChanged);
private:
    vtkSmartPointer<vtkTriangleFilter> m_object = nullptr;
public:
    xVisTriangleFilter();
    auto setPassVerts(bool) -> void;
    auto setPassLines(bool) -> void;
    auto getPassVerts() -> bool;
    auto getPassLines() -> bool;
signals:
    void passVertsChanged();
    void passLinesChanged();
};
