#pragma once

#include "xVisPolyDataAlgorithm.hpp"

#include <vtkDensifyPolyData.h>

class xVisDensifyPolyData : public xVisPolyDataAlgorithm {
    Q_OBJECT
    Q_PROPERTY(int numberOfSubdivisions READ getNumberOfSubdivisions WRITE setNumberOfSubdivisions NOTIFY numberOfSubdivisionsChanged);
private:
    vtkSmartPointer<vtkDensifyPolyData> m_object = nullptr;
public:
    xVisDensifyPolyData();
    auto setNumberOfSubdivisions(int) -> void;
    auto getNumberOfSubdivisions() -> int;
signals:
    void numberOfSubdivisionsChanged();
};
