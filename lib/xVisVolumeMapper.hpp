#pragma once

#include "xVisAbstractVolumeMapper.hpp"

#include <vtkVolumeMapper.h>

class xVisVolumeMapper : public xVisAbstractVolumeMapper {
    Q_OBJECT
public:
    enum BlendMode {
        CompositeBlend = vtkVolumeMapper::COMPOSITE_BLEND,
        MaximumIntensityBlend = vtkVolumeMapper::MAXIMUM_INTENSITY_BLEND,
        MinimumIntensityBlend = vtkVolumeMapper::MINIMUM_INTENSITY_BLEND,
        AdditiveBlend = vtkVolumeMapper::ADDITIVE_BLEND
    };
private:
    Q_ENUM(BlendMode);
    Q_PROPERTY(BlendMode blendMode READ getBlendMode WRITE setBlendMode NOTIFY blendModeChanged);
    Q_PROPERTY(bool cropping READ getCropping WRITE setCropping NOTIFY croppingChanged);
private:
    vtkSmartPointer<vtkVolumeMapper> m_object = nullptr;
public:
    xVisVolumeMapper(vtkSmartPointer<vtkVolumeMapper>);
    auto setBlendMode(BlendMode) -> void;
    auto getBlendMode() -> BlendMode;
    auto setCropping(bool) -> void;
    auto getCropping() -> bool;
signals:
    void blendModeChanged();
    void croppingChanged();
};
