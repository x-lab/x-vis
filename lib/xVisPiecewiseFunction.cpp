#include "xVisPiecewiseFunction.hpp"

xVisPiecewiseFunction::xVisPiecewiseFunction(vtk_t vtkObject, cb_t&& callback) : m_object(vtkObject), m_callback(callback) {
}

auto xVisPiecewiseFunction::update() -> void {
    this->m_callback.operator()();
}

void xVisPiecewiseFunction::clear() {
    this->m_xValues.clear();
    this->m_yValues.clear();

    emit this->sizeChanged();
    this->m_object->RemoveAllPoints();
    this->update();
}

void xVisPiecewiseFunction::add(double x, double y) {
    this->m_xValues.append(x);
    this->m_yValues.append(y);

    emit this->sizeChanged();
    this->m_object->AddPoint(x, y);
    this->update();
}

auto xVisPiecewiseFunction::getX(int i) -> double {
    return i < this->m_xValues.length() && i >= 0 ? this->m_xValues.at(i) : 0;
}

auto xVisPiecewiseFunction::getY(int i) -> double {
    return i < this->m_yValues.length() && i >= 0 ? this->m_yValues.at(i) : 0;
}

auto xVisPiecewiseFunction::setClamping(bool clamping) -> void {
    this->m_object->SetClamping(clamping);
    this->update();
}

auto xVisPiecewiseFunction::getClamping() -> bool {
    return this->m_object->GetClamping();
}

auto xVisPiecewiseFunction::getSize() -> int {
    return this->m_xValues.length();
}
