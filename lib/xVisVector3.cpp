#include "xVisVector3.hpp"

// Qml::Register::UncreatableClass<xVisVector3> xVisVector3::Register;

xVisVector3::xVisVector3(cb_t&& cb, array_t values) : m_callback(cb), m_values(values)
{

}

void xVisVector3::notify(void)
{
    this->m_callback.operator()();
}

void xVisVector3::setX(double x)
{
    this->m_values[0] = x;
    emit this->xChanged();
    this->notify();
}

void xVisVector3::setY(double y)
{
    this->m_values[1] = y;
    emit this->yChanged();
    this->notify();
}

void xVisVector3::setZ(double z)
{
    this->m_values[2] = z;
    emit this->zChanged();
    this->notify();
}

double xVisVector3::getX(void)
{
    return this->m_values[0];
}

double xVisVector3::getY(void)
{
    return this->m_values[1];
}

double xVisVector3::getZ(void)
{
    return this->m_values[2];
}

xVisVector3::array_t xVisVector3::getValues(void)
{
    return this->m_values;
}
