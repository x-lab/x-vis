#include "xVisContourFilter.hpp"

xVisContourFilter::xVisContourFilter(void) : xVisPolyDataAlgorithm(vtkSmartPointer<vtkContourFilter>::New())
{
    this->m_object = vtkContourFilter::SafeDownCast(xVisPolyDataAlgorithm::get());
}

xVisContourFilter::xVisContourFilter(vtkSmartPointer<vtkContourFilter> vtkObject) : xVisPolyDataAlgorithm(vtkObject)
{
    this->m_object = vtkContourFilter::SafeDownCast(xVisPolyDataAlgorithm::get());
}

auto xVisContourFilter::get(void) -> vtkSmartPointer<vtkContourFilter>
{
    return this->m_object;
}

xVisContourFilter::~xVisContourFilter(void)
{
    this->m_object = nullptr;
}
