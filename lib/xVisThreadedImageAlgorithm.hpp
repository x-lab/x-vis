#pragma once

#include "xVisImageAlgorithm.hpp"

#include <vtkThreadedImageAlgorithm.h>

class xVisThreadedImageAlgorithm : public xVisImageAlgorithm {
    Q_OBJECT
public:
    xVisThreadedImageAlgorithm(vtkSmartPointer<vtkThreadedImageAlgorithm>);
};
