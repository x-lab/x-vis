#include "xVisSmoothPolyDataFilter.hpp"

xVisSmoothPolyDataFilter::xVisSmoothPolyDataFilter() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkSmoothPolyDataFilter>::New()) {
    this->m_object = vtkSmoothPolyDataFilter::SafeDownCast(xVisAlgorithm::get());
}

auto xVisSmoothPolyDataFilter::setConvergence(qreal convergence) -> void {
    this->m_object->SetConvergence(convergence);
    this->update();
    emit this->convergenceChanged();
}

auto xVisSmoothPolyDataFilter::getConvergence() -> qreal {
    return this->m_object->GetConvergence();
}

auto xVisSmoothPolyDataFilter::setNumberOfIterations(int numberOfIterations) -> void {
    this->m_object->SetNumberOfIterations(numberOfIterations);
    this->update();
    emit this->numberOfIterationsChanged();
}

auto xVisSmoothPolyDataFilter::setRelaxationFactor(qreal relaxationFactor) -> void {
    this->m_object->SetRelaxationFactor(relaxationFactor);
    this->update();
    emit this->relaxationFactorChanged();
}

auto xVisSmoothPolyDataFilter::getRelaxationFactor() -> qreal {
    return this->m_object->GetRelaxationFactor();
}

auto xVisSmoothPolyDataFilter::getNumberOfIterations() -> int {
    return this->m_object->GetNumberOfIterations();
}

auto xVisSmoothPolyDataFilter::setFeatureEdgeSmoothing(bool featureEdgeSmoothing) -> void {
    this->m_object->SetFeatureEdgeSmoothing(featureEdgeSmoothing);
    this->update();
    emit this->featureEdgeSmoothingChanged();
}

auto xVisSmoothPolyDataFilter::getFeatureEdgeSmoothing() -> bool {
    return this->m_object->GetFeatureEdgeSmoothing();
}

auto xVisSmoothPolyDataFilter::setFeatureAngle(qreal featureAngle) -> void {
    this->m_object->SetFeatureAngle(featureAngle);
    this->update();
    emit this->featureAngleChanged();
}

auto xVisSmoothPolyDataFilter::getFeatureAngle() -> qreal {
    return this->m_object->GetFeatureAngle();
}

auto xVisSmoothPolyDataFilter::setEdgeAngle(qreal edgeAngle) -> void {
    this->m_object->SetEdgeAngle(edgeAngle);
    this->update();
    emit this->edgeAngleChanged();
}

auto xVisSmoothPolyDataFilter::getEdgeAngle() -> qreal {
    return this->m_object->GetEdgeAngle();
}

auto xVisSmoothPolyDataFilter::setBoundarySmoothing(bool boundarySmoothing) -> void {
    this->m_object->SetBoundarySmoothing(boundarySmoothing);
    this->update();
    emit this->boundarySmoothingChanged();
}

auto xVisSmoothPolyDataFilter::getBoundarySmoothing() -> bool {
    return this->m_object->GetBoundarySmoothing();
}

auto xVisSmoothPolyDataFilter::setGenerateErrorScalars(bool generateErrorScalars) -> void {
    this->m_object->SetGenerateErrorScalars(generateErrorScalars);
    this->update();
    emit this->generateErrorScalarsChanged();
}

auto xVisSmoothPolyDataFilter::getGenerateErrorScalars() -> bool {
    return this->m_object->GetGenerateErrorScalars();
}

auto xVisSmoothPolyDataFilter::setGenerateErrorVectors(bool generateErrorVectors) -> void {
    this->m_object->SetGenerateErrorScalars(generateErrorVectors);
    this->update();
    emit this->generateErrorVectorsChanged();
}

auto xVisSmoothPolyDataFilter::getGenerateErrorVectors() -> bool {
    return this->m_object->GetGenerateErrorVectors();
}
