#include "xVisExtractSurface.hpp"

xVisExtractSurface::xVisExtractSurface() : xVisPolyDataAlgorithm(vtkSmartPointer<vtkExtractSurface>::New())
{
    this->m_object = vtkExtractSurface::SafeDownCast(xVisAlgorithm::get());
}

auto xVisExtractSurface::setRadius(qreal radius) -> void {
    this->m_object->SetRadius(radius);
    this->update();
    emit this->radiusChanged();
}

auto xVisExtractSurface::getRadius() -> qreal {
    return this->m_object->GetRadius();
}

auto xVisExtractSurface::setHoleFilling(bool holeFilling) -> void {
    this->m_object->SetHoleFilling(holeFilling);
    this->update();
    emit this->holeFillingChanged();
}

auto xVisExtractSurface::getHoleFilling() -> bool {
    return this->m_object->GetHoleFilling();
}

auto xVisExtractSurface::setComputeNormals(bool computeNormals) -> void {
    this->m_object->SetComputeNormals(computeNormals);
    this->update();
    emit this->computeNormalsChanged();
}

auto xVisExtractSurface::getComputeNormals() -> bool {
    return this->m_object->GetComputeNormals();
}

auto xVisExtractSurface::setComputeGradients(bool computeGradients) -> void {
    this->m_object->SetComputeGradients(computeGradients);
    this->update();
    emit this->computeGradientsChanged();
}

auto xVisExtractSurface::getComputeGradients() -> bool {
    return this->m_object->GetComputeGradients();
}
