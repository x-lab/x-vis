import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import xQuick.Vis 1.0 as XVis

ApplicationWindow
{
    visible: true

    XVis.Viewer {

        anchors.fill: parent;

        mouseEnabled: true;

        XVis.Actor {
            XVis.PolyDataMapper {
                XVis.PLYReader {
                    id: source;
                    fileName: "/home/trcabel/Dev/naviscope/test_data/lucy.ply"
                }
            }
        }
    }
}
