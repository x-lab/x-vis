#include <QGuiApplication>
#include <QStyleHints>
#include <QQmlApplicationEngine>
#include <QtQml/qqmlextensionplugin.h>

#include <xVis.hpp>


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    x_vis_initialise();

    QGuiApplication::styleHints()->setUseHoverEffects(true);
    QLoggingCategory::setFilterRules(QStringLiteral("qt.qml.binding.removal.info=true"));

    QQmlApplicationEngine engine;
    engine.addImportPath("qrc:/qt/qml/");
    engine.load(QUrl("qrc:/xvisdemo/main.qml"));

    //qt 6.5
    //engine.load(QUrl("qrc:/qt/qml/xvisdemo/main.qml"));

    return app.exec();
}
