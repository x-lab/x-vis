use cpp::cpp;

cpp! {{
#include <xVis.hpp>
}}

pub fn init() {
    cpp!(unsafe [] {
        x_vis_initialise();
    });
}
